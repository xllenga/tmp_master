

export class StringUtil {

  static isNullOrEmpty(str:string|null) {
    return !(str && str.length > 0);
  }

}