import {MailEngine} from 'coremail/src/main/ets/engine/engine'
import { Properties } from 'coremail/src/main/ets/api';
import { StringUtil } from 'coremail/src/main/ets/format/string_util';

export type MailSummary = {
  mid: string;
  account: string;
  subject: string;
  summary: string;
  date: Date;
  isFlagged: boolean;
  isNew: boolean;
  hasAttachment: boolean;
  folder: string;
}

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'xiongwenshuai1@outlook.com',
    password: 'zhang@139250',
  }
}

interface IApp {
  search(account: string, keyword: string, filterType: ['subject' | 'from' | 'to'], start: number, size: number): Promise<MailSummary[]>;
}


class MailApp implements IApp {

  _engineMap: Map<string, MailEngine> = new Map;

  /**
   * 邮件搜索
   * @param account
   * @param keyword
   * @param types
   * @param start
   * @param size
   * @returns
   */
  async search(account: string, keyword: string, filterType: ('subject'| 'from' | 'to')[], start: number, size: number): Promise<MailSummary[]> {
    const exchangeEngine = new MailEngine();
    exchangeEngine.setProperties(properties);
    const mails = await exchangeEngine.search(keyword, filterType, start, size);
    return Promise.all(mails.map(async mail => {
      let from = await mail.getFrom()
      let folder = mail.getFolder().getName()
      if (folder == '已发送邮件' || folder == '发件箱' ) {
        let to = await mail.getTo()
        if (to.length > 0) {
          from = to[0]
        }
      }
      return {
        mid: mail.getId(),
        account: StringUtil.isNullOrEmpty(from.email) ? from.name : from.email,
        subject: await mail.getSubject(),
        summary: "", //获取摘要导致邮件数据返回慢，暂时先屏蔽获取方法 await mail.getDigest(10),
        date: await mail.getDate(),
        isFlagged: await mail.isFlagged(),
        isNew: !(await mail.isSeen()),
        hasAttachment: await mail.hasAttachment(),
        folder: folder
      }
    }))
  }
}

let app: MailApp | undefined;

export function getApp(): MailApp {

  if (!app) {
    app = new MailApp();
  }
  return app;
}



