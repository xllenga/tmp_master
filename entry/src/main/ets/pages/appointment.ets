import { CreateAppointmentMessage,
  AppointmentMessage } from 'coremail/src/main/ets/protocols/exchange/model/calendar/create_appointment_message';
import {
  createAppointmentItem,
  createAppointmentItems
} from 'coremail/src/main/ets/protocols/exchange/task/calendar/create_appointment_task';
import {
  createCalendarItem,
  createCalendarItems
} from 'coremail/src/main/ets/protocols/exchange/task/calendar/create_calendar_task';
import { cancelMeeting } from 'coremail/src/main/ets/protocols/exchange/task/calendar/cancel_meeting_task';
import { Properties } from 'coremail/src/main/ets/api';
import { BodyType, Importance, Sensitivity } from 'coremail/src/main/ets/protocols/exchange/model/mail/item_message';
import { createSoapXmlTag as $ } from 'coremail/src/main/ets/protocols/exchange/xml/xmlElement/SoapXmlTag';
import hilog from '@ohos.hilog';
import { SoapXmlWriter, XmlElement } from 'coremail/src/main/ets/protocols/exchange/xml';
import util from '@ohos.util';
import { SendMeetingInvitations } from 'coremail/src/main/ets/protocols/exchange/utils/common_enum';


const TEST_XML = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types"
    xmlns:m="http://schemas.microsoft.com/exchange/services/2006/messages">
    <soap:Header>
        <t:RequestServerVersion Version="Exchange2007_SP1"/>
    </soap:Header>
    <soap:Body>
        <CreateItem xmlns="http://schemas.microsoft.com/exchange/services/2006/messages" SendMeetingInvitations="SendToAllAndSaveCopy">
            <SavedItemFolderId>
                <t:DistinguishedFolderId Id="calendar"/>
            </SavedItemFolderId>
            <Items>
                <t:CalendarItem xmlns="http://schemas.microsoft.com/exchange/services/2006/types">
                    <Subject>wml创建的会议2</Subject>
                    <Body BodyType="Text">创建会议</Body>
                    <ReminderIsSet>true</ReminderIsSet>
                    <ReminderMinutesBeforeStart>60</ReminderMinutesBeforeStart>
                    <Start>2024-03-28T05:30:00</Start>
                    <End>2024-03-28T06:30:00</End>
                    <IsAllDayEvent>false</IsAllDayEvent>
                    <LegacyFreeBusyStatus>Busy</LegacyFreeBusyStatus>
                    <Location>china wuhan</Location>
                    <RequiredAttendees>
                        <Attendee>
                            <MailBox>
                                <EmailAddress>hongtenglong111@outlook.com</EmailAddress>
                            </MailBox>
                        </Attendee>
                        <Attendee>
                            <MailBox>
                                <EmailAddress>xlleng110@hotmail.com</EmailAddress>
                            </MailBox>
                        </Attendee>
                    </RequiredAttendees>
                </t:CalendarItem>
            </Items>
        </CreateItem>
    </soap:Body>
</soap:Envelope>`


let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'wangml9655@outlook.com',
    password: 'wml840576)(',
  }
};

const letters = Array.from('abcdefghijklmnopqrstuvwxyz');

function randomText() {
  let res = '';
  const randomLen = 3 + Math.floor(Math.random() * 5)
  for (let i = 0; i < randomLen; i ++) {
    const idx = Math.floor(Math.random() * 26);
    res += letters[idx];
  }
  return res;
}

@Entry
@Component
struct Index {
  @State message: string = 'Hello World';

  build() {
    Row() {
      Column() {
        Button('创建日程')
          .fontSize(16)
          .onClick(() => {
            this.createAppointment();
          })
        Button('批量创建日程')
          .fontSize(16)
          .onClick(() => {
            this.batchCreateCalendar();
          }).margin({top: 20})
        Button('创建日历')
          .fontSize(16)
          .onClick(() => {
            this.createCalendar();
          }).margin({top: 20})
        Button('批量创建日历')
          .fontSize(16)
          .onClick(() => {
            this.createCalendars();
          }).margin({top: 20})
        Button('取消会议')
          .fontSize(16)
          .onClick(() => {
            this.testCancelMeeting();
          }).margin({top: 20})
        Button('性能测试')
          .fontSize(16)
          .onClick(() => {
            this.performanceTest();
          }).margin({top: 20})
        Button('writer')
          .fontSize(16)
          .onClick(() => {
            this.testWriterWidthElement();
          }).margin({top: 20})
      }
      .width('100%')
    }
    .height('100%')
  }

  async testCancelMeeting() {
    try {
      const result = await cancelMeeting({
        id: 'AQMkADAwATM0MDAAMS02Y2ExLTU3MTgtMDACLTAwCgBGAAADHZHvh6yLEkeXDBhVyG4PHAcAbENx4CxZmEuCvSCxK7lYQQAAAgENAAAAbENx4CxZmEuCvSCxK7lYQQAAABPNQKYAAAA=',
        changeKey: 'DwAAABYAAABsQ3HgLFmYS4K9ILEruVhBAAATy6i+',
        content: '不开了'
      });
      console.log(JSON.stringify(result));
    } catch (err) {
      console.log(JSON.stringify(err));
    }
  }

  // 适配 xmlWriter
  testWriterWidthElement() {
    const xmlWriter = new SoapXmlWriter();

    $(XmlElement.NAME_ACCESS_LEVEL).appendChild(
      $(XmlElement.NAME_ABSOLUTE_DATE_TRANSITION).setAttribute('aaaa', 'bbbb')
    ).write(xmlWriter);

    const xmlString = xmlWriter.getXmlString();
    console.log(xmlString)
  }

  async performanceTest() {
    // 创建层级为12 的二叉树
    const root = $(randomText());
    let currentLevel = [root];
    for (let i = 0; i < 12; i ++) {
      const nodes = [
        $(randomText()).setAttribute(randomText(), randomText()),
        $(randomText()).setAttribute(randomText(), randomText()),
      ];
      for (const node of currentLevel) {
        node.appendChildren(nodes);
      }
      currentLevel = nodes;
    }
    const startTime = new Date().getTime();
    const res = root.toString();
    const endTime = new Date().getTime();
    const averageTime = endTime - startTime;
    hilog.info(0x0000, 'performanceTest', `averageTime: ${averageTime} ms`)
  }

  testWriter() {
    const arrayBuffer = new ArrayBuffer(8192);
    const textDecoder = util.TextDecoder.create();
    let view: Uint8Array = new Uint8Array(arrayBuffer); // 使用Uint8Array读取arrayBuffer的数据
    let res: string = textDecoder.decodeWithStream(view);
  }

  async createCalendar() {
    try {
      const result = await createCalendarItem('huawei');
      console.log(JSON.stringify(result));
    } catch (err) {
      console.log(JSON.stringify(err));
    }
  }

  async createCalendars() {
    try {
      const result = await createCalendarItems(['ccc', 'ddd']);
      console.log(JSON.stringify(result));
    } catch (err) {
      console.log(JSON.stringify(err));
    }
  }

  async batchCreateCalendar() {
    let appointmentMessage = new AppointmentMessage();
    let appointmentMessage1 = new AppointmentMessage();

    appointmentMessage.subject = 'javascript';
    appointmentMessage.bodyType = BodyType.HTML;
    appointmentMessage.bodyText = '创建会议html';
    appointmentMessage.reminderIsSet = true;
    appointmentMessage.reminderMinutesBeforeStart = 60;
    appointmentMessage.location = 'china wuhan';
    appointmentMessage.start = new Date('2024/4/17 13:30');
    appointmentMessage.end = new Date('2024/4/17 14:30');

    appointmentMessage1.subject = 'arkts typescript';
    appointmentMessage1.bodyType = BodyType.Text;
    appointmentMessage1.bodyText = '创建会议';
    appointmentMessage1.reminderIsSet = true;
    appointmentMessage1.reminderMinutesBeforeStart = 30;
    appointmentMessage1.location = 'china wuhan';
    appointmentMessage1.start = new Date('2024/4/17 14:30');
    appointmentMessage1.end = new Date('2024/4/17 15:30');
    appointmentMessage1.requiredAttendees = ['wangml9655@163.com'];

    try {
      const result = await createAppointmentItems(
        [appointmentMessage, appointmentMessage1],
        SendMeetingInvitations.SendToAllAndSaveCopy
      );
      console.log(JSON.stringify(result));
    } catch (err) {
      console.log(JSON.stringify(err));
    }
  }

  async createAppointment() {
    let appointmentMessage = new AppointmentMessage();
    // 会议主题
    appointmentMessage.subject = 'exchange meeting 0419';
    // 正文类型 Text Html
    appointmentMessage.bodyType = BodyType.HTML;
    // 正文内容
    appointmentMessage.bodyText = '创建会议';
    // 是否提醒
    appointmentMessage.reminderIsSet = true;
    // 提醒时间设置
    appointmentMessage.reminderMinutesBeforeStart = 60;
    // 地点
    appointmentMessage.location = 'china wuhan';
    // 开始时间
    // appointmentMessage.start = new Date('2024/4/19 13:30');
    // 结束时间
    appointmentMessage.end = new Date('2024/4/19 14:30');
    // 与会人
    // appointmentMessage.requiredAttendees = ['hongtenglong111@outlook.com'];
    // appointmentMessage.optionalAttendees = ['wangml9655@163.com']

    try {
      const result = await createAppointmentItem(appointmentMessage, SendMeetingInvitations.SendToAllAndSaveCopy);
      console.log(JSON.stringify(result));
    } catch (err) {
      console.log(err.code);
    };

  }
}