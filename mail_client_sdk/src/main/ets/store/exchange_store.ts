/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  EmailAddress,
  Filter,
  FolderData,
  FolderType,
  IBuffer,
  IBufferCreator,
  IStore,
  MailAttribute,
  MailBasic,
  MailEnvelope,
  MailHeader,
  Order,
  Properties,
  Query,
  StoreFeature,
} from '../api';


import {Mailbox, Message } from '../protocols/exchange/model/mail/item_message';

import { FieldURISchema } from '../protocols/exchange/xml/xml_attribute';
import {
  ComparisonMode,
  Composite,
  ContainmentMode, ContainsSubstring,
  LogicalOperator,
  SearchMailFilter } from '../protocols/exchange/model/search/mail_search_filter';
import { SearchMailTask } from '../protocols/exchange/task/search/search_mail_task'
import { SearchMailRequest } from '../protocols/exchange/request/search/search_mail_request';

import { Folder, FolderId } from '../protocols/exchange/model/folder/folder';

import { BasePoint,
  IItemView,
  ISortOrder,
  SearchView,
  SortOrder} from '../protocols/exchange/model/search/mail_search_view';
import { Filters } from '../protocols/exchange/model/search/mail_search_findItems';
import { FindItems} from '../protocols/exchange/model/search/mail_search_findItems'

import { EventItem, EventType } from '../protocols/exchange/model/streaming_subscription';
import { autoDiscoverUrl } from '../protocols/exchange/task/settings/autodiscover_task';
import { SubscribeToStreamingNotifications } from '../protocols/exchange/task/subscribe_streaming_notifications';
import { AuthType, BasicCredentials, CredentialsStore } from '../protocols/exchange/utils/exchange_credentials';
import { FolderName } from '../protocols/exchange/xml';

function parseDate(dateStr: string): Date {
  let date: Date;
  if (dateStr) {
    date = parseDateString(dateStr);
  } else {
    date = new Date("1970-01-01");
  }
  return date;
}

function parseFrom(envelope:Message[]):EmailAddress {
  return {
    name:envelope[0].from.name,
    email:envelope[0].from.emailAddress,
  };
}

function parseEnvelope(envelope:Message[]):MailEnvelope {
  function parseMailList(list: Mailbox[]) : EmailAddress[] {
    return list.map(item => {
      return {
        name: item.name,
        email: item.emailAddress,
      };
    });
  }
  const mailEnvelope: MailEnvelope = {
    date:parseDate(envelope[0].dateTimeCreated),
    subject:envelope[0].subject,
    from:parseFrom(envelope),
    to:parseMailList(envelope[0].toRecipients),
    cc:parseMailList(envelope[0].ccRecipients),
    bcc:parseMailList(envelope[0].bccRecipients),
    isHasAttachment:envelope[0].hasAttachments == 'true',
    body:envelope[0].body,
    messageId:'',
  };
  return mailEnvelope;
}

function parseAttributes(envelope:Message[]) {
  let attributes:MailAttribute[] = [];
  if(envelope[0].flag.flagStatus === 'Flagged') {
    attributes.push(MailAttribute.Flagged);
  }
  if(envelope[0].isRead=== 'true') {
    attributes.push(MailAttribute.Seen);
  }
  attributes.push(MailAttribute.Unknown);
  return attributes;
}

function parseMailBasicInfo(resp:ResponseMessage<Message>,uid:string):MailBasic{
  const envelope:MailEnvelope = parseEnvelope(resp.items);
  const attributes = parseAttributes(resp.items);
  const data: MailBasic =  {
    id:uid,
    envelope:envelope,
    structure:null,
    attributes:attributes,
    size:Number.parseInt(resp.items[0].size),
  };
  return data;
}

function parseFolder(folderFullName: string):UniteId {
  const i = folderFullName.lastIndexOf(";");
  let id = '';
  let changeKey = '';
  if(i>0) {
    id = folderFullName.substring(0,i);
    changeKey = folderFullName.substring(i+1);
  }
  const result :UniteId =  {
    id:id,
    changeKey:changeKey
  };
  return result;
}
import { MessageDisposition, PropertySet } from '../../ets/protocols/exchange/utils/common_enum';
import { SyncFolderItemsTask } from '../../ets/protocols/exchange/task/sync/sync_folder_items_task';
import { SyncFolderItemsRequest } from '../protocols/exchange/request/sync/sync_folder_items_request';
import { FolderItems, FolderItemsResp } from '../protocols/exchange/model/sync/folder_items';

import { ChangeItem, ItemField, ItemOperateType, UpdateItem } from '../protocols/exchange/model/update_item';
import { UpdateItemTask } from '../protocols/exchange/task/update_item_task';
import { UpdateItemRequest } from '../protocols/exchange/request/update_item_request';
import { UpdateItemResponse } from '../protocols/exchange/response/update_item_response';
import { AffectedTaskOccurrence, DeleteItem, SendCancellationsType } from '../protocols/exchange/model/delete_item';
import { DeleteItemTask } from '../protocols/exchange/task/delete_item_task';
import { DeleteItemRequest } from '../protocols/exchange/request/delete_item_request';
import { DeleteItemResponse } from '../protocols/exchange/response/delete_item_response';
import { DistinguishedFolderId } from '../protocols/exchange/model/folder/folder';
import { UniteId } from '../protocols/exchange/model/mode_base';
import { getLogger, Logger } from '../utils/log';
import { FolderHierarchy, FolderHierarchyResp } from '../protocols/exchange/model/sync/folder_hierarchy';
import { GetItem, GetItemMessageResponseModel } from '../protocols/exchange/model/get_item';
import { GetItemMessageResponse } from '../protocols/exchange/response/get_item_message_response';
import { GetItemTask } from '../protocols/exchange/task/get_item_task';
import { GetItemRequest } from '../protocols/exchange/request/get_item_request';
import { ResponseMessage } from '../protocols/exchange/response/response_base';
import { SyncFolderHierarchyTask } from '../protocols/exchange/task/sync/sync_folder_hierarchy_task';
import { SyncFolderHierarchyRequest } from '../protocols/exchange/request/sync/sync_folder_hierarchy_request';
import { GetFolder, GetFolderResp } from '../protocols/exchange/model/folder/get_folder';
import { GetFolderTask } from '../protocols/exchange/task/folder/folder_task';
import { GetFolderRequest } from '../protocols/exchange/request/folder/get_folder_request';
import { parseDateString } from '../utils/common';
import { DownLoadManager } from '../protocols/exchange/task/mail/download_manager_task';
import {
  BaseAttachments,
  CreateAttachment,
  DeleteAttachment, GetAttachment, OnProgress } from '../protocols/exchange/model/mail/attachments';
import { GetAttachmentTask } from '../protocols/exchange/task/mail/get_attachment_task';
import {
  CreateAttachmentResponse,
  DeleteAttachmentResponse,
  GetAttachmentResponse } from '../protocols/exchange/response/mail/attachment_response';
import {
  CreateAttachmentRequest,
  DeleteAttachmentRequest, GetAttachmentRequest } from '../protocols/exchange/request/mail/attachment_request';
import { DeleteAttachmentTask } from '../protocols/exchange/task/mail/delete_attachment_task';
import { CreateAttachmentTask } from '../protocols/exchange/task/mail/create_attachment_task';


const logger: Logger = getLogger("exchangeStore");
const streamingSubscription: SubscribeToStreamingNotifications = SubscribeToStreamingNotifications.getInstance();

export class ExchangeStore implements IStore {
  private properties: Properties | null = null;
  private currentFolder: string = "";
  private domainList: string[] = [
    'outlook.com',
    'hotmail.com',
    'gmail.cn',
    '163.com',
    '126.com',
    'qq.com',
    '139.com',
    '189.com',
    'wo.cn',
    'tom.com',
    'aliyun.com',
    'foxmail.com',
    'vip.163.com',
    'vip.126.com',
    'yeah.net',
    '188.com',
    'sina.com',
    'sina.cn',
    'sohu.com',
    'yahoo.com',
    'live.com',
    'live.cn',
  ];

  /**
   * 登录
   *
   * @param type
   */
  login(type?: AuthType): Promise<void> {
    if (!this.properties) {
      logger.error('properties is null');
      return;
    }
    if (!this.properties.userInfo.username || !this.properties.userInfo.password) {
      logger.error('username or password is null');
      return;
    }
    if (type === AuthType.OAUTH) {
      logger.info('auth2');
    } else if (type === AuthType.NTLM) {
      logger.info('ntlm');
    } else {
      CredentialsStore.getInstance().credentials = new BasicCredentials
      (this.properties.userInfo.username, this.properties.userInfo.password);
    }
    return;
  }

  /**
   * 设置配置信息
   *
   * @param properties
   * @throws { ExchangeError } USERNAME_OR_PASSWORD_IS_NULL userName or passWord is null.
   */
  setProperties(properties: Properties) {
    this.properties = properties;
    if (!CredentialsStore.getInstance().credentials) {
      this.login();
    }
  }

  /**
   * 自动发现，获取连接点
   *
   * @param callback 回调函数
   * @returns 连接点
   */
  public async autoDiscoverEwsUrl(callback?: (progress: number) => void): Promise<string> {
    if (!this.properties) {
      logger.error('autoDiscoverEwsUrl, properties is null');
      return '';
    }
    let ewsUrl: string = await autoDiscoverUrl(this.properties, callback);
    if (this.properties.exchange) {
      this.properties.exchange.url = ewsUrl;
    }
    return ewsUrl;
  }

  /**
   * 新邮件流式订阅-订阅
   *
   * @param properties 请求配置
   */
  public newMailSubscriptionInitiate(properties: Properties, folderNames: FolderName[], eventTypes: EventType[]) {
    streamingSubscription.setFolderIds(folderNames);
    streamingSubscription.setEventTypes(eventTypes);
    streamingSubscription.setProperties(properties);
    streamingSubscription.initiate();
  }

  /**
   * 新邮件流式订阅-打开
   *
   * @param listener 监听器，用于返回新邮件信息
   */
  public newMailSubscriptionOpen(listener: (eventList: EventItem[]) => void) {
    streamingSubscription.open(listener);
  }

  /**
   * 新邮件流式订阅-取消
   */
  public newMailSubscriptionClose() {
    streamingSubscription.close();
  }

  /**
   * 获取联想邮箱地址
   *
   * @returns domainList
   */
  getDomainList(): string[] {
    return this.domainList;
  }

  check(): Promise<void> {
    return;
  }

  supportedFeatures(): StoreFeature[] {
    return [];
  }

  hasFeature(feature: StoreFeature): boolean {
    return false;
  }

  async sync(){
    // folderHierarchy 接口请求
    let folderHierarchyRes: FolderHierarchyResp;
    try{
      folderHierarchyRes = await this.folderHierarchy(FolderName.contacts,
        PropertySet.AllProperties);
    } catch (err) {
      return 'folderHierarchy error' as string;
    }
    // let syncState: string = folderHierarchyRes.syncState;
    let folders = folderHierarchyRes.folders?.map((item) => {
        return item.folder.folderId
    })

    // getFolder 接口请求
    try{
      return await this.getFolder(folders);
    } catch (err) {
      return 'getFolder error' as string;
    }
  }

  on(event: 'new-mail', handler: (folderFullName: string, mailIdList: string[]) => void);

  on(events: 'new-mail', handler: (eventList: EventItem[]) => void);

  on(event: string | EventType[], handler: Function) {
    // 订阅事件
    this.newMailSubscriptionInitiate(this.properties, [FolderName.inbox], [EventType.NewMailEvent]);

    // 流式拉取事件
    let callback: (eventList: EventItem[]) => void = (eventList) => {

      // handle传递事件
      handler(eventList);
    };
    this.newMailSubscriptionOpen(callback);
  }

  getMail(folderFullName: string, mailId: string): Promise<IBuffer> {
    return;
  }

  getMailPartContent(folderFullName: string, mailId: string, partId: string): Promise<IBuffer> {
    return;
  }

  /**
   *  获取邮件信息
   * @param folderFullName 邮件所在文件Id
   * @param mailId 邮件Id
   * @returns MailBasic
   */
  async getMailBasic(folderFullName: string, mailId: string): Promise<MailBasic> {
    if(folderFullName !== this.currentFolder) {
      await this.openFolder(folderFullName);
    }
    let getItem: GetItem = new GetItem();
    let folder: UniteId = parseFolder(mailId);
    getItem.itemIds = new Array<UniteId>();
    getItem.itemIds.push(folder);
    getItem.includeMimeContent = 'true';
    getItem.baseShape = PropertySet.AllProperties;
    getItem.fieldURI = FieldURISchema.Subject;

    let getItemTask: GetItemTask<GetItemMessageResponse, GetItemMessageResponseModel> = new GetItemTask(
      new GetItemRequest(getItem),
      this.properties,
      new GetItemMessageResponse(new GetItemMessageResponseModel())
    );
    let respList = await getItemTask.execute();

    if(!respList.isSuccessful()) {
      return Promise.reject("mail not found");
    }
    const result = respList.getData().responseMessages ;
    const resp = result[0] as ResponseMessage<Message>;
    if(resp?.items.length === 0) {
      return Promise.reject("fetch mail failed");
    }
    const data = parseMailBasicInfo(resp,mailId);
    return data;
  }

  getMailIndex(folderFullName: string, mailId: string, order: Order): Promise<number> {
    return;
  }

  setMailAttributes(folderFullName: string, mailId: string, attributes: MailAttribute[], modifyType: '' | '+' | '-'): Promise<MailAttribute[]> {
    return;
  }

  createSubFolder(folderName: string, parent: string): Promise<void> {
    return;
  }

  renameFolder(folderName: string, newName: string): Promise<void> {
    return;
  }

  deleteFolder(folderName: string): Promise<void> {
    return;
  }

  /**
   * 获取全部文件夹
   * @returns FolderData[]
   */
  async getAllFolderList(): Promise<FolderData[]> {
    let propertySet = PropertySet.AllProperties;
    let mailFolder: FolderHierarchy = new FolderHierarchy();
    mailFolder.propertySet = propertySet;
    let syncFolderHierarchyTask: SyncFolderHierarchyTask = new SyncFolderHierarchyTask(new SyncFolderHierarchyRequest(mailFolder), this.properties);
    let respList = await syncFolderHierarchyTask.execute();
    console.info('respList ===' + respList)
    if(!respList.isSuccessful()) {
      const text = "list folder failed " + respList.getRespCode();
      console.log("getAllFolderList", text);
      return Promise.reject(text);
    }
    const result = respList.getData().folders;
    const res = result.filter(resp => resp.folder.folderClass == 'IPF.Note').map(item => {
      const fullName = item.folder.folderId.id + ";" + item.folder.folderId.changeKey
      let name = item.folder.displayName;
      let parentPart = "";
      const fd: FolderData = {
        name,
        fullName,
        parent: parentPart,
        type: FolderType.other,
      };
      if(name === '收件箱') {
        fd.type = FolderType.inbox;
      } else if( name === '垃圾邮件') {
        fd.type = FolderType.spam;
      } else if(name === '已删除邮件') {
        fd.type = FolderType.trash;
      } else if(name === '已发送邮件'  || name === '发件箱') {
        fd.type = FolderType.sent;
      }
      return fd;
    })
    return res;
  }

 async getFolderList(parent: string): Promise<FolderData[]> {
    if(parent == "") {
      return await this.getAllFolderList();
    }
    return Promise.resolve([] as FolderData[]);
  }

  async getFolderInfo(folderName: string): Promise<FolderData> {
    return;
  }

  /**
   * 获取文件名
   * @param folderName
   * @returns FolderData
   */
  async openFolder(folderName: string): Promise<FolderData> {
    let uniteId:UniteId = parseFolder(folderName);
    const folderId:FolderId  = new FolderId(false);
    folderId.folderId = uniteId;
    let folder: GetFolder = new GetFolder();
    folder.baseShape = PropertySet.AllProperties;
    folder.folderIds = [folderId];
    let getFolderTask: GetFolderTask = new GetFolderTask(new GetFolderRequest(folder), this.properties);
    const folderData: FolderData = {
      name: folderName,
      fullName: folderName,
      unreadCount: 0,
    };
    const respList = await getFolderTask.execute();
    if(!respList.isSuccessful()) {
      return Promise.reject("select folder failed " + respList.getRespCode());
    }
    const respLists = respList.getData().responseMessages;
    let result = respLists[respLists.length-1] as ResponseMessage<Folder>;
    folderData.fullName = result.items[0].folderId.id + ";" + result.items[0].folderId.changeKey;
    folderData.name = result.items[0].displayName;
    this.currentFolder = folderName;
    return folderData;
  }

  closeFolder(folderName: string): Promise<void> {
    return;
  }

  isFolderOpen(folderName: string): boolean {
    return;
  }

  getFolderMailIdList(folderName: string, start: number, size: number, order: Order): Promise<string[]> {
    return;
  }

  moveMail(fromFolderFullName: string, mailId: string, toFolderFullName: string): Promise<string> {
    return;
  }

  copyMail(fromFolderFullName: string, mailId: string, toFolderFullName: string): Promise<string> {
    return;
  }

  addMail(mail: string, folderName: string): Promise<string> {
    return;
  }

  deleteMail(fromFolder: string, mailId: string): Promise<void> {
    return;
  }

  filterMail(filter: Filter): Promise<Map<string, string[]>> {
    return;
  }

  getMailRaw(folderFullName: string, mailId: string): Promise<IBuffer> {
    throw new Error('Method not implemented.');
  }

  getMailHeader(folderFullName: string, mailId: string): Promise<MailHeader> {
    throw new Error('Method not implemented.');
  }

  /**
   * 邮件查询
   * @param query
   * @returns string[]
   */
  async searchMail(query: Query): Promise<string[] > {
    if(!query.folder) {
      return [] as string[];
    }
    let paginateAttribute: IItemView = {
      basePoint: BasePoint.Beginning,
      offset: '0'
    };
    // 设置自定义属性集
    //设置排序
    let SortOrderValue: ISortOrder = {
      orderBy:SortOrder.Descending,
      FieldURI: FieldURISchema.DateTimeReceived
    };
    //设置View
    let View: SearchView = new SearchView();
    View.itemView = paginateAttribute;
    View.orderBy = SortOrderValue;
    View.baseShape = PropertySet.IdOnly;
    let fieldURIArrValue = [FieldURISchema.Subject,FieldURISchema.ItemClass];
    View.fieldURIArr = fieldURIArrValue;

    let fields = query.fields;
    let searchFilters: Filters[] = [];
    if(fields.subject) {
      let subjectFilter: ContainsSubstring = new SearchMailFilter.ContainsSubstring(FieldURISchema.Subject, fields.subject, ContainmentMode.Substring, ComparisonMode.IgnoreCase);
      searchFilters.push(subjectFilter);
    } else if(fields.from) {
      let fromFilter: ContainsSubstring = new SearchMailFilter.ContainsSubstring(FieldURISchema.From, fields.from, ContainmentMode.Substring, ComparisonMode.IgnoreCase);
      searchFilters.push(fromFilter);
    } else if (fields.to) {
      let senderFilter: ContainsSubstring = new SearchMailFilter.ContainsSubstring(FieldURISchema.Sender,fields.to, ContainmentMode.Substring, ComparisonMode.IgnoreCase);
      searchFilters.push(senderFilter);
    }
    let compositeFilter:Composite = new SearchMailFilter.Composite(LogicalOperator.Or,searchFilters);
    let findItemValue = new FindItems<Composite>();
    const i = query.folder.lastIndexOf(";");
    if (i > 0) {
      const  folderId:UniteId = {
        id : query.folder.substring(0,i),
        changeKey : query.folder.substring(i+1),
      }
      findItemValue.folder = folderId;
    }
    findItemValue.filter = compositeFilter;
    findItemValue.view = View;
    let searchMailTask: SearchMailTask = new SearchMailTask(new SearchMailRequest(findItemValue), this.properties);
    const respList = await searchMailTask.execute();
    const result = respList.getData().findItemMessages;
    if(!result) {
      return [];
    }
    //IPM.Note 表示为电子邮件
    return result.filter(resp => resp.itemClass == 'IPM.Note').map(item=> {
      return item?.itemId.id + ";" + item?.itemId.changeKey;
    });
  }

  release?() {
  }

  setBufferCreator(creator: IBufferCreator) {
  }

  async folderHierarchy(folderName:FolderName, propertySet: PropertySet): Promise<FolderHierarchyResp> {
    return new Promise((resolve, reject) => {
      // 传内置文件夹
      let defaultId: DistinguishedFolderId = new DistinguishedFolderId();
      defaultId.id = folderName;
      let syncFolderId: FolderId = new FolderId(true);
      syncFolderId.folderId = defaultId;
      // 构造请求数据
      let mailFolder: FolderHierarchy = new FolderHierarchy();
      mailFolder.syncFolderId = syncFolderId;
      mailFolder.propertySet = propertySet;
      let syncFolderHierarchyTask: SyncFolderHierarchyTask = new SyncFolderHierarchyTask(
        new SyncFolderHierarchyRequest(mailFolder), this.properties)
      syncFolderHierarchyTask.execute().then(data => {
        resolve(data.getData());
      }).catch((err)=> {
        reject(err);
      });
    })
  }

  async syncFolderItems(uniteId: FolderId, propertySet: PropertySet, ignoredItemIds: UniteId[],
                        maxChangesReturned: number): Promise<FolderItemsResp> {
    let folderItems: FolderItems = new FolderItems();
    folderItems.syncFolderId = uniteId;
    folderItems.propertySet = propertySet;
    folderItems.ignoredItemIds = ignoredItemIds;
    folderItems.maxChangesReturned = maxChangesReturned;

    let syncFolderItemsTask: SyncFolderItemsTask = new SyncFolderItemsTask(new SyncFolderItemsRequest(folderItems), this.properties)
    const res = await syncFolderItemsTask.execute();
    return res.getData();
  }

  // includeMimeContent:string, // fieldURI: FieldURISchema, // indexFiledURL:IndexFiledURL, // extendedFieldURI:ExtendedFieldURI
  async getItem(itemIds: UniteId[], baseShape: PropertySet): Promise<ResponseMessage<Message>[]> {
    let getItem: GetItem = new GetItem();
    getItem.itemIds = itemIds;
    getItem.baseShape = baseShape;
    // getItem.includeMimeContent = includeMimeContent;
    // getItem.fieldURI = fieldURI;
    // getItem.indexFiledURL = indexFiledURL;
    // getItem.extendedFieldURI = extendedFieldURI;
    let getItemTask: GetItemTask<GetItemMessageResponse, GetItemMessageResponseModel> = new GetItemTask(
      new GetItemRequest(getItem),
      this.properties,
      new GetItemMessageResponse(new GetItemMessageResponseModel())
    );
    const res = await getItemTask.execute()
    let responseMessages: ResponseMessage<Message>[] = res.getData().responseMessages;
    responseMessages.forEach(item => {
      item.items.forEach(message => {
        logger.info(JSON.stringify(message));
      });
    });
    return responseMessages
  }

  async getFolder(folderIds: UniteId[]): Promise<GetFolderResp> {
    // let folderIds: FolderId[] = distinguishedFolderIds.map((item) => {
    //   let folder: FolderId = new FolderId();
    //   folder.folderId = item;
    //   return folder;
    // })
    //

    // folder.folderId = noFolderId;
    let folders = folderIds.map((folderId) => {
      let folder: FolderId = new FolderId(false);
      folder.folderId = folderId;
      return folder;
    })
    // folderIds.push(folder);
    let getFolder: GetFolder = new GetFolder();
    getFolder.baseShape = PropertySet.AllProperties;
    getFolder.folderIds = folders;

    let getFolderTask: GetFolderTask = new GetFolderTask(new GetFolderRequest(getFolder), this.properties);
    let res = await getFolderTask.execute();
    return res.getData();
  }

  async updateItem(itemChanges: ChangeItem[],
                   itemId: UniteId,
                   isRead: string,
                   itemFields: ItemField[],
                   itemOperateType: ItemOperateType,
                   messageDisposition: MessageDisposition): Promise<UpdateItemResponse> {
    let message = new Message();
    message.isRead = isRead;
    let itemField: ItemField = {
      operateType: itemOperateType,
      fieldURI: 'message:IsRead',
      item: message,
    }
    itemFields.push(itemField);
    let items = new ChangeItem();
    items.itemId = itemId;
    items.itemFields = itemFields;
    itemChanges.push(items);
    let updateItems = new UpdateItem();
    updateItems.itemChanges = itemChanges;
    updateItems.messageDisposition = messageDisposition;
    let updateItemTask: UpdateItemTask = new UpdateItemTask(new UpdateItemRequest(updateItems), this.properties);
    return await updateItemTask.execute();
  }

  async deleteItem(sendMeetingCancellations:SendCancellationsType, affectedTaskOccurrence:AffectedTaskOccurrence,
                   uniteId:UniteId): Promise<DeleteItemResponse> {
      let deleteItem: DeleteItem = new DeleteItem();
      deleteItem.sendMeetingCancellations = sendMeetingCancellations;
      deleteItem.affectedTaskOccurrence = affectedTaskOccurrence;
      deleteItem.itemIds = [uniteId];
      let deleteItemTask: DeleteItemTask = new DeleteItemTask(new DeleteItemRequest(deleteItem), this.properties)
      return await deleteItemTask.execute();
  }

  /**
   * 创建附件
   *
   * @param parentItemId 邮件的ID
   * @param attachments 需要添加的附件
   * @returns 创建成功的返回数组对象
   */
  createAttachments(parentItemId : string, attachments: BaseAttachments[]): Promise<CreateAttachmentResponse>{
    let createAttachment = new CreateAttachment();
    createAttachment.parentItemId = parentItemId;
    attachments.forEach((item) => {
      createAttachment.addAttachments(item);
    })
    return new Promise((resolve,reject) => {
      let createAttachmentTask: CreateAttachmentTask = new CreateAttachmentTask(
        new CreateAttachmentRequest(createAttachment), this.properties);
      createAttachmentTask.execute().then(data => {
        resolve(data);
      }).catch((err) => {
        reject(err)
      });
    })
  }

  /**
   * 删除附件
   *
   * @param attachmentIds 删除的附件ID
   * @returns 删除的返回结果数组对象
   */
  deleteAttachments(attachmentIds: string[]): Promise<DeleteAttachmentResponse> {
    let deleteAttachment = new DeleteAttachment();
    deleteAttachment.attachmentIds = attachmentIds;
    return new Promise((resolve,reject) => {
      let deleteAttachmentTask: DeleteAttachmentTask = new DeleteAttachmentTask(
        new DeleteAttachmentRequest(deleteAttachment), this.properties);
      deleteAttachmentTask.execute().then(data => {
        resolve(data);
      }).catch((err) => {
        reject(err)
      });
    })
  }

  /**
   * 获取附件
   *
   * @param attachmentIds 需要获取附件的ID
   * @returns 获取附件的数组对象
   */
  getAttachment(attachmentIds: string[]): Promise<GetAttachmentResponse> {
    let getAttachment = new GetAttachment();
    getAttachment.attachmentIds = attachmentIds;
    return new Promise((resolve,reject) => {
      let getAttachmentTask: GetAttachmentTask = new GetAttachmentTask(
        new GetAttachmentRequest(getAttachment), this.properties);
      getAttachmentTask.execute().then(data => {
        resolve(data);
      }).catch((err) => {
        reject(err)
      });
    })
  }

  /**
   * 下载附件
   *
   * @param attachmentId 需要下载附件的ID
   * @param sagePath 附件下载保存的路径
   * @param onDataReceive 获取进度条CallBack函数
   * @returns 附件下载保存”路径/名称.后缀“
   */
  downLoadAttachment(attachmentId: string,onDataReceive: OnProgress, sagePath: string,
                     attachmentSize: number): Promise<string>{
    return new Promise((resolve,reject) => {
      const downLoadManager = new DownLoadManager();
      downLoadManager.downLoad(attachmentId ,this.properties, sagePath, onDataReceive, attachmentSize).then((data) => {
        resolve(data)
      }).catch((err) => {
        reject(err)
      });
    })
  }

  /**
   * 取消下载
   *
   * @param attachmentId 需要取消下载附件的ID
   * @returns 空
   */
  cancelDownLoadAttachment(downLoadManager: DownLoadManager, attachmentId: string): Promise<void> {
    downLoadManager?.cancelDownLoad(attachmentId);
    return;
  }
}