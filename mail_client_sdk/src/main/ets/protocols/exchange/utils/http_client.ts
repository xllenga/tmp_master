/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BusinessError } from '@ohos.base';
import rcp from '@hms.collaboration.rcp';
import { http } from '@kit.NetworkKit';
import { ExchangeCredentials } from './exchange_credentials';
import { getLogger, Logger } from '../../../utils/log';
import { ClientErrorCode, ErrorType, ExchangeError } from '../error/exchange_error';

const logger: Logger = getLogger('HttpClient');
const USER_AGENT_VALUE: string = 'libcurl-agent/1.0';

/**
 * Http请求日志级别
 */
export enum LogLevel {
  NONE = 0,
  HEAD = 1,
  BODY = 2,
}

/**
 * 媒体类型
 */
export enum MediaType {
  TEXT_XML = 'text/xml',
  TEXT_XML_UTF8 = 'text/xml; charset=utf-8',
  APPLICATION_JSON = 'application/json',
  APPLICATION_JSON_UTF8 = 'application/json;charset=utf-8',
}

/**
 * Exchange Http请求客户端。
 */
export interface ExchangeHttpClient {
  /**
   * 设置请求URL。
   *
   * @param url 请求的目标服务器url。
   * @returns { HttpClient } Http客户端实例。
   */
  setUrl(url: string): ExchangeHttpClient;

  /**
   * 设置是否校验服务器证书。
   *
   * @param { boolean } isSslVerify  true校验证书，否则不校验证书。
   * @returns { HttpClient } Http客户端实例。
   */
  enableSslVerify(isSslVerify: boolean): ExchangeHttpClient;

  /**
   * 设置ca证书路径。
   *
   * @param path  ca证书沙箱路径。
   * @returns { HttpClient } Http客户端实例。
   */
  setCaPath(path: string): ExchangeHttpClient;

  /**
   * 设置请求头，如: {'Content-Type': 'application/json; charset=utf-8', 'Accept': 'application/json'}。
   *
   * @param header 请求头
   * @returns { HttpClient } Http客户端实例。
   */
  setHeader(header: Object): ExchangeHttpClient;

  /**
   * 设置Exchange认证凭据。
   *
   * @param { ExchangeCredentials } credentials 凭据。
   * @returns { HttpClient } Http客户端实例。
   */
  setExchangeCredentials(credentials?: ExchangeCredentials): ExchangeHttpClient;

  /**
   * 取消请求。
   *
   * @param requestToCancel 取消请求列表。
   */
  cancel(requestToCancel?: rcp.Request | rcp.Request[]): void;

  /**
   * 释放资源
   */
  destroy(): void;

  /**
   * 发起http请求。
   *
   * @param { rcp.Request } request 请求。
   * @returns { rcp.Response } 响应。
   */
  request(request?: rcp.Request): Promise<rcp.Response>;

  /**
   * 发起http请求。
   *
   * @param { rcp.RequestContent } data 请求体数据。
   * @param { rcp.OnDataReceive } onDataReceive 数据接收回调
   * @returns { rcp.Response } 响应。
   */
  request(data?: rcp.RequestContent, onDataReceive?: rcp.OnDataReceive): Promise<rcp.Response>;
}

/**
 * Http请求工具基类。
 *
 * @since 2024-04-02
 */
export class HttpRequest implements ExchangeHttpClient {
  private static readonly CONNECT_TIMEOUT = 6_000;
  private static readonly READ_TIMEOUT = 15_000;
  protected session: rcp.Session | null = null;
  private _url: string;
  private _isSslVerify: boolean = true;
  private _caPath: string = '';
  private _headers: rcp.RequestHeaders;
  private _credentials: ExchangeCredentials | null = null;
  private logLevel: LogLevel = LogLevel.BODY;

  constructor() {
    this._headers = {
      'content-type': MediaType.TEXT_XML_UTF8,
      'accept': MediaType.TEXT_XML,
      'user-agent': USER_AGENT_VALUE,
    };
  }

  public setExchangeCredentials(credentials: ExchangeCredentials = null): ExchangeHttpClient {
    this._credentials = credentials;
    return this;
  }

  public setUrl(url: string): ExchangeHttpClient {
    this._url = url ?? '';
    return this;
  }

  public enableSslVerify(isSslVerify: boolean = true): ExchangeHttpClient {
    this._isSslVerify = isSslVerify;
    return this;
  }

  public setCaPath(path: string = null): ExchangeHttpClient {
    this._caPath = path;
    return this;
  }

  public setHeader(header: Object = {}): ExchangeHttpClient {
    Object.keys(header).forEach(key => {
      this._headers[key] = header[key];
    });
    return this;
  }

  /**
   * 设置请求显示日志级别。
   *
   * @param { LogLevel } level
   * @returns { HttpRequestBase } 请求实例。
   */
  public setLogLevel(level: LogLevel): ExchangeHttpClient {
    this.logLevel = level;
    return this;
  }

  public destroy(): void {
    this.session?.cancel();
    this.session?.close();
    this.session = null;
  }

  public cancel(requestToCancel?: rcp.Request | rcp.Request[]): void {
    this.session?.cancel(requestToCancel);
  }

  public async request(requestOrData?: rcp.Request | rcp.RequestContent,
                       onDataReceive?: rcp.OnDataReceive): Promise<rcp.Response> {
    if (!this.session) {
      this.session = rcp.createSession();
    }
    let request: rcp.Request = new rcp.Request(this._url);
    let config: rcp.Configuration = this.configuration(onDataReceive);
    this.onConfiguration(config);
    request.configuration = config;
    request.content = requestOrData;
    request.headers = { ...this._headers };
    request.method = 'POST';
    if (requestOrData instanceof rcp.Request) {
      request = requestOrData;
    }

    // 请求中添加认证信息
    this._credentials?.authenticate(request);
    this.showLog(`${request.method} --> ${request.url} [id=${request.id}]`, LogLevel.HEAD);
    Object.keys(request.headers).forEach(key => {
      this.showLog(`${key}: ${request.headers[key]}\n`, LogLevel.HEAD);
    });
    this.showLog(`${request.content}`, LogLevel.BODY);
    let response: rcp.Response | null = null;
    try {
      response = await this.session.fetch(request);
      this.showLog(`${request.method} ${response.statusCode} <-- ${request.url} [id=${request.id}]`, LogLevel.HEAD);
      this.showLog(response.toString(), LogLevel.BODY);
    } catch (err) {
      let origin: BusinessError = err as BusinessError;
      if (!origin.code) {
        logger.error(`request, an unknown error has occurred, ${origin.message}`);
        throw new ExchangeError(ErrorType.CLIENT_ERROR, err.message, ClientErrorCode[ClientErrorCode.UNKOWN_ERROR]);
      }
      logger.error(`request, error occurred, code: ${origin.code}`);
      throw this.handleHttpError(err);
    }
    if (!response) {
      throw new ExchangeError(ErrorType.CLIENT_ERROR, 'The http response is a null object.',
        ClientErrorCode[ClientErrorCode.UNKOWN_ERROR]);
    }

    // Http状态码大于等于200并且小于400，或者状态码等于500正常返回响应内容，否则返回错误码。
    if ((response.statusCode >= http.ResponseCode.OK && response.statusCode < http.ResponseCode.BAD_REQUEST) ||
      response.statusCode === http.ResponseCode.INTERNAL_ERROR) {
      return response;
    } else {
      throw new ExchangeError(ErrorType.HTTP_ERROR, 'Http response status error.', response.statusCode);
    }
  }

  /**
   * 配置时修改配置内容。
   *
   * @param config { rcp.Configuration } 请求配置。
   */
  protected onConfiguration(config: rcp.Configuration): void {
    config.transfer = {
      autoRedirect: true,
      timeout: {
        connectMs: HttpRequest.CONNECT_TIMEOUT,
        transferMs: HttpRequest.READ_TIMEOUT,
      },
    };
  }

  /**
   * 生成配置。
   *
   * @param onDataReceiveCallback 数据接收回调。
   * @returns { rcp.SessionConfiguration } 配置实例。
   */
  private configuration(onDataReceive?: rcp.OnDataReceive): rcp.Configuration {
    let targetRemoteValidation: 'system' | 'skip' | rcp.CertificateAuthority = 'system';
    if (!this._isSslVerify) {
      targetRemoteValidation = 'skip';
    } else {
      targetRemoteValidation = this._caPath?.match(/\.(pem)$/) ? { filePath: this._caPath } : 'system';
    }
    let config: rcp.Configuration = {
      tracing: {
        httpEventsHandler: {
          onDataReceive: onDataReceive,
        },
        verbose: true,
      },
      security: {
        remoteValidation: targetRemoteValidation,
      },
    };
    return config;
  }

  /**
   * 显示请求日志。
   *
   * @param data 日志数据。
   * @param { LogLevel } level 级别。
   */
  private showLog(data: string, level: LogLevel): void {
    if (this.logLevel < level) {
      return;
    }
    if (data === '') {
      return;
    }
    logger.log(data);
  }

  /**
   * 处理HTTP请求异常。
   * RCP错误码：1007900000
   * 通用错误码: 201、202、401、801
   * @param { BusinessError } rawError 原始异常。
   * @returns { ExchangeError } exchange异常实例对象。
   */
  private handleHttpError(rawError: BusinessError): ExchangeError {
    // 正则匹配RCP错误码
    let type: ErrorType = `${rawError.code}`.match(/^10079\d{5}$/) ? ErrorType.HTTP_ERROR : ErrorType.SYS_COMMON_ERROR;
    return new ExchangeError(type, 'An error occurs in the request.', rawError.code);
  }
}

/**
 * 流式请求
 */
export class HttpStreamRequest extends HttpRequest {
  private static readonly STREAM_CONNECT_TIMEOUT = 6_000;
  private static readonly STREAM_READ_TIMEOUT = 1_800_000;

  protected onConfiguration(config: rcp.Configuration): void {
    config.transfer = {
      autoRedirect: true,
      timeout: {
        connectMs: HttpStreamRequest.STREAM_CONNECT_TIMEOUT,
        transferMs: HttpStreamRequest.STREAM_READ_TIMEOUT,
      },
    };
  }
}

export default new HttpRequest();

export const httpStreamRequest: ExchangeHttpClient = new HttpStreamRequest();