/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { JsObjectElement, XmlElement } from '../xml';

/**
 * 字符串首字母小写
 *
 * @param 需要处理的字符串
 * @returns 处理后的字符串
 */
export function lowercaseFirstLetter(string: string): string {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

/**
 * 获取响应信息
 *
 * @param 响应报文转换的json对象
 * @param 对应请求的响应标签
 * @returns 对应请求的根标签对象
 */
export function getResponseMessage(bodyElement: JsObjectElement, xmlElement: XmlElement): JsObjectElement[] {
  let elementsName = bodyElement._name;
  let jsonElement = bodyElement._elements[0];
  let responseMessage: JsObjectElement[] = [];
  while (elementsName != xmlElement) {
    jsonElement = jsonElement._elements[0];
    elementsName = jsonElement._elements[0]._name;
  }
  responseMessage = jsonElement._elements[0]._elements;
  return responseMessage;
}

/**
 * 解析特定类型的属性
 *
 * @param properties 属性值数据
 * @param specProp 特定属性类型对象
 * @returns 特定属性类型对象
 */
export function readSpecProp<T>(properties: JsObjectElement[], specProp: T): T {
  properties.forEach(property => {
    let name = property._name;
    let value = property._elements[0]._text;
    specProp[lowercaseFirstLetter(name)] = value;
  })
  return specProp;
}