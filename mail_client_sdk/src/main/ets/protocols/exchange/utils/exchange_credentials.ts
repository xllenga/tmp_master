/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import rcp from '@hms.collaboration.rcp';
import { ClientErrorCode, ErrorType, ExchangeError } from '../error/exchange_error';

/**
 * 登录方式枚举
 *
 * @since 2024-03-19
 */
export enum AuthType {
  /**
   * Basic登录
   */
  BASIC = 'Basic',

  /**
   * Ntlm登录
   */
  NTLM = 'Ntlm',

  /**
   * OAuth登录
   */
  OAUTH = 'OAuth',
}

/**
 * Exchange凭据用来在请求中添加认证信息。
 *
 * @since 2024-03-19
 */
export interface ExchangeCredentials {
  /**
   * 请求中添加认证信息。
   *
   * @param { rcp.Request } request 应进行身份验证的请求。
   */
  authenticate(request: rcp.Request): void;
}

/**
 * BasicCredentials类,basic的登录方式
 *
 * @since 2024-03-19
 */
export class BasicCredentials implements ExchangeCredentials {
  /**
   * basic登录构造器
   *
   * @param userName 用户名
   * @param password 用户密码
   */
  public constructor(private userName: string, private password: string) {
    if (!userName || !password) {
      throw new ExchangeError(ErrorType.CLIENT_ERROR, 'userName or passWord is null.',
        ClientErrorCode[ClientErrorCode.USERNAME_OR_PASSWORD_IS_NULL]);
    }
  }

  authenticate(request: rcp.Request): void {
    const serverAuth: rcp.ServerAuthentication = {
      authenticationType: 'basic',
      credential: {
        username: this.userName,
        password: this.password,
      }
    }
    request.configuration.security.serverAuthentication = serverAuth;
  }
}

/**
 * OAuthCredentials类,OAuth的登录方式
 *
 * @since 2024-03-19
 */
export class OAuthCredentials implements ExchangeCredentials {
  /**
   * OAuth登录构造器
   *
   * @param token 请求token
   */
  public constructor(private token: string) {
  }

  authenticate(request: rcp.Request): void {
    request.headers = {
      ...request.headers,
      'authorization': `Bearer ${this.token}`
    }
  }
}

/**
 * NtlmCredentials类,Ntlm的登录方式
 *
 * @since 2024-03-19
 */
export class NtlmCredentials implements ExchangeCredentials {
  /**
   * Ntlm登录构造器
   *
   * @param userName 用户名
   * @param password 用户密码
   */
  public constructor(private userName: string, private password: string) {
    if (!userName || !password) {
      throw new ExchangeError(ErrorType.CLIENT_ERROR, 'userName or passWord is null.',
        ClientErrorCode[ClientErrorCode.USERNAME_OR_PASSWORD_IS_NULL]);
    }
  }

  authenticate(request: rcp.Request): void {
    const serverAuth: rcp.ServerAuthentication = {
      authenticationType: 'ntlm',
      credential: {
        username: this.userName,
        password: this.password,
      }
    }
    request.configuration.security.serverAuthentication = serverAuth;
  }
}

/**
 * 凭据储存器
 *
 * @since 2024-03-28
 */
export class CredentialsStore {
  private static INSTANCE: CredentialsStore;
  private _credentials: ExchangeCredentials | null = null;

  /**
   * 单例实现私有化构造器
   */
  private constructor() {
  }

  /**
   * 返回单例实例
   *
   * @returns CredentialsHeaderStore实例
   */
  public static getInstance(): CredentialsStore {
    if (!CredentialsStore.INSTANCE) {
      CredentialsStore.INSTANCE = new CredentialsStore();
    }
    return CredentialsStore.INSTANCE;
  }

  /**
   * 设置凭据。
   *
   * @param { ExchangeCredentials } credentials 凭据。
   */
  public set credentials(credentials: ExchangeCredentials) {
    this._credentials = credentials;
  }

  /**
   * 返回凭据实例。
   *
   * @returns { ExchangeCredentials } 凭据实例对象，如果没有返回null.
   */
  public get credentials(): ExchangeCredentials | null {
    return this._credentials;
  }
}
