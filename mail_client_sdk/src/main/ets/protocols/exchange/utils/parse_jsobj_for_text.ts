/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { JsObjectElement, XmlElement } from '../xml';

const MAX_DEEPS: number = 20;

// 返回值表示是否找到元素
export type LoopFunction = (tagInstance: JsObjectElement, depth: number) => boolean;

function setResponseData(responseData: Map<string, string>, response: JsObjectElement, depth: number): void {
  if (!response._elements?.length || depth > MAX_DEEPS) {
    return;
  }
  if (response._elements.length === 1 && response._elements[0]._type === XmlElement.NAME_TEXT) {
    responseData.set(response._name, response._elements[0]._text);
  } else {
    for (const element of response._elements) {
      setResponseData(responseData, element, depth++);
    }
  }
}

/**
 * 解析 xml的 jsObject 对象，保存 tag 及 text 到一个 Map；
 * 注意：用于解析简单值，不能用于解析 list 结果；
 *
 * @param bodyElement xml的body层jsObject
 * @returns 所有文本值得map
 */
export function parseResponse(bodyElement: JsObjectElement): Map<string, string> {
  const responseData: Map<string, string> = new Map<string, string>();
  const response: JsObjectElement = bodyElement?._elements?.[0];
  if (response) {
    let depth = 0;
    setResponseData(responseData, response, depth);
  }
  return responseData;
}

/**
 * 遍历 xml的 jsObject 节点树对象；
 *
 * @param el 节点对象,从改节点向下遍历
 * @param func 回调函数,第一个参数为节点对象,第二个为depth
 * @param skipCurrentLevel 跳过当前层节点
 * @returns void
 */
export function traverseElementTree(
  el: JsObjectElement | JsObjectElement[], func: LoopFunction, skipCurrentLevel = false
): void {
  let currentLevel: JsObjectElement[] = Array.isArray(el) ? el : [el];
  if (skipCurrentLevel) {
    currentLevel = currentLevel.reduce((result, el) => {
      return el._elements ? [...result, ...el._elements] : result;
    }, []);
  };
  let saveLevel: JsObjectElement[] = [];
  let depth = 0;
  // 标识是否找到元素，如果找到遍历完这层节点就退出
  let isFind = false;

  let i = 0;
  while ( i < currentLevel.length ) {
    const elObj = currentLevel[i];
    isFind = func(elObj, depth);
    if (elObj._elements && elObj._elements.length) {
      saveLevel = saveLevel.concat(elObj._elements);
    };
    i++;
    if (i === currentLevel.length) {
      if (isFind) { break; };
      currentLevel = saveLevel;
      saveLevel = [];
      i = 0;
      depth += 1;
    };
  };
}

/**
 * 根据传入的elName查找节点；
 *
 * @param el 节点对象或节点对象数组,从该节点向下遍历
 * @param elName 要查找节点的name
 * @param skipCurrentLevel 跳过当前层节点
 * @returns 节点数组
 */
export function findElements(el: JsObjectElement, elName: string, skipCurrentLevel?: boolean): JsObjectElement[]
export function findElements(el: JsObjectElement[], elName: string, skipCurrentLevel?: boolean): JsObjectElement[]
export function findElements( el: JsObjectElement | JsObjectElement[], elName: string, skipCurrentLevel = false ): JsObjectElement[] {
  const result: JsObjectElement[] = [];
  const loopCallback = (xmlJsObj) => {
    if (xmlJsObj._name === elName) {
      result.push(xmlJsObj);
      return true;
    }
    return false;
  }
  traverseElementTree(el, loopCallback, skipCurrentLevel);
  return result;
}

/**
 * 根据传入的elNameList查找多个节点；
 *
 * @param el 节点对象或节点对象数组,从该节点向下遍历
 * @param elNameList 要查找节点的name列表
 * @param skipCurrentLevel 跳过当前层节点
 * @returns 对象key为节点名，value为查找的节点数组
 */
export function findMultiElements(el: JsObjectElement,  elNameList: string[], skipCurrentLevel?: boolean): Record<string, JsObjectElement[]>
export function findMultiElements(el: JsObjectElement[],  elNameList: string[], skipCurrentLevel?: boolean): Record<string, JsObjectElement[]>
export function findMultiElements(
  el: JsObjectElement | JsObjectElement[], elNameList: string[], skipCurrentLevel = false
): Record<string, JsObjectElement[]> {
  const result: Record<string, JsObjectElement[]> = {};
  for (const resultKey of elNameList) {
    result[resultKey] = [];
  };
  const loopCallback = (xmlJsObj) => {
    if (elNameList.includes(xmlJsObj._name)) {
      result[xmlJsObj._name].push(xmlJsObj)
      return true;
    };
    return false;
  };
  traverseElementTree(el, loopCallback, skipCurrentLevel);
  return result;
}
