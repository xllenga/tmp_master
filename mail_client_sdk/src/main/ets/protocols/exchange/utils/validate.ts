/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * 校验条件，是否必须、字符串最大最小长度、数值最大最小值
 * */
interface ValidCondition<T> {
  require?: boolean;
  maxLength?: number;
  minLength?: number;
  max?: number;
  min?: number;
  customValidateFunction?: (param: T[keyof T], T) => string | undefined;
}

type ValidateFields<T> = {
  [k in keyof T]?: ValidCondition<T>
};

type ValidResult<T> = {
  [k in keyof T]?: string[]
};

/*
 * 校验结果对象
 * */
interface ValidateResult {
  hasError(): boolean;
  getValidationError(): string;
}

class MessageValidateResult<T> implements ValidateResult {

  public readonly result: ValidResult<T> = {};

  private _hasError: boolean;
  public messageIndex: number = 0;

  /*
   * 是否有校验错误
   * */
  public hasError() {
    if (typeof this._hasError === 'boolean') {
      return this._hasError;
    } else {
      this._hasError = Object.keys(this.result).length !== 0;
      return this._hasError;
    }
  }

  /*
   * 获取校验错误
   * */
  public getValidationError(): string {
    let errString = '';
    for (const errItem of Object.entries(this.result)) {
      const field = errItem[0];
      const errList = errItem[1] as string[];
      errString += `[${field}]: ${errList.join(', ')}\r\n`
    };
    return errString;
  }
}

class MessagesValidateResult<T> implements ValidateResult {

  constructor(private validateList: MessageValidateResult<T>[]) {}

  private _hasError: boolean;

  public hasError() {
    if (typeof this._hasError === 'boolean') {
      return this._hasError;
    } else {
      this._hasError = this.validateList.some(ins => ins.hasError());
      return this._hasError;
    }
  }

  public getValidationError(): string | null {
    return this.hasError() ? this.validateList.reduce((errMsg, ins) => {
      return errMsg += `Message[${ins.messageIndex}] ${ins.getValidationError()}\r\n`;
    }, '') : null
  }

}

class MessagesValidator<T> {

  private validFields: ValidateFields<T> = {};

  constructor(private messageInstance: T[]) {}

  /*
   * 添加校验条件
   * */
  public addValidFields(fields: ValidateFields<T>): this {
    this.validFields = { ...this.validFields, ...fields }
    return this;
  }

  /*
   * 校验string值
   * */
  private validStringValue(field: string, val: string, condition: ValidCondition<T>): string[] {
    const errMsg: string[] = [];
    if (condition.minLength !== undefined && condition.minLength >= 0) {
      if (val.length < condition.minLength) {
        errMsg.push(`The length of ${field} must be greater than ${condition.minLength}.`)
      }
    }
    if (condition.maxLength !== undefined && condition.maxLength >= 0) {
      if (val.length > condition.maxLength) {
        errMsg.push(`The length of ${field} must be less than ${condition.maxLength}.`)
      }
    }
    return errMsg;
  }

  /*
   * 校验number值
   * */
  private validNumberValue(field: string, val: number, condition: ValidCondition<T>): string[] {
    const errMessage: string[] = [];
    if (condition.min !== undefined) {
      if (val < condition.min) {
        errMessage.push(`The value of ${field} must be greater than ${condition.min}.`)
      }
    }
    if (condition.max !== undefined) {
      if (val > condition.max) {
        errMessage.push(`The value of ${field} must be less than ${condition.max}.`)
      }
    }
    return errMessage;
  }

  private validateSingle(instance: T, field: keyof T, condition: ValidCondition<T>): MessageValidateResult<T> {
    const msgRes = new MessageValidateResult<T>();
    const messageValue = instance[field];
    const errMessage: string[] = [];
    if(isNil(messageValue)) {
      if (condition.require) {
        errMessage.push(`${String(field)} is required!`);
      };
    } else {
      if (typeof messageValue === 'string') {
        errMessage.push(...this.validStringValue(String(field), messageValue, condition));
      };
      if (typeof messageValue === 'number') {
        errMessage.push(...this.validNumberValue(String(field), messageValue, condition));
      };
    }
    if (condition.customValidateFunction) {
      const result = condition.customValidateFunction(messageValue, instance);
      result && errMessage.push(result)
    };
    if (errMessage.length) {
      msgRes.result[field] = errMessage;
    };
    return msgRes;
  }

  /*
   * @returns 返回校验结果对象,提供了hasError、getValidationError方法
   * */
  public validate(): ValidateResult {
    const resultList: MessageValidateResult<T>[] = [];
    let index = 0;
    for (const instance of this.messageInstance) {
      for (const item of Object.entries(this.validFields)) {
        const msgRes = this.validateSingle(instance, item[0] as keyof T, item[1])
        msgRes.messageIndex = index;
        msgRes.hasError() && resultList.push(msgRes);
        index += 1;
      }
    }
    return new MessagesValidateResult(resultList);
  }
}

/*
 * 创建校验器
 * @param messageInstance 校验对象
 * @param validFields 校验条件
 * */
export const createMessageValidator = <T>(
  messageInstance: T | T[], validFields: ValidateFields<T>
): MessagesValidator<T> => {
  let validator: MessagesValidator<T>;
  if (Array.isArray(messageInstance)) {
    if (!messageInstance.length) {
      throw new Error('The verification method cannot be an empty array.')
    }
    validator = new MessagesValidator(messageInstance);
  } else {
    validator = new MessagesValidator([messageInstance]);
  }
  validator.addValidFields(validFields);
  return validator;
}

/*
 * 是否为无效值
 * */
function isNil(value: any): boolean {
  return value === undefined || value === null || value === '';
};
