/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from './task_base';
import { SoapXmlWriter } from '../xml/soap_xml';
import { CopyItem, MoveCopyItemResp, MoveItem } from '../model/move_copy_item';
import { CopyItemRequest, MoveItemRequest } from '../request/move_copy_item_request';
import { MoveCopyItemResponse } from '../response/move_copy_item_response';

/**
 * 移动邮件、联系人、日历等公共请求task类
 *
 * @since 2024-04-15
 */
export class MoveItemTask extends TaskBase<MoveItemRequest, MoveCopyItemResponse, MoveItem, MoveCopyItemResp> {
  public execute(): Promise<MoveCopyItemResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let moveCopyItemResponse: MoveCopyItemResponse = new MoveCopyItemResponse(new MoveCopyItemResp());
    return this.send<MoveCopyItemResponse>(writer, moveCopyItemResponse);
  }
}

/**
 * 复制邮件、联系人、日历等公共请求task类
 *
 * @since 2024-04-15
 */
export class CopyItemTask extends TaskBase<CopyItemRequest, MoveCopyItemResponse, CopyItem, MoveCopyItemResp> {
  public execute(): Promise<MoveCopyItemResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let moveCopyItemResponse: MoveCopyItemResponse = new MoveCopyItemResponse(new MoveCopyItemResp());
    return this.send<MoveCopyItemResponse>(writer, moveCopyItemResponse);
  }
}

