/*
 * Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SoapXmlWriter } from '../../xml';
import { TaskBase } from '../task_base';
import { ModelBase } from '../../model/mode_base';
import { ForwardItem, ReplyAllToItem } from '../../model/mail/forward_reply_item';
import { ForwardItemRequest, ReplyItemRequest } from '../../request/mail/forward_reply_item';
import { ForwardReplyItemResponse } from '../../response/mail/forward_reply_item_respones';

/**
 * 转发
 *
 * @since 2024-04-20
 */
export class ForwardItemTask extends TaskBase<ForwardItemRequest, ForwardReplyItemResponse, ForwardItem, ModelBase> {
  public async execute(): Promise<ForwardReplyItemResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let forwardReplyItemResponse: ForwardReplyItemResponse = new ForwardReplyItemResponse();
    return this.send<ForwardReplyItemResponse>(writer, forwardReplyItemResponse);
  }
}


/**
 * 回复
 *
 * @since 2024-04-20
 */
export class ReplyItemTask extends TaskBase<ReplyItemRequest, ForwardReplyItemResponse, ReplyAllToItem, ModelBase> {
  public async execute(): Promise<ForwardReplyItemResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let forwardReplyItemResponse: ForwardReplyItemResponse = new ForwardReplyItemResponse();
    return this.send<ForwardReplyItemResponse>(writer, forwardReplyItemResponse);
  }
}