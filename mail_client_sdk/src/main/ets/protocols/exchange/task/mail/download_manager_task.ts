/* Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Properties } from '../../../../api';
import { DownLoadAttachment, GetAttachment, ItemAttachment, OnProgress } from '../../model/mail/attachments';
import { DownLoadAttachmentRequest, GetAttachmentRequest } from '../../request/mail/attachment_request';
import { DownLoadAttachmentTask } from './downLoad_attachment_task';
import { GetAttachmentTask } from './get_attachment_task';

/**
 * 下载管理类
 *
 * @since 2024-03-28
 */
export class DownLoadManager {
  private downloadMap: Map<string, DownLoadAttachmentTask> = new Map<string, DownLoadAttachmentTask>();

  /**
   * 下载项目附件
   *
   * @param attachmentId 附件ID
   * @param properties 用户信息
   * @returns 项目对象ItemAttachment
   */
  public downLoad(attachmentId: string, properties: Properties): Promise<ItemAttachment>;

  /**
   * 下载文件附件
   *
   * @param properties 用户信息
   * @param attachmentId 附件ID
   * @param onDataReceive 进度callback
   * @returns 下载附件的路径/文件名称.后缀
   */
  public downLoad(attachmentId: string, properties: Properties, path: string, onDataReceive: OnProgress,
                  attachmentSize: number): Promise<string>;

  /**
   * 下载实现
   */
  public downLoad(attachmentId: string , properties: Properties, path?: string,onDataReceive?: OnProgress,
                  attachmentSize?: number){
    if (path) {
      return new Promise<string>((resolve) => {
        let downLoadAttachment = new DownLoadAttachment();
        downLoadAttachment.attachmentIds = [attachmentId];
        downLoadAttachment.path = path;
        downLoadAttachment.attachmentSize = attachmentSize;
        downLoadAttachment.onDataReceive = onDataReceive;
        const task = new DownLoadAttachmentTask(new DownLoadAttachmentRequest(downLoadAttachment), properties)
        task.execute().then((result) => {
          resolve(result.downLoadPath)
        })
        this.downloadMap.set(attachmentId, task);
      })
    } else {
      return new Promise((resolve,reject) => {
        let getAttachment = new GetAttachment();
        getAttachment.attachmentIds = [attachmentId];
        let itemAttachment: ItemAttachment;
        let getAttachmentTask: GetAttachmentTask = new GetAttachmentTask(new GetAttachmentRequest(getAttachment), properties);
        getAttachmentTask.execute().then(data => {
          data.getData().responseMessages.forEach(responseMessages => {
            if(responseMessages.responseCode == 'NoError'){
              responseMessages.attachmentList.forEach(att => {
                if (att instanceof ItemAttachment) {
                  itemAttachment = (att as ItemAttachment);
                  resolve(itemAttachment)
                }
              })
            } else {
              reject(responseMessages)
            }
          })
        });
      })
    }
  }

  /**
   * 取消下载
   */
  public cancelDownLoad(attachmentId: string): void {
    this.downloadMap.get(attachmentId)?.cancelDownLoad();
    this.downloadMap.delete(attachmentId);
  }
}
