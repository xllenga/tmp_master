/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import rcp from '@hms.collaboration.rcp';
import fs from '@ohos.file.fs';
import { util } from '@kit.ArkTS';
import { BusinessError } from '@kit.BasicServicesKit';
import {
  DownLoadAttachment,
  DownLoadAttachmentResp,
  FileAttachment,
} from '../../model/mail/attachments';
import { getLogger, Logger } from '../../../../utils/log';
import { base64Decode } from '../../../../utils/encodings';
import { getFileSuffixByType } from '../../../../utils/file_stream';
import { SoapXmlReader, SoapXmlWriter } from '../../xml';
import { TaskBase } from '../task_base';
import { DownLoadAttachmentRequest } from '../../request/mail/attachment_request';
import { DownLoadAttachmentResponse, GetAttachmentResponse } from '../../response/mail/attachment_response';
import { Properties } from '../../../../api';
import { httpStreamRequest } from '../../utils/http_client';
import { CredentialsStore } from '../../utils/exchange_credentials';

const logger: Logger = getLogger('DownLoadAttachmentTask');

/**
 * 下载附件类
 *
 * @since 2024-03-28
 */
export class DownLoadAttachmentTask extends TaskBase<DownLoadAttachmentRequest, DownLoadAttachmentResponse,
  DownLoadAttachment, DownLoadAttachmentResp> {
  public async execute(): Promise<DownLoadAttachmentResponse> {
    const path = this.request.getRawData().path;
    const attachmentSize = this.request.getRawData().attachmentSize;
    const onDataReceive = this.request.getRawData().onDataReceive;
    const downLoadAttachmentResponse: DownLoadAttachmentResponse = new DownLoadAttachmentResponse();
    await this.downLoad(path, onDataReceive, attachmentSize).then((result) => {
      downLoadAttachmentResponse.downLoadPath = result;
    })
    return new Promise((resolve: Function) => {
      resolve(downLoadAttachmentResponse);
    });
  }
  /**
   * 下载附件
   *
   * @param path 下载路径
   * @param onDataReceive 进度callback
   * @returns 下载附件的路径/文件名称.后缀
   */
  public downLoad(path: string, onDataReceive: Function, attachmentSize: number): Promise<string> {
    let streamingEventsXml: string = '';
    return new Promise((resolve: Function, reject: Function) => {
      let writer: SoapXmlWriter = new SoapXmlWriter();
      let progress: string;
      this.request.writeToXml(writer);
      logger.info('DownLoadAttachmentTaskXml', this.request.xmlToString(writer));
      this.httpClient.request(this.request.xmlToString(writer), (data) => {
            const textDecoder: util.TextDecoder = util.TextDecoder.create();
            let bufferXml = textDecoder.decodeWithStream(new Uint8Array(data));
            streamingEventsXml += bufferXml.toString();
            if (streamingEventsXml.length / attachmentSize <= 1) {
              progress = (streamingEventsXml.length / attachmentSize * 100) + '%';
              onDataReceive(progress);
            } else if(progress != '100%'){
              onDataReceive('100%');
              progress = '100%';
            }
            return data.byteLength;
          }).then((data: rcp.Response) => {
          if (data.statusCode === 200) {
            const resp: GetAttachmentResponse = new GetAttachmentResponse();
            const reader: SoapXmlReader = new SoapXmlReader(streamingEventsXml as string);
            let name: string;
            let contentType: string;
            const uuid = util.generateRandomUUID();
            resp.readFromXml(reader).getData().responseMessages.forEach(responseMessages => {
              if(responseMessages.responseCode == 'NoError'){
                responseMessages.attachmentList.forEach(att => {
                  if (att instanceof FileAttachment) {
                    const fileAttachment = (att as FileAttachment);
                    name = fileAttachment.name;
                    contentType = fileAttachment.contentType;
                    this.downLoadBase64(fileAttachment.content, contentType, name+uuid, path);
                  }
                })
                const finalName: string =`${path}/${name}${uuid}.${getFileSuffixByType(contentType)}`;
                resolve(finalName);
              } else {
                reject(responseMessages);
              }
            })
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });


    });
  }

  /**
   * 取消下载附件
   */
  public cancelDownLoad(): void {
    this.httpClient.destroy();
    logger.info('CancelDownLoad success')
  }

  /**
   * 下载文件base64到filesDir
   *
   * @param base64String 附件
   * @param contentType 附件类型
   * @param fileName 附件名称
   * @param filesDir 附件下载路径
   */
  public downLoadBase64(base64String: string, contentType: string, fileName: string, filesDir: string) {
    try {
      let file = fs.openSync(`${filesDir}/${fileName}.${getFileSuffixByType(contentType)}` , fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
      let writeLen = fs.writeSync(file.fd, base64Decode(base64String).buffer);
      logger.info('downLoadBase64 length', writeLen)
      fs.closeSync(file);
    } catch (e) {
      logger.info('downLoadBase64 error ', e)
    }
  }

  protected initHttpClient(properties: Properties): void {
    this.httpClient = httpStreamRequest;
    let caPath: string[] = properties?.ca ?? [];
    this.httpClient
      .setUrl(properties.exchange?.url)
      .enableSslVerify(properties.exchange?.secure)
      .setExchangeCredentials(CredentialsStore.getInstance().credentials)
      .setCaPath(caPath[0] ?? '');
  }
}


