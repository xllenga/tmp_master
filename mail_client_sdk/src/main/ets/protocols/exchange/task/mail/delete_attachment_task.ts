/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DeleteAttachmentResponse } from '../../response/mail/attachment_response';
import { TaskBase } from '../task_base';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { DeleteAttachment, DeleteAttachmentResp } from '../../model/mail/attachments';
import { DeleteAttachmentRequest } from '../../request/mail/attachment_request';

/**
 * 删除附件类
 *
 * @since 2024-03-28
 */
export class DeleteAttachmentTask extends TaskBase<DeleteAttachmentRequest, DeleteAttachmentResponse,
  DeleteAttachment, DeleteAttachmentResp> {
  public async execute(): Promise<DeleteAttachmentResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let deleteAttachmentResponse: DeleteAttachmentResponse = new DeleteAttachmentResponse();
    return this.send<DeleteAttachmentResponse>(writer, deleteAttachmentResponse, false);
  }
}

