/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from './task_base';
import { SoapXmlWriter } from '../xml/soap_xml';
import { StreamingSubscriptionRequest } from '../request/streaming_subscription_request';
import { StreamingSubscriptionResponse } from '../response/streaming_subscription_response';
import { StreamingSubscription, StreamingSubscriptionResp } from '../model/streaming_subscription';

/**
 * 订阅请求任务
 *
 * @since 2024-03-19
 */
export class StreamingSubscriptionTask extends TaskBase<StreamingSubscriptionRequest, StreamingSubscriptionResponse,
StreamingSubscription, StreamingSubscriptionResp> {
  public execute(): Promise<StreamingSubscriptionResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let streamingSubscriptionResponse: StreamingSubscriptionResponse = new StreamingSubscriptionResponse(new
    StreamingSubscriptionResp());
    return this.send<StreamingSubscriptionResponse>(writer, streamingSubscriptionResponse);
  }
}
