/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { CreateCalendarRequest } from '../../request/calendar/create_calendar_request';
import { CreateCalendarsMessage } from '../../model/calendar/create_calendar_message';
import { Properties } from '../../../../api';
import { ResponseBatchModelBase, ResponseModelBase } from '../../response/response_base';
import { SoapXmlWriter } from '../../xml';
import { UniteId } from '../../model/mode_base';
import { CreateItemResponse, CreateItemsResponse } from '../../response/create_item_response';

/*
 * 创建日历文件夹Task
 * */
export class createCalendarTask<R extends CreateItemResponse | CreateItemsResponse> extends TaskBase<
  CreateCalendarRequest,
  CreateItemResponse | CreateItemsResponse,
  CreateCalendarsMessage,
  ResponseBatchModelBase<UniteId> | ResponseModelBase<UniteId>
> {
  // 是否批量创建
  public batch = false;

  public async execute(): Promise<R> {
    const xmlWriter = new SoapXmlWriter();
    this.request.validate();
    this.request.writeToXml(xmlWriter);
    if (this.batch) {
      return this.send(xmlWriter, new CreateItemsResponse() as R);
    } else {
      return this.send(xmlWriter, new CreateItemResponse() as R);
    }
  }
}

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'wangml9655@outlook.com',
    password: 'wml840576)(',
  }
};

export async function createCalendarItems(folders: string[]): Promise<ResponseBatchModelBase<UniteId>> {
  const msg = new CreateCalendarsMessage();
  msg.folderDisplayNames = folders;
  const task = new createCalendarTask<CreateItemsResponse>(new CreateCalendarRequest(msg), properties);
  task.batch = true;
  try {
    const resp = await task.execute();
    const data = resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}

export async function createCalendarItem(folderName: string): Promise<ResponseModelBase<UniteId>> {
  const msg = new CreateCalendarsMessage();
  msg.folderDisplayNames = [folderName];
  const task = new createCalendarTask<CreateItemResponse>(new CreateCalendarRequest(msg), properties);
  try {
    const resp = await task.execute();
    const data = resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}
