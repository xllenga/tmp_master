/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { ModelBase } from '../../model/mode_base';
import { getLogger } from '../../../../utils/log';
import { GetRoomListResponse } from '../../response/calendar/get_room_list_response';
import { GetRoomListRequest } from '../../request/calendar/get_room_list_request';
import { GetRoomListsMessage } from '../../model/calendar/get_room_lists_message';
import rcp from '@hms.collaboration.rcp';

const logger = getLogger("CalendarGetRoomListTask");

/**
 * 获取所有会议室列表
 */
export class GetRoomListsTask extends TaskBase<GetRoomListRequest, GetRoomListResponse, GetRoomListsMessage, ModelBase> {
  public async execute(): Promise<GetRoomListResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    const xmlReqString = this.request.xmlToString(writer);
    logger.debug('execute: xmlReqString = ', xmlReqString);
    let response: rcp.Response = await this.httpClient.request(xmlReqString);
    let responseData: string = response.toString() ?? '';
    console.log('[liwang-mail]', '请求结果=' + JSON.stringify(responseData))
    let resp: GetRoomListResponse = new GetRoomListResponse();
    let reader: SoapXmlReader = new SoapXmlReader(responseData);
    resp.readFromXml(reader);
    return resp;
  }
}
