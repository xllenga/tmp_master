/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { CreateAppointmentItemRequest } from '../../request/calendar/create_appointment_request';
import {
  CreateAppointmentMessage,
  AppointmentMessage,
} from '../../model/calendar/create_appointment_message';
import { Properties } from '../../../../api';
import { ResponseBatchModelBase, ResponseModelBase } from '../../response/response_base';
import { SendMeetingInvitations } from '../../utils/common_enum';
import { FolderName, SoapXmlWriter } from '../../xml';
import { CreateItemResponse, CreateItemsResponse } from '../../response/create_item_response';
import { UniteId } from '../../model/mode_base';

/*
 * 创建会议或约会Task
 * */
export class createAppointmentItemTask extends TaskBase<
  CreateAppointmentItemRequest,
  CreateItemResponse,
  CreateAppointmentMessage,
  ResponseModelBase<UniteId>
> {
  public async execute(): Promise<CreateItemResponse> {
    const xmlWriter = new SoapXmlWriter();
    this.request.validate();
    this.request.writeToXml(xmlWriter);
    return this.send(xmlWriter, new CreateItemResponse());
  }
}

/*
 * 创建会议或约会Task(批量）
 * */
export class CreateAppointmentItemsTask extends TaskBase<
  CreateAppointmentItemRequest,
  CreateItemsResponse,
  CreateAppointmentMessage,
  ResponseBatchModelBase<UniteId>
> {
  public async execute(): Promise<CreateItemsResponse> {
    const xmlWriter = new SoapXmlWriter();
    this.request.writeToXml(xmlWriter);
    return this.send(xmlWriter, new CreateItemsResponse());
  }
}

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'wangml9655@outlook.com',
    password: 'wml840576)(',
  }
};

export async function createAppointmentItem(appointMessage: AppointmentMessage, sendMeetingInvitations: SendMeetingInvitations, folderId ?: string): Promise<ResponseModelBase<UniteId>> {
  const appointmentMsg = new CreateAppointmentMessage();
  appointmentMsg.sendInvitationsMode = sendMeetingInvitations;
  if (folderId) {
    appointmentMsg.folderId = folderId;
  } else {
    appointmentMsg.distinguishedFolderId = FolderName.calendar
  }
  appointmentMsg.items = [appointMessage];
  const task = new createAppointmentItemTask(new CreateAppointmentItemRequest(appointmentMsg), properties);
  try {
    const resp = await task.execute();
    const data =  resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}

export async function createAppointmentItems(appointMessage: AppointmentMessage[], sendMeetingInvitations: SendMeetingInvitations, folderId ?: string): Promise<ResponseBatchModelBase<UniteId>> {
  const appointmentMsg = new CreateAppointmentMessage();
  appointmentMsg.sendInvitationsMode = sendMeetingInvitations;
  if (folderId) {
    appointmentMsg.folderId = folderId;
  } else {
    appointmentMsg.distinguishedFolderId = FolderName.calendar
  }
  appointmentMsg.items = appointMessage;
  const task = new CreateAppointmentItemsTask(new CreateAppointmentItemRequest(appointmentMsg), properties);
  try {
    const resp = await task.execute();
    const data =  resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}