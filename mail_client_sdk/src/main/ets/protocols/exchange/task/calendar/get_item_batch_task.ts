/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { getLogger } from '../../../../utils/log';
import { GetItemBatchResponse } from '../../response/calendar/get_item_batch_response';
import { GetItemBatchRequest } from '../../request/calendar/get_item_batch_request';
import { GetItemBatchMessage, GetItemBatchResponseMessage } from '../../model/calendar/get_item_batch_message';
import rcp from '@hms.collaboration.rcp';

const logger = getLogger("GetItemBatchTask");

/**
 * 批量获取日历项
 */
export class GetItemBatchTask extends
  TaskBase<GetItemBatchRequest, GetItemBatchResponse, GetItemBatchMessage, GetItemBatchResponseMessage> {
  public async execute(): Promise<GetItemBatchResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    const xmlReqString = this.request.xmlToString(writer);
    logger.debug('execute: xmlReqString = ', xmlReqString);
    let response: rcp.Response = await this.httpClient.request(xmlReqString);
    let resp: GetItemBatchResponse = new GetItemBatchResponse();
    let reader: SoapXmlReader = new SoapXmlReader(response.toString() ?? '');
    resp.readFromXml(reader);
    return resp;
  }
}
