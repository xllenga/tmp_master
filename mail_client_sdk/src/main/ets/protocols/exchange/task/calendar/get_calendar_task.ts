/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import rcp from '@hms.collaboration.rcp';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { getLogger } from '../../../../utils/log';
import { GetAppointmentResponse } from '../../response/calendar/get_appointment_response';
import { GetCalendarMessage, GetAppointmentResponseMessage } from '../../model/calendar/get_calendar_message';
import { GetCalendarRequest } from '../../request/calendar/get_calendar_request';

const logger = getLogger("GetCalendarTask");

/**
 * 获取日历文件夹
 */
export class GetCalendarTask extends
  TaskBase<GetCalendarRequest, GetAppointmentResponse, GetCalendarMessage, GetAppointmentResponseMessage> {
  public async execute(): Promise<GetAppointmentResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    const xmlReqString = this.request.xmlToString(writer);
    logger.debug('execute: xmlReqString = ', xmlReqString);
    let response: rcp.Response = await this.httpClient.request(xmlReqString);
    let resp: GetAppointmentResponse = new GetAppointmentResponse();
    let reader: SoapXmlReader = new SoapXmlReader(response.toString() ?? '');
    resp.readFromXml(reader);
    return resp;
  }
}
