/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { Properties } from '../../../../api';
import { ResponseModelBase } from '../../response/response_base';
import { MessageDisposition } from '../../utils/common_enum';
import { SoapXmlWriter } from '../../xml';
import { CancelMeetingRequest } from '../../request/calendar/cancel_meeting_request';
import { CancelMeetingInfo, CancelMeetingMessage } from '../../model/calendar/cancel_meeting_message';
import { CreateItemResponse } from '../../response/create_item_response';
import { UniteId } from '../../model/mode_base';

/*
 * 取消会议Task
 * */
export class CancelMeetingTask extends TaskBase<
  CancelMeetingRequest,
  CreateItemResponse,
  CancelMeetingMessage,
  ResponseModelBase<UniteId>
> {
  public async execute(): Promise<CreateItemResponse> {
    const xmlWriter = new SoapXmlWriter();
    this.request.validate();
    this.request.writeToXml(xmlWriter);
    return this.send(xmlWriter, new CreateItemResponse());
  }
}

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'wangml9655@outlook.com',
    password: 'wml840576)(',
  }
};


export async function cancelMeeting(appointMessage: CancelMeetingInfo)
  : Promise<ResponseModelBase<UniteId>> {
  const cancelMeetingMsg = new CancelMeetingMessage();
  cancelMeetingMsg.messageDisposition = MessageDisposition.SendAndSaveCopy;
  cancelMeetingMsg.items = [appointMessage];
  const task = new CancelMeetingTask(new CancelMeetingRequest(cancelMeetingMsg), properties);
  try {
    const resp = await task.execute();
    const data =  resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}
