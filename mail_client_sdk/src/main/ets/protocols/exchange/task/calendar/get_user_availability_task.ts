/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { ModelBase } from '../../model/mode_base';
import { getLogger } from '../../../../utils/log';
import { GetUserAvailabilityRequest } from '../../request/calendar/get_user_availability_request';
import { GetUserAvailabilityMessage } from '../../model/calendar/get_user_availability_message';
import { GetUserAvailabilityResponse } from '../../response/calendar/get_user_availability_response';
import rcp from '@hms.collaboration.rcp';

const logger = getLogger("CalendarGetUserAvailabilityTask");

/**
 * 获取忙/闲信息
 */
export class GetUserAvailabilityTask extends TaskBase<GetUserAvailabilityRequest, GetUserAvailabilityResponse, GetUserAvailabilityMessage, ModelBase> {
  public async execute(): Promise<GetUserAvailabilityResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    const xmlReqString = this.request.xmlToString(writer);
    logger.debug('execute: xmlReqString = ', xmlReqString);
    let response: rcp.Response = await this.httpClient.request(xmlReqString);
    let resp: GetUserAvailabilityResponse = new GetUserAvailabilityResponse();
    let reader: SoapXmlReader = new SoapXmlReader(response.toString() ?? '');
    resp.readFromXml(reader);
    return resp;
  }
}
