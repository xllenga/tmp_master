/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SearchMailResponse } from '../../response/search/search_mail_response';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { Filters, FindItems, FindItemResp } from '../../model/search/mail_search_findItems';
import { SearchMailRequest } from '../../request/search/search_mail_request';
import rcp from '@hms.collaboration.rcp';

/**
 * 搜索邮件任务
 *
 * @since 2024-03-30
 */
export class SearchMailTask extends TaskBase<SearchMailRequest, SearchMailResponse, FindItems<Filters>, FindItemResp> {

  /**
   * 搜索邮件请求
   *
   * @returns Promise<SearchMailResponse>
   */
  public execute(): Promise<SearchMailResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let searchMailResponse: SearchMailResponse = new SearchMailResponse();
    return this.send<SearchMailResponse>(writer, searchMailResponse);
  }
}