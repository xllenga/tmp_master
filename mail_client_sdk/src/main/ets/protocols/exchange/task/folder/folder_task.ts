/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { CreateFolder, CreateFolderResp } from '../../model/folder/create_folder';
import { CreateFolderRequest } from '../../request/folder/create_folder_request';
import { CreateFolderResponse } from '../../response/folder/create_folder_response';
import { DeleteFolder, DeleteFolderResp } from '../../model/folder/delete_folder';
import { DeleteFolderRequest } from '../../request/folder/delete_folder_request';
import { DeleteFolderResponse } from '../../response/folder/delete_folder_response';
import { EmptyFolder, EmptyFolderResp } from '../../model/folder/empty_folder';
import { EmptyFolderResponse } from '../../response/folder/empty_folder_response';
import { EmptyFolderRequest } from '../../request/folder/empty_folder_request';
import { GetFolderRequest } from '../../request/folder/get_folder_request';
import { GetFolderResponse } from '../../response/folder/get_folder_response';
import { GetFolder, GetFolderResp } from '../../model/folder/get_folder';
import { MarkAllItemsAsRead, MarkAllItemsAsReadResp } from '../../model/folder/mark_all_items_as_read';
import { MarkAllItemsAsReadResponse } from '../../response/folder/mark_all_items_ad_read_response';
import { MarkAllItemsAsReadRequest } from '../../request/folder/mark_all_items_as_read_request';
import { UpdateFolder, UpdateFolderResp } from '../../model/folder/update_folder';
import { UpdateFolderRequest } from '../../request/folder/update_folder_request';
import { UpdateFolderResponse } from '../../response/folder/update_folder_response';
import { MoveFolderRequest } from '../../request/folder/move_folder_request';
import { MoveFolder, MoveFolderResp } from '../../model/folder/move_folder';
import { MoveFolderResponse } from '../../response/folder/move_folder_response';

/**
 * 创建文件夹任务
 *
 * @since 2024-03-19
 */
export class CreateFolderTask extends TaskBase<CreateFolderRequest, CreateFolderResponse, CreateFolder, CreateFolderResp> {
  public execute(): Promise<CreateFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let createFolderResponse: CreateFolderResponse = new CreateFolderResponse();
    return this.send<CreateFolderResponse>(writer, createFolderResponse);
  }
}

/**
 * 删除文件夹任务
 *
 * @since 2024-03-19
 */
export class DeleteFolderTask extends TaskBase<DeleteFolderRequest, DeleteFolderResponse, DeleteFolder, DeleteFolderResp> {
  public execute(): Promise<DeleteFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let deleteFolderResponse: DeleteFolderResponse = new DeleteFolderResponse();
    return this.send<DeleteFolderResponse>(writer, deleteFolderResponse);
  }
}

/**
 * 清空文件夹任务
 *
 * @since 2024-03-19
 */
export class EmptyFolderTask extends TaskBase<EmptyFolderRequest, EmptyFolderResponse, EmptyFolder, EmptyFolderResp> {
  public execute(): Promise<EmptyFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let emptyFolderResponse: EmptyFolderResponse = new EmptyFolderResponse();
    return this.send<EmptyFolderResponse>(writer, emptyFolderResponse);
  }
}

/**
 * 获取文件夹任务
 *
 * @since 2024-03-19
 */
export class GetFolderTask extends TaskBase<GetFolderRequest, GetFolderResponse, GetFolder, GetFolderResp> {
  public execute(): Promise<GetFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let getFolderResponse: GetFolderResponse = new GetFolderResponse();
    return this.send<GetFolderResponse>(writer, getFolderResponse);
  }
}

/**
 * 文件夹标记已读任务
 *
 * @since 2024-03-19
 */
export class MarkAllItemsAsReadTask extends TaskBase<MarkAllItemsAsReadRequest, MarkAllItemsAsReadResponse, MarkAllItemsAsRead, MarkAllItemsAsReadResp> {
  public execute(): Promise<MarkAllItemsAsReadResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let markAllItemsAsReadResponse: MarkAllItemsAsReadResponse = new MarkAllItemsAsReadResponse();
    return this.send<MarkAllItemsAsReadResponse>(writer, markAllItemsAsReadResponse);
  }
}

/**
 * 移动文件夹任务
 *
 * @since 2024-03-19
 */
export class MoveFolderTask extends TaskBase<MoveFolderRequest, MoveFolderResponse, MoveFolder, MoveFolderResp> {
  public execute(): Promise<MoveFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let moveFolderResponse: MoveFolderResponse = new MoveFolderResponse();
    return this.send<MoveFolderResponse>(writer, moveFolderResponse);
  }
}

/**
 * 更新文件夹任务
 *
 * @since 2024-03-19
 */
export class UpdateFolderTask extends TaskBase<UpdateFolderRequest, UpdateFolderResponse, UpdateFolder, UpdateFolderResp> {
  public execute(): Promise<UpdateFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let updateFolderResponse: UpdateFolderResponse = new UpdateFolderResponse();
    return this.send<UpdateFolderResponse>(writer, updateFolderResponse);
  }
}