/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import rcp from '@hms.collaboration.rcp';
import { SoapXmlReader, SoapXmlWriter } from '../xml/soap_xml';
import { TaskBase } from './task_base';
import { GetStreamingEventsRequest } from '../request/get_streaming_events_request';
import { GetStreamingEventsResponse } from '../response/get_streaming_events_response';
import {
  StreamingSubscription,
  GetStreamingEventsResp,
  Notification,
  EventItem
} from '../model/streaming_subscription';
import { getLogger, Logger } from '../../../utils/log';
import { util } from '@kit.ArkTS';
import { Properties } from '../../../api';
import { HttpStreamRequest } from '../utils/http_client';
import { CredentialsStore } from '../utils/exchange_credentials';

const logger: Logger = getLogger('StreamingEventsTask');

// 响应类型标识 eventResponse为事件的响应
const EVENT_RESPONSE: string = 'eventResponse';

/**
 * 流式处理通知任务
 *
 * @since 2024-03-19
 */
export class GetStreamingEventsTask extends TaskBase<GetStreamingEventsRequest, GetStreamingEventsResponse,
StreamingSubscription, GetStreamingEventsResp> {
  private subscription: StreamingSubscription;
  private streamingEventsResp: GetStreamingEventsResp;
  public listener: Function;

  /**
   * 设置订阅数据模型
   *
   * @param sub 订阅数据模型
   */
  public setSubscription(sub: StreamingSubscription): void {
    this.subscription = sub;
  }

  /**
   * 设置流式处理通知响应模型
   */
  public setListener(callback: (eventList: EventItem[]) => void) {
    this.listener = callback;
  }

  /**
   * 设置流式处理通知响应模型
   */
  public getStreamingEventsResp(): GetStreamingEventsResp {
    return this.streamingEventsResp;
  }

  protected initHttpClient(properties: Properties): void {
    let caPath: string[] = properties?.ca ?? [];
    this.httpClient = new HttpStreamRequest();
    this.httpClient
      .setUrl(properties.exchange?.url)
      .enableSslVerify(properties.exchange?.secure)
      .setExchangeCredentials(CredentialsStore.getInstance().credentials)
      .setCaPath(caPath[0] ?? '');
  }

  /**
   * 获取所有更改事件的列表
   *
   * @param notifications 通知列表
   * @returns allEventsList 所有更改事件列表
   */
  private getAllEventsList(notifications: Notification[]): EventItem[] {
    let allEventsList: EventItem[] = [];
    if (notifications.length > 0) {
      notifications?.forEach(notification => {
        let eventsMap: Map<string, EventItem[]> = notification.events;
        let notificationEventsList: EventItem[] = this.getNotificationEventsList(eventsMap);
        allEventsList.push.apply(allEventsList, notificationEventsList);
      });
    }
    return allEventsList;
  }

  /**
   * 获取每个通知下的更改事件列表
   *
   * @param eventsMap 每个通知的事件Map
   * @returns notificationEventsList 每个通知下的更改事件列表
   */
  private getNotificationEventsList(eventsMap: Map<string, EventItem[]>): EventItem[] {
    let notificationEventsList: EventItem[] = [];
    for (const eventList of eventsMap.values()) {
      eventList.forEach(eventItem => {
        let hasThatEvent: EventItem | null = notificationEventsList.find(c => c.itemId === eventItem.itemId) ?? null;
        if (!hasThatEvent) {
          notificationEventsList.push(eventItem);
        }
      });
    }
    return notificationEventsList;
  }

  public async execute(): Promise<GetStreamingEventsResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let response: rcp.Response = await this.httpClient.request(this.request.xmlToString(writer), (data: ArrayBuffer) => {
      let resp: GetStreamingEventsResponse = new GetStreamingEventsResponse(new GetStreamingEventsResp());
      const textDecoder: util.TextDecoder = util.TextDecoder.create();
      const bufferXml: string = textDecoder.decodeWithStream(new Uint8Array(data));
      this.subscription.streamingEventsXml += bufferXml.toString();
      let streamingEventsXml: string = this.subscription.streamingEventsXml;
      const regex: RegExp = /<\/Envelope>\s*$/;
      const isEndingEnvelope: boolean = regex.test(streamingEventsXml);
      if (isEndingEnvelope) {
        let reader: SoapXmlReader = new SoapXmlReader(streamingEventsXml as string);
        resp.readFromXml(reader, false);
        this.streamingEventsResp = resp.getData();
        if (this.streamingEventsResp.responseType === EVENT_RESPONSE) {
          let notifications: Notification[] = this.streamingEventsResp.notifications;
          let allEventsList: EventItem[] = this.getAllEventsList(notifications) ?? [];
          this.listener(allEventsList);
        }
        this.subscription.streamingEventsXml = '';
      } else {
        logger.info('The response is not ending...');
        return;
      }
      return data.byteLength;
    });
    return;
  }
}
