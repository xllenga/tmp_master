/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from '../task_base';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { CreateContactRequest } from '../../request/contact/create_contact_request';
import { CreateContact, CreateContactResp } from '../../model/contact/create_contact';
import { CreateContactResponse } from '../../response/contact/create_contact_response';
import { Contact } from '../../model/contact/item_contact';

/**
 * 创建联系人网络请求类
 *
 * @since 2024-04-11
 */
export class CreateContactTask
  extends TaskBase<CreateContactRequest, CreateContactResponse, CreateContact, CreateContactResp> {
  /**
   * 创建联系人网络请求API
   *
   * @returns 返回附带请求结果的Promise，内部判断为NoError则reject
   */
  public async execute(): Promise<CreateContactResponse> {
    let size: number = 1024 * 4; // 初始4k
    const contacts: Contact[] = this.request.getRawData().contacts ?? [];
    contacts.forEach(contact => {
      size += 1024 * 4; // 每一个contact加4k
    });
    let writer: SoapXmlWriter = new SoapXmlWriter(size);
    this.request.writeToXml(writer);
    let createContactResponse: CreateContactResponse = new CreateContactResponse(new CreateContactResp());
    return this.send<CreateContactResponse>(writer, createContactResponse);
  }
}