/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskBase } from './task_base';
import { SoapXmlReader, SoapXmlWriter } from '../xml/soap_xml';
import { DeleteItem, DeleteItemResp } from '../model/delete_item';
import { DeleteItemRequest } from '../request/delete_item_request';
import { DeleteItemResponse } from '../response/delete_item_response';
import { getLogger, Logger } from '../../../utils/log';
import rcp from '@hms.collaboration.rcp';

const logger: Logger = getLogger("DeleteTask");

/**
 * 删除邮件、联系人、日历等公共请求task类
 *
 * @since 2024-04-15
 */
export class DeleteItemTask extends TaskBase<DeleteItemRequest, DeleteItemResponse, DeleteItem, DeleteItemResp> {
  public async execute(): Promise<DeleteItemResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let response: rcp.Response = await this.httpClient.request(this.request.xmlToString(writer));
    let resp: DeleteItemResponse = new DeleteItemResponse(new DeleteItemResp());
    const result: string = response.toString() ?? '';
    logger.debug(`deleteItemResponse result is: ${result}`);
    resp.readFromXml(new SoapXmlReader(result));
    if (resp.isSuccessful()) {
      return resp;
    } else {
      throw this.handleEwsError(resp.getRespCode());
    }
  }
}

