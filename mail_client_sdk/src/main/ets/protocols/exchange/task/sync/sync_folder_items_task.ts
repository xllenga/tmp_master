/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SoapXmlWriter } from '../../xml/soap_xml';
import { TaskBase } from '../task_base';
import { SyncFolderItemsRequest } from '../../request/sync/sync_folder_items_request';
import { FolderItems, FolderItemsResp } from '../../model/sync/folder_items';
import { SyncFolderItemsResponse } from '../../response/sync/sync_folder_items_response';

/**
 * 同步文件夹中变更项
 *
 * @since 2024-03-24
 */
export class SyncFolderItemsTask extends TaskBase<SyncFolderItemsRequest, SyncFolderItemsResponse, FolderItems,
  FolderItemsResp> {
  public execute(): Promise<SyncFolderItemsResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return this.send(writer, new SyncFolderItemsResponse(new FolderItemsResp()));
  }
}