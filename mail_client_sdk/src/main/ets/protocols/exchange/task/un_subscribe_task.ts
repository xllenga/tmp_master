/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UnSubscribeRequest } from '../request/un_subscribe_request';
import { SoapXmlWriter } from '../xml/soap_xml';
import { TaskBase } from './task_base';
import { UnSubscribeResponse } from '../response/un_subscribe_response';
import { StreamingSubscription, UnSubscribeResp } from '../model/streaming_subscription';

/**
 * 取消订阅任务
 *
 * @since 2024-03-19
 */
export class UnSubscribeTask extends TaskBase<UnSubscribeRequest, UnSubscribeResponse, StreamingSubscription
, UnSubscribeResp> {
  public execute(): Promise<UnSubscribeResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let unSubscribeResponse: UnSubscribeResponse = new UnSubscribeResponse();
    return this.send<UnSubscribeResponse>(writer, unSubscribeResponse);
  }
}
