/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { EventItem, EventType, GetStreamingEventsResp, StreamingSubscription } from '../model/streaming_subscription';
import { StreamingSubscriptionTask } from './streaming_subscription_task';
import { StreamingSubscriptionRequest } from '../request/streaming_subscription_request';
import { UnSubscribeRequest } from '../request/un_subscribe_request';
import { UnSubscribeTask } from './un_subscribe_task';
import { Properties } from '../../../api';
import { getLogger, Logger } from '../../../utils/log';
import { GetStreamingEventsRequest } from '../request/get_streaming_events_request';
import { GetStreamingEventsTask } from './get_streaming_events_task';
import { FolderName } from '../xml';

const logger: Logger = getLogger('SubscriptionConnection');

// 连接关闭状态
const CONNECTION_STATUS_CLOSE: string = 'close';
// 最大重连次数
const RECONNECTION_COUNT_MAX: number = 3;

// 数字常量枚举
enum Constants {
  NumberZero = 0,
  NumberOne = 1,
  NumberMaxTime = 30,
  NumberMinute = 60,
  NumberSecond = 1000,
}

/**
 * 流式通知操作类
 * 用于流式通知的订阅、流式处理通知请求、取消订阅以及销毁订阅
 *
 * @since 2024-03-19
 */
export class SubscribeToStreamingNotifications {
  private subscription: StreamingSubscription = new StreamingSubscription();
  private properties: Properties;
  private static INSTANCE: SubscribeToStreamingNotifications;

  // 重连次数
  private reconnectionCount: number = 0;

  /**
   * 设置文件夹标识符数组
   *
   * @param names 文件夹标识符数组
   */
  public setFolderIds(names: FolderName[]): void {
    this.subscription.folderIds = names;
  }

  /**
   * 设置事件通知的集合
   *
   * @param events 事件通知的集合
   */
  public setEventTypes(events: EventType[]): void {
    this.subscription.eventTypes = events;
  }

  /**
   * 设置请求路径
   *
   * @param properties 请求所需信息
   */
  public setProperties(properties: Properties): void {
    this.properties = properties;
  }

  /**
   * 生成实例
   *
   * @returns CredentialsHeaderStore实例
   */
  public static getInstance(): SubscribeToStreamingNotifications {
    if (!SubscribeToStreamingNotifications.INSTANCE) {
      SubscribeToStreamingNotifications.INSTANCE = new SubscribeToStreamingNotifications();
    }
    return SubscribeToStreamingNotifications.INSTANCE;
  }

  /**
   * 创建订阅
   * 创建一个拥有唯一标识的流式订阅
   *
   * @returns Promise<void> 用于返回异常
   */
  public initiate(): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      if (!this.subscription.eventTypes.length) {
        logger.info('eventTypes cannot be empty!');
        return;
      }
      let streamingSubscriptionRequest: StreamingSubscriptionRequest = new StreamingSubscriptionRequest(
        this.subscription);
      let task: StreamingSubscriptionTask = new StreamingSubscriptionTask(streamingSubscriptionRequest,
        this.properties);
      task.execute().then((res) => {
        logger.info('Subscribe to StreamingNotification Success', res);
        this.subscription.subscriptionId = res.getData().subscriptionId;
        resolve();
      }).catch((err) => {
        logger.error('Subscribe to StreamingNotification Error', err);
        reject(err);
      });
    });
  }

  /**
   * 流式处理通知请求
   * 与服务器达成连接，接收事件流通知
   *
   * @param time 打开状态的分钟数(只能设置1-30的整数),默认为30分钟
   * @returns Promise<void> 用于返回异常
   */
  public open(listener: (eventList: EventItem[]) => void, time: number = Constants.NumberMaxTime): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      const connectionTime: string = time.toString();
      const regex: RegExp = /^([1-9]|1[0-9]|2[0-9]|30)$/;
      const isValidNumber: boolean = regex.test(connectionTime);
      if (!isValidNumber) {
        throw new Error('Please enter an integer ranging from 1 to 30!');
      }
      this.subscription.connectionTime = connectionTime;
      let getStreamingEventsRequest: GetStreamingEventsRequest = new GetStreamingEventsRequest(this.subscription);
      let getStreamingEventsTask: GetStreamingEventsTask = new GetStreamingEventsTask(getStreamingEventsRequest,
        this.properties);
      getStreamingEventsTask.setListener(listener);
      getStreamingEventsTask.setSubscription(this.subscription);
      getStreamingEventsTask.execute().then(() => {
        let streamingEventsResp: GetStreamingEventsResp = getStreamingEventsTask.getStreamingEventsResp();
        if (streamingEventsResp.connectionStatus === CONNECTION_STATUS_CLOSE) {
          this.reconnection(listener, time);
        }
        setTimeout(() => {
          this.reconnection(listener, time);
        }, time * Constants.NumberMinute * Constants.NumberSecond);
        resolve();
      }).catch((err) => {
        logger.error('GetStreamingEvents (streamingSubscription) connect Error', err);
        reject(err);
      });
    });
  }

  /**
   * 取消订阅
   * 中断连接,并销毁订阅数据模型
   *
   * @returns Promise<void> 用于返回异常
   */
  public close(): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      let unSubscribeRequest: UnSubscribeRequest = new UnSubscribeRequest(this.subscription);
      let unSubscribeTask: UnSubscribeTask = new UnSubscribeTask(unSubscribeRequest, this.properties);
      unSubscribeTask.execute().then((res) => {
        logger.info('Unsubscribe streamingNotification Success', res);
        this.dispose();
        resolve();
      }).catch((err) => {
        logger.error('Unsubscribe streamingNotification Error', err);
        reject(err);
      });
    });
  }

  /**
   * 销毁订阅，用于释放内存
   */
  public dispose(): void {
    this.subscription = null;
  }

  /**
   * 重新连接
   * 连接超时或连接意外中断时重新建立连接
   *
   * @param time 连接时间
   */
  private reconnection(listener: (eventList: EventItem[]) => void, time: number): void {
    if (this.reconnectionCount > RECONNECTION_COUNT_MAX) {
      throw new Error('reconnection failed!');
    } else {
      this.initiate();
      this.open(listener, time);
    }
    this.reconnectionCount++;
  }
}
