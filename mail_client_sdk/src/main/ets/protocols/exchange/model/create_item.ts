/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MessageDisposition, SendMeetingInvitations } from '../utils/common_enum';
import { FolderName } from '../xml';
import { ModelBase } from './mode_base';

/**
 * CreateItem封装
 *
 * @since 2024-03-16
 */
export class CreateItem extends ModelBase {
  private _messageDisposition: MessageDisposition | null = null; // 此属性仅适用于电子邮件
  private _sendInvitationsMode: SendMeetingInvitations | null = null; // 日历项需要此属性
  private _distinguishedFolderId?: FolderName; // 系统目录的id
  private _folderId?: string; // 自定义目录的id

  /**
   * 获取要操作的系统目录ID
   *
   * @returns 系统目录ID
   */
  public get distinguishedFolderId(): FolderName | undefined {
    return this._distinguishedFolderId;
  }

  /**
   * 设置要操作的系统目录ID
   *
   * @param value 系统目录ID
   */
  public set distinguishedFolderId(value: FolderName) {
    this._distinguishedFolderId = value;
  }

  /**
   * 获取要操作的自定义目录的id
   *
   * @returns 自定义目录的id
   */
  public get folderId(): string | undefined {
    return this._folderId;
  }

  /**
   * 设置要操作的自定义目录的id
   *
   * @param value 自定义目录的id
   */
  public set folderId(value: string) {
    this._folderId = value;
  }

  /**
   * 获取邮件操作类型
   *
   * @returns 邮件操作类型
   */
  public get messageDisposition(): MessageDisposition | null {
    return this._messageDisposition;
  }

  /**
   * 设置邮件操作类型
   *
   * @param value 邮件操作类型
   */
  public set messageDisposition(value: MessageDisposition) {
    this._messageDisposition = value;
  }

  /**
   * 获取会议邀请模式
   *
   * @returns 会议邀请模式
   */
  public get sendInvitationsMode(): SendMeetingInvitations | null {
    return this._sendInvitationsMode;
  }

  /**
   * 设置会议邀请模式
   *
   * @param value 会议邀请模式
   */
  public set sendInvitationsMode(value: SendMeetingInvitations) {
    this._sendInvitationsMode = value;
  }
}
