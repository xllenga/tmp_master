/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { createMessageValidator } from '../../utils/validate';
import { CreateItem } from '../create_item';

/*
 * 取消会议传入参数
 * */
export interface CancelMeetingInfo {
  id: string;
  changeKey: string;
  content: string; // 取消会议原因
}

/*
 * 取消会议传入参数(批量)
 * */
export class CancelMeetingMessage extends CreateItem {
  items: CancelMeetingInfo[] = [];
  /*
   * 校验 id、changeKey 为必传项
   * */
  public validate(): void {
    const validRes = createMessageValidator(this.items, {
      id: { require: true },
      changeKey: { require: true }
    }).validate();
    if (validRes.hasError()) {
      throw new Error(validRes.getValidationError())
    }
  }
}
