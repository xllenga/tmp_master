/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UniteId } from '../mode_base';
import { Calendar } from './item_calendar';

export class CalendarFolder {
  folderId?: UniteId;
  parentFolderId?: UniteId;
  folderClass?: string;
  displayName?: string;
  totalCount?: number;
  childFolderCount?: number;
  effectiveRights?: EffectiveRights;
  extendedProperty?: ExtendedProperty;
  managedFolderInformation?: ManagedFolderInformation;
  sharingEffectiveRights?: SharingEffectiveRights;
  permissionSet?: PermissionSet;
}

export class ExtendedProperty {
  extendedFieldURI?: ExtendedFieldURI;
  values?: string[];
  value?: string;
}

export class ExtendedFieldURI {
  distinguishedPropertySetId?: string;
  propertySetId?: string;
  propertyTag?: string;
  propertyName?: string;
  propertyId?: string;
  propertyType?: string;
}

export class ManagedFolderInformation {
  canDelete: boolean;
  canRenameOrMove: boolean;
  mustDisplayComment: boolean;
  hasQuota: boolean;
  isManagedFoldersRoot: boolean;
  managedFolderId: string;
  comment: string;
  storageQuota: string;
  folderSize: number;
  homePage: string;
}

export enum SharingEffectiveRights {
  None,
  TimeOnly,
  TimeAndSubjectAndLocation,
  FullDetails
}

export class PermissionSet {
  calendarPermissions: CalendarPermissions;
}

export class UserId {
  distinguishedUser: string;
}

export class CalendarPermission {
  userId: UserId;
  canCreateItems: boolean;
  canCreateSubFolders: boolean;
  isFolderOwner: boolean;
  isFolderVisible: boolean;
  isFolderContact: boolean;
  editItems: string;
  deleteItems: string;
  readItems: string;
  calendarPermissionLevel: string;
}

export class CalendarPermissions {
  calendarPermission: CalendarPermission[];
}

export class Body {
  bodyType?: string;
  isTruncated?: boolean;
  content?: string;
}

export class EffectiveRights {
  createAssociated: boolean;
  createContents: boolean;
  createHierarchy: boolean;
  Delete: boolean;
  modify: boolean;
  read: boolean;
  viewPrivateItems: boolean;
}

export class FlagStatus {
  flagStatus: string;
}

export class Organizer {
  mailbox: Mailbox;
}

export class Mailbox {
  name: string;
  emailAddress: string;
  routingType: string;
  mailboxType: string;
  itemId: UniteId;
}

export class Attendee {
  mailbox: Mailbox;
  responseType: string;
}

export type RootFolder = {
  totalItemsInView: string;
  includesLastItemInRange: boolean;
}

export class Room {
  id: Mailbox;
}

export class CalendarEvent{
  startTime: string;
  endTime: string;
  busyType: string;
}

export class WorkingHours {
  timeZone: TimeZone;
  endTime: string;
  busyType: string;
  workingPeriodArray: WorkingPeriod[];
}

export class TimeZone {
  bias: string;
  standardTime: Time;
  daylightTime: Time;
}

export class Time {
  bias: string;
  time: string;
  dayOrder: number;
  month: number;
  dayOfWeek: string;
}

export class WorkingPeriod {
  dayOfWeek: string;
  startTimeInMinutes: number;
  endTimeInMinutes: number;
}

export class SuggestionDayResult {
  date: string;
  dayQuality: string;
  suggestionArray: Suggestion[];
}

export class Suggestion {
  meetingTime: string;
  isWorkTime: boolean;
  suggestionQuality: string;
  attendeeConflictDataArray: IndividualAttendeeConflictData[];
}

export class IndividualAttendeeConflictData {
  busyType: string;
}

export class Recurrence {
  weeklyRecurrence?: WeeklyRecurrence;
  endDateRecurrence?: EndDateRecurrence;
}

export class WeeklyRecurrence {
  interval: number;
  daysOfWeek: string;
}

export class EndDateRecurrence {
  startDate: string;
  endDate: string;
}

export class ConflictResults {
  count: number;
}

export class Attachments {
  itemAttachment?: ItemAttachment;
  fileAttachment?: FileAttachment;
}

export class ItemAttachment {
  attachmentId: string;
  name: string;
  contentType: string;
  contentId: string;
  contentLocation: string;
  size: string;
  lastModifiedTime: string;
  isInline: boolean;
  item?: any;
  message?: any;
  calendarItem?: any;
  contact?: any;
  task?: any;
  meetingMessage?: any;
  meetingRequest?: any;
  meetingResponse?: any;
  meetingCancellation?: any;
}

export class FileAttachment {
  attachmentId: string;
  name: string;
  contentType: string;
  contentId: string;
  contentLocation: string;
  size: string;
  lastModifiedTime: string;
  isInline: boolean;
  isContactPhoto: boolean;
  content: string;
}

export class InternetMessageHeader {
  headerName: string;
}

export class ResponseObjects {
  acceptItem: any;
  tentativelyAcceptItem: any;
  declineItem: any;
  proposeNewTime: any;
  replyToItem: any;
  forwardItem: any;
  replyAllToItem: any;
  cancelCalendarItem: any;
  removeItem: any;
  postReplyItem: any;
  suppressReadReceipt: any;
  acceptSharingInvitation: any;
}

export class Occurrence {
  itemId: UniteId;
  start: string;
  end: string;
  originalStart: string;
}

export class BaseResponseMessage {
  responseClass: string;
  responseCode: string;
}

export class FolderResponseMessage extends BaseResponseMessage {
  folders: CalendarFolder[];
}

export class ItemResponseMessage extends BaseResponseMessage {
  calendar: Calendar;
}

export class FindItemResponseMessage extends BaseResponseMessage {
  rootFolder: RootFolder;
  items: Calendar[];
}

export class RoomListsResponseMessage extends BaseResponseMessage {
  roomLists: Mailbox[];
}

export class RoomsResponseMessage extends BaseResponseMessage {
  rooms: Room[];
}

export class UserAvailabilityResponseMessage {
  freeBusyResponseArray: FreeBusyResponseMessage[];
  suggestionsResponse: SuggestionsResponseMessage;
}

export class FreeBusyResponseMessage extends BaseResponseMessage {
  freeBusyViewType: string;
  calendarEventArray: CalendarEvent[];
  workingHours: WorkingHours;
}

export class SuggestionsResponseMessage extends BaseResponseMessage {
  suggestionDayResultArray: SuggestionDayResult[];
}





