/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FreeBusyStatus } from '../../xml/xml_attribute';
import { BodyType, Importance, Sensitivity } from '../mail/item_message';
import { CreateItem } from '../create_item';
import { createMessageValidator } from '../../utils/validate';

export class AppointmentMessage {
  public subject: string = '';
  public bodyType: BodyType = BodyType.Text;
  public bodyText: string = '';
  public location = '';
  public requiredAttendees: string[] = [];
  public optionalAttendees: string[] = [];

  public reminderIsSet: boolean = true;
  public reminderMinutesBeforeStart: number = 60
  public start: Date;
  public end: Date;
  public isAllDayEvent: boolean = false;
  public legacyFreeBusyStatus: FreeBusyStatus;
  public sensitivity: Sensitivity;
  public importance: Importance;
}

export class CreateAppointmentMessage extends CreateItem {
  items: AppointmentMessage[] = [];
  public validate(): void {
    const res = createMessageValidator(this.items, {
      subject: { require: true },
      start: { require: true },
      end: { require: true }
    }).validate();
    if (res.hasError()) {
      throw new Error(res.getValidationError());
    }
  }
}
