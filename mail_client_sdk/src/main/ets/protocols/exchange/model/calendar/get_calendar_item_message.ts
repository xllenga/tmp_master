/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase, UniteId } from '../mode_base';
import { FindItemResponseMessage } from './calendar_response_types';
import { PropertySet, TraversalType } from '../../utils/common_enum';
import { FieldURISchema } from '../../xml/xml_attribute';

/**
 * 获取日历项详细数据请求报文数据
 */
export class GetCalendarItemMessage extends ModelBase {
  private _traversal: TraversalType;
  private _baseShape: PropertySet;
  private _fieldUris: FieldURISchema[];
  private _calendarView: CalendarView;
  private _parentFolderIds: UniteId[];

  public set traversal(value: TraversalType) {
    this._traversal = value;
  }

  public get traversal(): TraversalType {
    return this._traversal;
  }

  public set baseShape(value: PropertySet) {
    this._baseShape = value;
  }

  public get baseShape(): PropertySet {
    return this._baseShape;
  }

  public set fieldUris(value: FieldURISchema[]) {
    this._fieldUris = value;
  }

  public get fieldUris(): FieldURISchema[] {
    return this._fieldUris;
  }

  public set calendarView(value: CalendarView) {
    this._calendarView = value;
  }

  public get calendarView(): CalendarView {
    return this._calendarView;
  }

  public set parentFolderIds(value: UniteId[]) {
    this._parentFolderIds = value;
  }

  public get parentFolderIds(): UniteId[] {
    return this._parentFolderIds;
  }
}

export type CalendarView = {
  startDate: string;
  maxEntriesReturned: string;
  endDate: string;
}

/**
 * 获取日历项详细数据返回数据
 */
export class GetAppointmentItemResponseMessage extends ModelBase {
  private _responseMessages: FindItemResponseMessage[];

  public set responseMessages(value: FindItemResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): FindItemResponseMessage[] {
    return this._responseMessages;
  }
}