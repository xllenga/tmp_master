/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase } from '../mode_base';

const ERROR_MSG = 'Calendar folder name cannot be empty.'

/*
 * 创建日历文件夹参数对象
 * */
export class CreateCalendarMessage extends ModelBase {
  folderDisplayNames: string = '';
  public validate(): void {
    if (!this.folderDisplayNames) {
      throw new Error(ERROR_MSG);
    }
  }
}

/*
 * 创建日历文件夹参数对象（批量）
 * */
export class CreateCalendarsMessage extends ModelBase {
  folderDisplayNames: string[] = [];
  public validate(): void {
    const hasEmptyString = this.folderDisplayNames.some((folderName) => !folderName);
    if (hasEmptyString) {
      throw new Error(ERROR_MSG);
    }
  }
}

