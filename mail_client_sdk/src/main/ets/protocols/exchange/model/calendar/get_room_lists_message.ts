/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { RoomListsResponseMessage } from './calendar_response_types';
import { ModelBase } from '../mode_base';

export class GetRoomListsMessage extends ModelBase {

}

export class GetRoomListsResponseMessage extends ModelBase {
  private responseMessages: RoomListsResponseMessage;

  public setResponseMessages(value: RoomListsResponseMessage): void {
    this.responseMessages = value;
  }

  public getResponseMessages(): RoomListsResponseMessage {
    return this.responseMessages;
  }
}