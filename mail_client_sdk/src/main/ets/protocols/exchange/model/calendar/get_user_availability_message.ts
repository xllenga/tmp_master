/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UserAvailabilityResponseMessage } from './calendar_response_types';
import { ModelBase } from '../mode_base';

export class GetUserAvailabilityMessage extends ModelBase {
  private timeZoneDefinition : TimeZoneDefinition;
  private periods: Period[];
  private transitionsGroups: TransitionsGroup[];
  private transitions: Transitions;
  private mailboxDataArray: MailboxData[];
  private freeBusyViewOptions: FreeBusyViewOption;
  private suggestionsViewOptions: SuggestionsViewOption;

  public setTimeZoneDefinition(value: TimeZoneDefinition): void {
    this.timeZoneDefinition = value;
  }

  public getTimeZoneDefinition(): TimeZoneDefinition {
    return this.timeZoneDefinition;
  }

  public setPeriods(value: Period[]): void {
    this.periods = value;
  }

  public getPeriods(): Period[] {
    return this.periods;
  }

  public setTransitionsGroups(value: TransitionsGroup[]): void {
    this.transitionsGroups = value;
  }

  public getTransitionsGroups(): TransitionsGroup[] {
    return this.transitionsGroups;
  }

  public setTransitions(value: Transitions): void {
    this.transitions = value;
  }

  public getTransitions(): Transitions {
    return this.transitions;
  }

  public setMailboxDataArray(value: MailboxData[]): void {
    this.mailboxDataArray = value;
  }

  public getMailboxDataArray(): MailboxData[] {
    return this.mailboxDataArray;
  }

  public setFreeBusyViewOption(value: FreeBusyViewOption): void {
    this.freeBusyViewOptions = value;
  }

  public getFreeBusyViewOption(): FreeBusyViewOption {
    return this.freeBusyViewOptions;
  }

  public setSuggestionsViewOption(value: SuggestionsViewOption): void {
    this.suggestionsViewOptions = value;
  }

  public getSuggestionsViewOption(): SuggestionsViewOption {
    return this.suggestionsViewOptions;
  }
}

export type TimeZoneDefinition = {
  name: string,
  id: string,
}

export type Period = {
  bias: string,
  name: string,
  id: string,
}

export type TransitionsGroup = {
  id: string,
  recurringDayTransitions: RecurringDayTransition[],
}

export type Transitions = {
  transition: Transition,
  absoluteDateTransition: AbsoluteDateTransition,
}

export type Transition = {
  kind: string,
  to: number,
}

export type AbsoluteDateTransition = {
  kind: string,
  to: number,
  dateTime: string,
}

export type RecurringDayTransition = {
  to: string,
  kind: string,
  timeOffset: string,
  month: number,
  dayOfWeek: string,
  occurrence: number,
}

export type MailboxData = {
  address: string,
  attendeeType: string,
  excludeConflicts: boolean,
}

export type FreeBusyViewOption = {
  startTime: string,
  endTime: string,
  mergedFreeBusyIntervalInMinutes: number,
  requestedView: string,
}

export type SuggestionsViewOption = {
  goodThreshold: number,
  maximumResultsByDay: number,
  maximumNonWorkHourResultsByDay: number,
  meetingDurationInMinutes: number,
  minimumSuggestionQuality: string,
  startTime: string,
  endTime: string,
}

export class GetUserAvailabilityResponseMessage extends ModelBase {
  private responseMessages: UserAvailabilityResponseMessage;

  public setResponseMessages(value: UserAvailabilityResponseMessage): void {
    this.responseMessages = value;
  }

  public getResponseMessages(): UserAvailabilityResponseMessage {
    return this.responseMessages;
  }
}