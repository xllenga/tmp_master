/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DistinguishedPropertySetId, PropertySet, PropertyType,TraversalType } from '../../utils/common_enum';

import { FieldURISchema } from '../../xml/xml_attribute';

/**
 * 公共属性定义类
 * 用于定义属性和扩展属性
 *
 * @since 2024-03-23
 */
export class PropertyDefinition {
  private _fieldURIArr: (IPropertySet | FieldURISchema)[];
  private _propertySet: IPropertySet;
  private _fieldURIString: FieldURISchema;
  private _value: string | FieldURISchema;

  /**
   * 设置属性项集合
   *
   * @param value 属性项和自定义扩展属性数组
   */
  public set fieldURIArr(value: (IPropertySet | FieldURISchema)[]) {
    this._fieldURIArr = value;
  }

  /**
   * 获取属性项集合
   *
   * @returns value 属性项和自定义扩展属性数组
   */
  public get fieldURIArr(): (IPropertySet | FieldURISchema)[] {
    return this._fieldURIArr;
  }

  /**
   * 设置扩展属性
   *
   * @param value 扩展属性
   */
  public set propertySet(value: IPropertySet) {
    this._propertySet = value;
  }

  /**
   * 设置扩展属性
   *
   * @returns value 扩展属性
   */
  public get propertySet(): IPropertySet {
    return this._propertySet;
  }

  /**
   * 设置属性项
   *
   * @returns value 属性项
   */
  public set fieldURIString(value: FieldURISchema) {
    this._fieldURIString = value;
  }

  /**
   * 获取属性项
   *
   * @returns value 属性项
   */
  public get fieldURIString(): FieldURISchema {
    return this._fieldURIString;
  }

  /**
   * 设置属性项的值
   *
   * @param value 属性项
   */
  public set value(value: string | FieldURISchema) {
    this._value = value;
  }

  /**
   * 获取属性项的值
   *
   * @returns value 属性项
   */
  public get value(): string | FieldURISchema {
    return this._value;
  }

  /**
   * 公共属性定义类的构造方法
   * @param propertyDefinition 属性项或扩展属性
   * @param value 要比较的值或者要比较的属性项
   *
   * @since 2024-03-23
   */
  public constructor(propertyDefinition?: IPropertySet | FieldURISchema, value?: string | FieldURISchema) {
    if (typeof propertyDefinition === 'string') {
      this.fieldURIString = propertyDefinition;
    } else {
      this.propertySet = propertyDefinition;
    }
    this._value = value;
  }
}

/**
 * 搜索视图类
 * 搜索视图的项视图、排序顺序、项遍历和基本形状
 * @since 2024-03-23
 */
export class SearchView extends PropertyDefinition {
  private _itemView: IItemView;
  private _orderBy: ISortOrder;
  private _itemTraversal: TraversalType;
  private _baseShape: PropertySet;

  /**
   * 设置邮件项目返回的总数、偏移量、从尾部还是头部开始搜索
   *
   * @param value 邮件项目返回的总数、偏移量、项目从尾部还是头部开始搜索
   */
  public set itemView(value: IItemView) {
    this._itemView = value;
  }

  /**
   * 获取邮件项目返回的总数、偏移量、从尾部还是头部开始搜索
   *
   * @returns  邮件项目返回的总数、偏移量、项目从尾部还是头部开始搜索
   */
  public get itemView(): IItemView {
    return this._itemView;
  }

  /**
   * 设置返回邮件项目的排序方式
   *
   * @param value 返回邮件项目的排序方式
   */
  public set orderBy(value: ISortOrder) {
    this._orderBy = value;
  }

  /**
   * 获取返回邮件项目的排序方式
   *
   * @returns  返回邮件项目的排序方式
   */
  public get orderBy(): ISortOrder {
    return this._orderBy;
  }

  /**
   * 设置搜索邮件项目遍历的深度
   *
   * @param value 搜索邮件项目遍历的深度
   */
  public set itemTraversal(value: TraversalType) {
    this._itemTraversal = value;
  }

  /**
   * 获取搜索邮件项目遍历的深度
   *
   * @returns  搜索邮件项目遍历的深度
   */
  public get itemTraversal(): TraversalType {
    return this._itemTraversal;
  }

  /**
   * 设置返回的邮件属性，只有id或默认或全部
   *
   * @param value 返回的邮件属性，只有id或默认或全部
   */
  public set baseShape(value: PropertySet) {
    this._baseShape = value;
  }

  /**
   * 获取返回的邮件属性，只有id或默认或全部
   *
   * @returns  返回的邮件属性，只有id或默认或全部
   */
  public get baseShape(): PropertySet {
    return this._baseShape;
  }
}

/**
 * 扩展属性的类型
 */
export interface IPropertySet {
  propertySetId: string;
  propertyName: string;
  DistinguishedPropertySetId?: DistinguishedPropertySetId;
  propertySet?: string;
  propertyType: PropertyType;
  PropertyTag?: string;
}

/**
 * 排序的类型
 */
export interface ISortOrder {
  orderBy: SortOrder;
  FieldURI: FieldURISchema;
}

/**
 * 分页的类型
 */
export interface IItemView {
  offset: string;
  basePoint: BasePoint;
  maxEntriesReturned?: string;
}

/**
 * 从尾部还是头部开始搜索的枚举值
 */
export enum BasePoint {
  Beginning,
  End,
}

/**
 * 排序的枚举值
 */
export enum SortOrder {
  Ascending,
  Descending,
}