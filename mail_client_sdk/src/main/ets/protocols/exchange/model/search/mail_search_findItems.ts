/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase, UniteId } from '../mode_base';
import { FolderName } from '../../xml/xml_attribute';
import {
  Composite,
  ContainsSubstring,
  ExcludesBitmask,
  Exists,
  IsEqualTo,
  IsGreaterThan,
  IsGreaterThanOrEqual,
  IsLessThan,
  IsLessThanOrEqual,
  Not
} from './mail_search_filter';
import { SearchView } from './mail_search_view';

/**
 * 搜索筛选器类型别名
 */
export type Filters = ContainsSubstring
  | ExcludesBitmask
  | Exists
  | IsEqualTo
  | IsGreaterThan
  | IsGreaterThanOrEqual
  | IsLessThan
  | IsLessThanOrEqual
  | Not
  | Composite;

/**
 * 搜索类
 * 主要用于存储搜索xml报文数据，Request将这些数据组合成请求报文
 *
 * @since 2024-03-18
 */
export class FindItems<T extends Filters> extends ModelBase {
  private _folder: FolderName | UniteId;
  private _filter?: T;
  private _queryString?: string;
  private _view: SearchView;

  /**
   * 设置需要搜索的文件夹
   *
   * @param value 文件夹名称或文件夹id或者changeKey
   */
  public set folder(value: FolderName | UniteId) {
    this._folder = value;
  }

  /**
   * 获取需要搜索的文件夹
   *
   * @returns 文件夹名称或文件夹id或者changeKey
   */
  public get folder(): FolderName | UniteId {
    return this._folder;
  }

  /**
   * 设置搜索筛选器
   *
   * @param value 搜索筛选器
   */
  public set filter(value: T) {
    this._filter = value;
  }

  /**
   * 获取搜索筛选器
   *
   * @returns 搜索筛选器
   */
  public get filter(): T {
    if (this._filter) {
      return this._filter;
    }
  }

  /**
   * 设置搜索视图
   *
   * @param value 搜索视图对象
   */
  public set view(value: SearchView) {
    this._view = value;
  }

  /**
   * 获取搜索视图
   *
   * @returns value 搜索视图对象
   */
  public get view(): SearchView {
    return this._view;
  }

  /**
   * 设置查询字符串
   *
   * @param value 查询字符串
   */
  public set queryString(value: string) {
    this._queryString = value;
  }

  /**
   * 获取查询字符串
   *
   * @returns 查询字符串
   */
  public get queryString(): string {
    if (this._queryString) {
      return this._queryString;
    }
  }
}

/**
 * 返回报文解析后的数据类型
 */
export interface IEffectiveRights {
  createAssociated?: string; // 指示客户端是否可以创建关联的内容表。 此属性仅用于文件夹对象。
  createContents?: string; // 指示客户端是否可以创建内容表。 此属性仅用于文件夹对象。
  createHierarchy?: string; // 指示客户端是否可以创建层次结构表。 此属性仅用于文件夹对象。
  delete?: string; // 指示客户端是否可以删除文件夹或项目。
  modify?: string; // 指示客户端是否可以修改文件夹或项目。
  read?: string; // 指示客户端是否可以读取文件夹或项目。
  viewPrivateItems?: string; // 指示是否可以查看私有项目。
}

export interface IFlag {
  flagStatus?: string; // 包含当前文件夹中项目的聚合标志状态。
}

export interface IMailBox {
  name?: string; // 定义邮箱用户的名称。 此元素为可选。
  emailAddress?: string; //定义邮箱用户的 简单邮件传输协议 (SMTP) 地址。
  routingType?: string; // 定义供邮箱使用的路由。 默认为 SMTP。 此元素为可选。
  mailboxType?: string; // 定义邮箱用户的类型。 此元素为可选。
}

export interface IItemId {
  id: string; // 包含项目在数据存储区中的唯一标识符Exchange项。 此属性是只读的
  changeKey: string; // 包含项目在数据存储区中的唯一标识符Exchange项。 此属性是只读的
}

export interface IFindItemResp {
  itemId: IItemId; // 表示邮箱的id和ChangeKey
  subject: string; // 表示用于存储Exchange响应对象的主题。 主题限制为 255 个字符。
  parentFolderId: string; // 表示包含项目或文件夹的父文件夹的标识符。
  parentFolderChangeKey: string; // 表示包含项目或文件夹的父文件夹的标识符。
  itemClass: string; // 表示项目的邮件类。
  sensitivity: string; // 指示项目的敏感度级别。
  dateTimeReceived: string; // 表示接收邮箱中的项目的日期和时间。
  size: string; // 表示项目的大小（以字节为单位）。
  importance: string; // 描述项目的重要性。
  isSubmitted: string; // 指示项目是否已提交到发件箱默认文件夹。
  isDraft: string; // 表示项目尚未发送。
  isResend: string; // 指示项目先前是否已发送。
  isUnmodified: string; // 指示项目是否已修改。
  dateTimeSent: string; // 表示邮箱中项目的发送日期和时间。
  dateTimeCreated: string; // 表示邮箱中给定项目的创建日期和时间。
  displayCc: string; // 表示用于抄送行内容的显示字符串。 这是所有抄送收件人显示名称的串联字符串。
  displayTo: string; // 表示用于"To"框内容的显示字符串。 这是所有"收件人"收件人显示名称的串联字符串。
  hasAttachments: string; // 表示一个属性 ，如果项目 具有至少一个可见附件，则该属性设置为 true。
  culture: string; // 表示邮箱中给定项目的区域性。
  effectiveRights: IEffectiveRights; // 包含客户端的权利基础的项或文件夹的权限设置。
  lastModifiedName: string; // 包含显示名称用户修改项目的详细信息。
  lastModifiedTime: string; // 指示项目的上次修改时间。
  isAssociated: string; // 指示项目是否与文件夹关联。
  webClientReadFormQueryString: string; // 表示要连接到 WebApp终结点以MicrosoftOfficeOutlookWebApp终结点以读取OutlookWebApp.
  conversationId: string; //包含项目或对话的标识符。
  flag: IFlag; // Flag 元素指定邮箱项目上的标志。
  instanceKey: string; // InstanceKey 元素指定项或会话的实例键。
  sender: IMailBox; //定义邮箱用户
  isReadReceiptRequested: string; // 指示项目的发件人是否请求已读回执。
  conversationIndex: string; // 包含二进制 ID，表示此消息所属的线程。
  conversationTopic: string; // 表示对话标识符。
  from: IMailBox; // 表示发送邮件的收件人。
  internetMessageId: string; // 表示项目的 Internet 邮件标识符。
  isRead: string; // 表示是否已读
  receivedBy: IMailBox; // 标识委派访问方案中的代理
  receivedRepresenting: IMailBox; // 标识委派访问方案中的主体
}

export type FindItemRespTypes = IFindItemResp[];

/**
 * 搜索返回结果类
 * 主要用于存储搜索报文返回数据
 *
 * @since 2024-04-08
 */
export class FindItemResp extends ModelBase {
  /**
   * 是否包含搜索的最后一个项目
   */
  private _includesLastItemInRange: string;

  /**
   * 设置是否包含搜索的最后一个项目的布尔值
   *
   * @param value 包含搜索的最后一个项目的布尔值
   */
  public set includesLastItemInRange(value: string) {
    this._includesLastItemInRange = value;
  }

  /**
   * 获取是否包含搜索的最后一个项目的布尔值
   *
   * @returns value 包含搜索的最后一个项目的布尔值
   */
  public get includesLastItemInRange(): string {
    return this._includesLastItemInRange;
  }

  /**
   * 搜索的邮箱项目偏移量
   */
  private _indexedPagingOffset: string;

  /**
   * 设置搜索的邮箱数量的偏移量
   *
   * @param value 搜索的邮箱数量的偏移量
   */
  public set indexedPagingOffset(value: string) {
    this._indexedPagingOffset = value;
  }

  /**
   * 获取搜索的邮箱数量的偏移量
   *
   * @returns value 搜索的邮箱数量的偏移量
   */
  public get indexedPagingOffset(): string {
    return this._indexedPagingOffset;
  }

  /**
   * 搜索的邮箱总项目数
   */
  private _totalItemsInView: string;

  /**
   * 设置搜索的邮箱总项目数
   *
   * @param value 搜索的邮箱总项目数
   */
  public set totalItemsInView(value: string) {
    this._totalItemsInView = value;
  }

  /**
   * 获取搜索的邮箱总项目数
   *
   * @returns value 搜索的邮箱总项目数
   */
  public get totalItemsInView(): string {
    return this._totalItemsInView;
  }

  /**
   * 搜索的邮件项目
   */
  private _findItemsMessages: FindItemRespTypes;

  /**
   * 设置搜索的邮箱项目
   *
   * @param value 搜索的邮箱项目
   */
  public set findItemMessages(value: FindItemRespTypes) {
    this._findItemsMessages = value;
  }

  /**
   * 获取搜索的邮箱项目
   *
   * @returns value 搜索的邮箱项目
   */
  public get findItemMessages(): FindItemRespTypes {
    return this._findItemsMessages;
  }
}