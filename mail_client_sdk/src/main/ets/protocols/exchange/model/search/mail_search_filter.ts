/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FieldURISchema } from '../../xml/xml_attribute';
import { PropertyDefinition, IPropertySet } from './mail_search_view';
import { Filters } from './mail_search_findItems';

/**
 * 搜索筛选器命名空间
 * 用于返回不同类型的搜索筛选器
 *
 * @since 2024-03-20
 */
export namespace SearchMailFilter {
  /**
   * 包含搜索筛选器类
   * 用于检查文本属性中是否存在子字符串。
   *
   * @since 2024-03-20
   */
  export class ContainsSubstring extends PropertyDefinition {
    private _containmentMode: ContainmentMode;
    private _comparisonMode: ComparisonMode;
    private _containsSubstringValue: string;

    /*
     * 获取该比较模式包含模式
     *
     * @return 比较模式的属性值
     */
    public get containmentMode(): ContainmentMode {
      return this._containmentMode;
    }

    /*
     * 获取比较模式
     *
     * @returns 比较模式的属性值
     */
    public get comparisonMode(): ComparisonMode {
      return this._comparisonMode;
    }

    /*
     * 获取要将指定属性与进行比较的值。
     */
    public get value(): string {
      return this._containsSubstringValue;
    }

    /**
     * ContainsSubstring类的构造方法
     *
     * @param {fieldURISchema} 筛选搜索的项属性
     * @param {value} 与指定属性进行比较的值
     * @param {containmentMode} 比较的包含模式
     * @param {comparisonMode} 比较模式
     */
    public constructor(PropertyDefinitionBase: IPropertySet | FieldURISchema,
      value: string,
      containmentMode: ContainmentMode,
      comparisonMode: ComparisonMode) {
      super(PropertyDefinitionBase)
      this._containsSubstringValue = value;
      this._containmentMode = containmentMode;
      this._comparisonMode = comparisonMode;
    }
  }

  /**
   * 位掩码排除搜索筛选器类
   * 将整数属性作为位掩码进行搜索，并返回未在指定属性的值中设置特定位的结果
   *
   * @since 2024-03-22
   */
  export class ExcludesBitmask extends PropertyDefinition {
    private _bitmask: string;

    /*
    * 获取位掩码
    *
    * @returns  位掩码
    */
    public get bitmask(): string {
      return this._bitmask;
    }

    /**
     * ExcludesBitmask类的构造方法
     *
     * @param {bitmask} 与属性进行比较的位掩码
     * @param {bitmaskValue} 用于筛选搜索的项属性
     */
    public constructor(bitmaskValue: IPropertySet | FieldURISchema, bitmask: string,) {
      super(bitmaskValue);
      this._bitmask = bitmask;
    }
  }

  /**
   * 存在搜索筛选器类
   * 搜索具有特定属性的项，而不考虑值
   *
   * @since 2024-03-22
   */
  export class Exists extends PropertyDefinition {
    /**
     * Exists类的构造方法
     *
     * @param {existsValue} 用于筛选搜索的项属性
     */
    public constructor(existsValue: IPropertySet | FieldURISchema) {
      super(existsValue);
    }
  }

  /**
   * 相等搜索筛选器类
   * 可以搜索指定属性值等于特定值的所有项。 要比较的值可以是常量值，也可以是每个项上另一个属性的值。
   *
   * @since 2024-03-22
   */
  export class IsEqualTo extends PropertyDefinition {
    /**
     * IsEqualTo类的构造方法
     *
     * @param {equalToValue} 用于筛选搜索的项属性
     * @param {value} 与属性比较的值或者其他属性
     */
    public constructor(equalToValue: IPropertySet | FieldURISchema, value: string | FieldURISchema) {
      super(equalToValue, value);
    }
  }

  /**
   * 不相等搜索筛选器类
   * 可以搜索指定属性值不等于特定值的所有项。 要比较的值可以是常量值，也可以是每个项上另一个属性的值。
   *
   * @since 2024-03-22
   */
  export class IsNotEqualTo extends PropertyDefinition {
    /**
     * IsNotEqualTo类的构造方法
     *
     * @param notEqualToValue 用于筛选搜索的项属性
     * @param value 与属性比较的值或者其他属性
     */
    public constructor(notEqualToValue: IPropertySet | FieldURISchema, value: string | FieldURISchema) {
      super(notEqualToValue, value);
    }
  }

  /**
   * 关系测试搜索筛选器 大于
   * 可以搜索指定属性中值大于(>)指定值的所有项。要比较的值可以是常量值，也可以是每个项上另一个属性的值
   *
   * @since 2024-03-22
   */
  export class IsGreaterThan extends PropertyDefinition {
    /**
     * IsGreaterTan类的构造方法
     *
     * @param greaterTan 用于筛选搜索的项属性
     * @param value 与属性比较的值或者其他属性
     */
    public constructor(greaterTan: IPropertySet | FieldURISchema, value: string | FieldURISchema) {
      super(greaterTan, value);
    }
  }

  /**
   * 关系测试搜索筛选器 大于等于
   * 可以搜索指定属性中值大于等于(>=)指定值的所有项。要比较的值可以是常量值，也可以是每个项上另一个属性的值
   *
   * @since 2024-03-22
   */
  export class IsGreaterThanOrEqual extends PropertyDefinition {
    /**
     * IsGreaterTanOrEqual类的构造方法
     *
     * @param greaterTanOrEqualValue 用于筛选搜索的项属性
     * @param value 与属性比较的值或者其他属性
     */
    public constructor(greaterTanOrEqualValue: IPropertySet | FieldURISchema, value: string | FieldURISchema) {
      super(greaterTanOrEqualValue, value);
    }
  }

  /**
   * 关系测试搜索筛选器 小于
   * 可以搜索指定属性中值小于(<)指定值的所有项。要比较的值可以是常量值，也可以是每个项上另一个属性的值
   *
   * @since 2024-03-22
   */
  export class IsLessThan extends PropertyDefinition {
    /**
     * IsLessThan类的构造方法
     *
     * @param lessValue 用于筛选搜索的项属性
     * @param value 与属性比较的值或者其他属性
     */
    public constructor(lessValue: IPropertySet | FieldURISchema, value: string | FieldURISchema) {
      super(lessValue, value);
    }
  }

  /**
   * 关系测试搜索筛选器 小于等于
   * 可以搜索指定属性中值小于等于(<=)指定值的所有项。要比较的值可以是常量值，也可以是每个项上另一个属性的值
   *
   * @since 2024-03-22
   */
  export class IsLessThanOrEqual extends PropertyDefinition {
    /**
     * IsLessThanOrEqual类的构造方法
     *
     * @param lessOrEqualValue 用于筛选搜索的项属性
     * @param value 与属性比较的值或者其他属性
     */
    public constructor(lessOrEqualValue: IPropertySet | FieldURISchema, value: string | FieldURISchema) {
      super(lessOrEqualValue, value);
    }
  }

  /**
   * 否定搜索筛选器类
   * 使用否定筛选器可以否定另一个筛选器并获取相反的搜索结果。
   *
   * @since 2024-03-22
   */
  export class Not extends PropertyDefinition {
    private _searchFilter: Filters;

    /*
    * 获取搜索筛选器
    *
    * @returns  搜索筛选器
    */
    public get searchFilter(): Filters {
      return this._searchFilter;
    }

    /**
     * Not类的构造方法
     *
     * @param searchFilter 搜索筛选器
     */
    public constructor(searchFilter: Filters) {
      super();
      this._searchFilter = searchFilter;
    }
  }

  /**
   * 复合搜索筛选器
   * 使用复合筛选器可以组合多个筛选器来创建更复杂的搜索条件。 可以使用逻辑运算符 AND 或 OR 组合条件
   *
   * @since 2024-03-22
   */
  export class Composite extends PropertyDefinition {
    private _searchFilters: Filters[];
    private _logicalOperator: LogicalOperator;

    /*
    * 获取搜索筛选器数组
    *
    * @returns  搜索筛选器数组
    */
    public get searchFilters(): Filters[] {
      return this._searchFilters;
    };

    /*
    * 获取逻辑运算符
    *
    * @returns  逻辑运算符
    */
    public get logicalOperator(): LogicalOperator {
      return this._logicalOperator;
    }

    /**
     * Composite类的构造方法
     *
     * @param logicalOperator 逻辑运算符 or或and
     * @param searchFilters 搜索筛选器的集合
     */
    public constructor(logicalOperator: LogicalOperator, searchFilters: Filters[]) {
      super();
      this._logicalOperator = logicalOperator;
      this._searchFilters = searchFilters;
    }
  }
}

/*
 * 搜索筛选器的类型别名
*/
export type ContainsSubstring = SearchMailFilter.ContainsSubstring;

export type ExcludesBitmask = SearchMailFilter.ExcludesBitmask;

export type Exists = SearchMailFilter.Exists;

export type IsEqualTo = SearchMailFilter.IsEqualTo;

export type IsNotEqualTo = SearchMailFilter.IsNotEqualTo;

export type IsGreaterThan = SearchMailFilter.IsGreaterThan;

export type IsGreaterThanOrEqual = SearchMailFilter.IsGreaterThanOrEqual;

export type IsLessThan = SearchMailFilter.IsLessThan;

export type IsLessThanOrEqual = SearchMailFilter.IsLessThanOrEqual;

export type Not = SearchMailFilter.Not;

export type Composite = SearchMailFilter.Composite;


/*
 * 复合搜索筛选器的逻辑运算符枚举值
 */
export enum LogicalOperator {
  And,
  Or,
}

/*
 * 比较的包含模式枚举值
 */
export enum ContainmentMode {
  FullString,
  Prefixed,
  Substring,
  PrefixOnWords,
  ExactPhrase,
}

/*
 * 比较模式枚举值
 */
export enum ComparisonMode {
  Exact,
  IgnoreCase,
  IgnoreNonSpacingCharacters,
  IgnoreCaseAndNonSpacingCharacters,
}
