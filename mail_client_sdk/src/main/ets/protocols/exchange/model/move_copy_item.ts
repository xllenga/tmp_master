/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ResponseMessage } from '../response/response_base';

import {
  JsObjectElement,
  SoapXmlWriter,
  XmlAttribute,
  XmlElement,
  XmlNamespace,
  XmlReadable,
  XmlWritable } from '../xml';
import { FolderId } from './folder/folder';
import { ModelBase, UniteId } from './mode_base';
import { getLogger, Logger } from '../../../utils/log';
import { Message } from './mail/item_message';

const logger: Logger = getLogger("MoveCopyResp");

/**
 * 移动项和复制项的基类
 *
 * @since 2024-04-13
 */
export class MoveCopyBase extends ModelBase implements XmlWritable {
  private _toFolderId: FolderId; // 目标文件夹
  private _itemIds: UniteId[] = []; // 文件夹数组
  private _returnNewItemIds?: 'true' | 'false'; // 是否在响应中返回新项的标识符

  /**
   * 获取目标文件夹
   *
   * @returns 目标文件夹
   */
  public get toFolderId(): FolderId {
    return this._toFolderId;
  }

  /**
   * 设置目标文件夹
   *
   * @param val 目标文件夹
   */
  public set toFolderId(val: FolderId) {
    this._toFolderId = val;
  }

  /**
   * 获取文件夹数组
   *
   * @returns 文件夹数组
   */
  public get itemIds(): UniteId[] {
    return this._itemIds;
  }

  /**
   * 设置文件夹数组
   *
   * @param val 文件夹数组
   */
  public set itemIds(val: UniteId[]) {
    this._itemIds = val;
  }

  /**
   * 获取是否在响应中返回新项的标识符
   *
   * @returns 是否在响应中返回新项的标识符
   */
  public get returnNewItemIds(): 'true' | 'false' | undefined {
    return this._returnNewItemIds;
  }

  /**
   * 设置是否在响应中返回新项的标识符
   *
   * @param val 是否在响应中返回新项的标识符
   */
  public set returnNewItemIds(val: 'true' | 'false') {
    this._returnNewItemIds = val;
  }

  /**
   * 写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement('', XmlElement.NAME_TO_FOLDER_ID);
    this.toFolderId.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeStartElement('', XmlElement.NAME_ITEM_IDS);
    this.itemIds?.forEach(item => {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_ITEM, new Map([
        [XmlAttribute.NAME_KEY, item.id],
        [XmlAttribute.NAME_CHANGE_KEY, item.changeKey],
      ]));
    })
    soapXmlWriter.writeEndElement();

    if (this.returnNewItemIds) {
      soapXmlWriter.writeElementWithValue('', XmlElement.NAME_RETURN_NEW_ITEM_IDS, this.returnNewItemIds);
    }
  }
}

/**
 * 移动项的请求
 *
 * @since 2024-04-13
 */
export class MoveItem extends MoveCopyBase implements XmlWritable {
  /**
   * 写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement('', XmlElement.NAME_MOVE_ITEM)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_MESSAGE)
      .writeAttribute(`${XmlNamespace.XMLNS}:${XmlNamespace.TYPE}`, XmlNamespace.SCHEMA_TYPE);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 复制项的请求
 *
 * @since 2024-04-13
 */
export class CopyItem extends MoveCopyBase implements XmlWritable {
  /**
   * 写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement('', XmlElement.NAME_COPY_ITEM)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_MESSAGE);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 移动item响应模型
 *
 * @since 2024-04-15
 */
export class MoveCopyItemResp extends ModelBase implements XmlReadable {
  private _responseMessages: ResponseMessage<Message>[]; // item返回结构数组

  /**
   * 设置item返回结构数组
   *
   * @param value item返回结构数组
   */
  public set responseMessages(value: ResponseMessage<Message>[]) {
    this._responseMessages = value;
  }

  /**
   * 获取item返回结构数组
   *
   * @returns ResponseMessage[] item返回结构数组
   */
  public get responseMessages(): ResponseMessage<Message>[] {
    return this._responseMessages;
  }

  readFromXml(jsObject: JsObjectElement[]): void {
    logger.info('MoveItemResp', jsObject);
  }
}