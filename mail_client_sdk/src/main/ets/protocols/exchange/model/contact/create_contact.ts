/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateItem } from '../create_item';
import { Contact } from './item_contact';
import { ModelBase } from '../mode_base';

/**
 * 新增与更新联系人请求数据类数组
 *
 * @since 2024-4-9
 */
export class CreateContact extends CreateItem {
  private _contacts: Contact[];

  public set contacts(value: Contact[]) {
    this._contacts = value;
  }

  public get contacts(): Contact[] {
    return this._contacts;
  }
}

/**
 * 创建联系人XMl报文解析类
 *
 * @since 2024-03-28
 */
export class CreateContactResp extends ModelBase {
  private _contacts: CreateContactResponseData[];

  public set contacts(value: CreateContactResponseData[]) {
    this._contacts = value;
  }

  public get contacts(): CreateContactResponseData[] {
    return this._contacts;
  }
}

/**
 * 创建联系人XML报文解析子类数组项
 *
 * @since 2024-04-12
 */
export class CreateContactResponseData {
  private _responseCode: string;// 返回报文状态
  private _items: CreateContactResponseItem;// 返回报文内容
  private _messageText: string;// 错误报文内容
  private _responseClass: string;// 错误报文信息

  public set responseCode(value: string) {
    this._responseCode = value;
  }

  public get responseCode(): string {
    return this._responseCode;
  }

  public set items(value: CreateContactResponseItem) {
    this._items = value;
  }

  public get items(): CreateContactResponseItem {
    return this._items;
  }

  public set messageText(value: string) {
    this._messageText = value;
  }

  public get messageText(): string {
    return this._messageText;
  }

  public set responseClass(value: string) {
    this._responseClass = value;
  }

  public get responseClass(): string {
    return this._responseClass;
  }
}

/**
 * 创建联系人XML报文解析子类型
 *
 * @since 2024-04-12
 */
export interface CreateContactResponseItem {
  id: string;
  changeKey: string;
}