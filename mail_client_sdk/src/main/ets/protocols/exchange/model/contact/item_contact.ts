/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { XmlNamespace, XmlWritable } from '../../xml/index';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { FileAsMapping, PhysicalAddressElementContent } from './get_contact';

/**
 * 新增与更新联系人请求数据类
 * setter会为contactUpdateItem设置值
 * 特殊数据类型会取数组第一个设置值
 * 新增特殊数据类型的静态属性与其对应的getter、setter需在更新与新增的switch判断中增加对应判断与报文构建逻辑
 *
 * @since 2024-03-24
 */
export class Contact implements XmlWritable {
  private _emailAddresses: EmailAddress[]; //表示联系人的电子邮件地址的集合。
  private _physicalAddresses: PhysicalAddress[]; //包含与联系人关联的物理地址的集合。
  private _phoneNumbers: PhoneNumber[]; //表示联系人的电话号码集合。
  private _imAddresses: ImAddress[]; //表示联系人的即时消息地址的集合。
  private _fileAs: string; //表示如何在“联系人”文件夹中提交联系人。
  private _fileAsMapping: FileAsMapping; //定义如何构造联系人显示的内容。
  private _displayName: string; //定义联系人的显示名称。
  private _displayNamePrefix: string; //定义联系人的职务显示名称。
  private _givenName: string; //包含联系人的给定姓名。
  private _initials: string; //表示联系人的首字母缩写。
  private _middleName: string; //表示联系人的中间名。
  private _nickname: string; //表示联系人的昵称。
  private _yomiGivenName: string; //表示联系人的名字拼音。
  private _yomiSurname: string; //表示联系人的姓氏拼音。
  private _yomiCompanyName: string; //表示联系人公司拼音。
  private _personalHomePage: string; //表示联系人个人网页。
  private _companyName: string; //表示与联系人关联的公司名称。
  private _assistantName: string; //表示联系人的助手。
  private _birthday: string; //表示联系人的出生日期。
  private _businessHomePage: string; //表示联系人的主页 (Web 地址) 。
  private _children: string; //包含联系人的子女的姓名。
  private _department: string; //表示联系人的工作部门。
  private _generation: string; //表示在联系人全名之后的代系缩写。
  private _jobTitle: string; //表示联系人的职务。
  private _manager: string; //表示联系人的经理。
  private _mileage: string; //表示联系人项的里程。
  private _officeLocation: string; //表示联系人的办公室位置。
  private _postalAddressIndex: string; //表示物理地址的显示类型。
  private _profession: string; //表示联系人的职业。
  private _spouseName: string; //表示联系人的配偶/伴侣的姓名。
  private _surname: string; //表示联系人的姓氏。
  private _weddingAnniversary: string; //包含联系人的结婚纪念日。
  private _contactUpdateItem: ContactUpdateItem; //更新参数存储器

  public set emailAddresses(value: EmailAddress[]) {
    this._emailAddresses = value;
    value[0].typeName = 'emailAddresses';
    this._contactUpdateItem = {
      key: 'emailAddresses',
      value: value[0]
    }
  }

  public get emailAddresses(): EmailAddress[] {
    return this._emailAddresses;
  }

  public set physicalAddresses(value: PhysicalAddress[]) {
    this._physicalAddresses = value;
    value[0].typeName = 'physicalAddresses';
    this._contactUpdateItem = {
      key: 'physicalAddresses',
      value: value[0]
    }
  }

  public get physicalAddresses(): PhysicalAddress[] {
    return this._physicalAddresses;
  }

  public set phoneNumbers(value: PhoneNumber[]) {
    this._phoneNumbers = value;
    value[0].typeName = 'phoneNumbers'
    this._contactUpdateItem = {
      key: 'phoneNumbers',
      value: value[0]
    }
  }

  public get phoneNumbers(): PhoneNumber[] {
    return this._phoneNumbers;
  }

  public set imAddresses(value: ImAddress[]) {
    this._imAddresses = value;
    value[0].typeName = 'imAddresses';
    this._contactUpdateItem = {
      key: 'imAddresses',
      value: value[0]
    };
  }

  public get imAddresses(): ImAddress[] {
    return this._imAddresses;
  }

  public set fileAs(value: string) {
    this._fileAs = value;
    this._contactUpdateItem = {
      key: 'fileAs',
      value: value
    }
  }

  public get fileAs(): string {
    return this._fileAs;
  }

  public set fileAsMapping(value: FileAsMapping) {
    this._fileAsMapping = value;
    this._contactUpdateItem = {
      key: 'fileAsMapping',
      value: value
    }
  }

  public get fileAsMapping(): FileAsMapping {
    return this._fileAsMapping;
  }

  public set displayName(value: string) {
    this._displayName = value;
    this._contactUpdateItem = {
      key: 'displayName',
      value: value
    }
  }

  public get displayName(): string {
    return this._displayName;
  }

  public set displayNamePrefix(value: string) {
    this._displayNamePrefix = value;
    this._contactUpdateItem = {
      key: 'displayNamePrefix',
      value: value
    }
  }

  public get displayNamePrefix(): string {
    return this._displayNamePrefix;
  }

  public set givenName(value: string) {
    this._givenName = value;
    this._contactUpdateItem = {
      key: 'givenName',
      value: value
    }
  }

  public get givenName(): string {
    return this._givenName;
  }

  public set initials(value: string) {
    this._initials = value;
    this._contactUpdateItem = {
      key: 'initials',
      value: value
    }
  }

  public get initials(): string {
    return this._initials;
  }

  public set middleName(value: string) {
    this._middleName = value;
    this._contactUpdateItem = {
      key: 'middleName',
      value: value
    }
  }

  public get middleName(): string {
    return this._middleName;
  }

  public set nickname(value: string) {
    this._nickname = value;
    this._contactUpdateItem = {
      key: 'nickname',
      value: value
    }
  }

  public get nickname(): string {
    return this._nickname;
  }

  public set yomiGivenName(value: string) {
    this._yomiGivenName = value;
    this._contactUpdateItem = {
      key: 'yomiGivenName',
      value: value
    }
  }

  public get yomiGivenName(): string {
    return this._yomiGivenName;
  }

  public set yomiSurname(value: string) {
    this._yomiSurname = value;
    this._contactUpdateItem = {
      key: 'yomiSurname',
      value: value
    }
  }

  public get yomiSurname(): string {
    return this._yomiSurname;
  }

  public set yomiCompanyName(value: string) {
    this._yomiCompanyName = value;
    this._contactUpdateItem = {
      key: 'yomiCompanyName',
      value: value
    }
  }

  public get yomiCompanyName(): string {
    return this._yomiCompanyName;
  }

  public set personalHomePage(value: string) {
    this._personalHomePage = value;
    this._contactUpdateItem = {
      key: 'personalHomePage',
      value: value
    }
  }

  public get personalHomePage(): string {
    return this._personalHomePage;
  }

  public set companyName(value: string) {
    this._companyName = value;
    this._contactUpdateItem = {
      key: 'companyName',
      value: value
    }
  }

  public get companyName(): string {
    return this._companyName;
  }

  public set assistantName(value: string) {
    this._assistantName = value;
    this._contactUpdateItem = {
      key: 'assistantName',
      value: value
    }
  }

  public get assistantName(): string {
    return this._assistantName;
  }

  public set birthday(value: string) {
    this._birthday = value;
    this._contactUpdateItem = {
      key: 'birthday',
      value: value
    }
  }

  public get birthday(): string {
    return this._birthday;
  }

  public set businessHomePage(value: string) {
    this._businessHomePage = value;
    this._contactUpdateItem = {
      key: 'businessHomePage',
      value: value
    }
  }

  public get businessHomePage(): string {
    return this._businessHomePage;
  }

  public set children(value: string) {
    this._children = value;
    this._contactUpdateItem = {
      key: 'children',
      value: value
    }
  }

  public get children(): string {
    return this._children;
  }

  public set department(value: string) {
    this._department = value;
    this._contactUpdateItem = {
      key: 'department',
      value: value
    }
  }

  public get department(): string {
    return this._department;
  }

  public set generation(value: string) {
    this._generation = value;
    this._contactUpdateItem = {
      key: 'generation',
      value: value
    }
  }

  public get generation(): string {
    return this._generation;
  }

  public set jobTitle(value: string) {
    this._jobTitle = value;
    this._contactUpdateItem = {
      key: 'jobTitle',
      value: value
    }
  }

  public get jobTitle(): string {
    return this._jobTitle;
  }

  public set manager(value: string) {
    this._manager = value;
    this._contactUpdateItem = {
      key: 'manager',
      value: value
    }
  }

  public get manager(): string {
    return this._manager;
  }

  public set mileage(value: string) {
    this._mileage = value;
    this._contactUpdateItem = {
      key: 'mileage',
      value: value
    }
  }

  public get mileage(): string {
    return this._mileage;
  }

  public set officeLocation(value: string) {
    this._officeLocation = value;
    this._contactUpdateItem = {
      key: 'officeLocation',
      value: value
    }
  }

  public get officeLocation(): string {
    return this._officeLocation;
  }

  public set postalAddressIndex(value: string) {
    this._postalAddressIndex = value;
    this._contactUpdateItem = {
      key: 'postalAddressIndex',
      value: value
    }
  }

  public get postalAddressIndex(): string {
    return this._postalAddressIndex;
  }

  public set profession(value: string) {
    this._profession = value;
    this._contactUpdateItem = {
      key: 'profession',
      value: value
    }
  }

  public get profession(): string {
    return this._profession;
  }

  public set spouseName(value: string) {
    this._spouseName = value;
    this._contactUpdateItem = {
      key: 'spouseName',
      value: value
    }
  }

  public get spouseName(): string {
    return this._spouseName;
  }

  public set surname(value: string) {
    this._surname = value;
    this._contactUpdateItem = {
      key: 'surname',
      value: value
    }
  }

  public get surname(): string {
    return this._surname;
  }

  public set weddingAnniversary(value: string) {
    this._weddingAnniversary = value;
    this._contactUpdateItem = {
      key: 'weddingAnniversary',
      value: value
    }
  }

  public get weddingAnniversary(): string {
    return this._weddingAnniversary;
  }

  public set contactUpdateItem(value: ContactUpdateItem) {
    this._contactUpdateItem = value;
  }

  public get contactUpdateItem(): ContactUpdateItem {
    return this._contactUpdateItem;
  }

  private capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  /**
   * 新增联系人writeItemsChildren API调用该接口进行单个Contact实例新增
   * 修改联系人与新增附件调用该接口时，不传入第二个param
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   * @param flag 以此参数判断新增或修改/附件
   */
  writeToXml(soapXmlWriter: SoapXmlWriter, flag?: string): void {
    if (flag) {
      this.buildCreateContactXML(soapXmlWriter)
    } else {
      this.buildUpdateContactXML(soapXmlWriter)
    }
  }

  /**
   * 创建联系人XML构建器
   * 字符串判断条件下划线为当前类Private属性
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   * @param contact 新增与更新联系人请求数据类，以此参数判断新增或修改/附件
   */
  private buildCreateContactXML(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONTACT);
    for (let elementName in this) {
      switch (elementName) {
        case '_imAddresses':
          this.buildCreateImAddressXML(soapXmlWriter, elementName);
          break;
        case '_emailAddresses':
          this.buildCreateEmailAddressesXML(soapXmlWriter, elementName);
          break;
        case '_physicalAddresses':
          this.buildCreatePhysicalAddressesXML(soapXmlWriter, elementName);
          break;
        case '_phoneNumbers':
          this.buildCreatePhoneNumbersXML(soapXmlWriter, elementName);
          break;
        default:
        /**
         * 常规标签构建依赖于本类静态属性
         */
          this.buildCreateDefaultXML(soapXmlWriter, elementName);
          break;
      }
    }
    soapXmlWriter.writeEndElement();
  }

  /**
   * 更新联系人XML构建器
   * 由于更新联系人XML构建器参数为开发者传入，更新联系人判断条件无下划线
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  private buildUpdateContactXML(soapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONTACT);
    switch (this.contactUpdateItem.key) {
      case 'emailAddresses':
        this.buildUpdateEmailAddressXML(soapXmlWriter);
        break;
      case 'physicalAddresses':
        this.buildUpdatePhysicalAddressesXML(soapXmlWriter);
        break;
      case 'phoneNumbers':
        this.buildUpdatePhoneNumbersXML(soapXmlWriter);
        break;
      case 'imAddresses':
        this.buildUpdateImAddressesXML(soapXmlWriter);
        break;
      default:
      /**
       * 常规标签构建依赖于本类静态属性
       */
        this.buildUpdateDefaultXML(soapXmlWriter);
        break;
    }
    soapXmlWriter.writeEndElement()
  }

  /**
   * 修改联系人物理地址报文构建器
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  private buildUpdatePhysicalAddressesXML(soapXmlWriter): void {
    if (this.contactUpdateItem) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_PHYSICAL_ADDRESSES)
        .writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
        .writeAttribute(XmlAttribute.NAME_KEY, (this.contactUpdateItem.value as PhysicalAddress).key);
      for (let phyAdd in (this.contactUpdateItem.value as PhysicalAddress).elementContent) {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, this.capitalizeFirstLetter(phyAdd))
          .writeElementValue((this.contactUpdateItem.value as PhysicalAddress).elementContent[phyAdd])
          .writeEndElement();
      }
      soapXmlWriter.writeEndElement()
        .writeEndElement();
    }
  }

  /**
   * 修改联系人常规属性构建器
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  private buildUpdateDefaultXML(soapXmlWriter): void {
    if (this.contactUpdateItem) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, this.capitalizeFirstLetter(this.contactUpdateItem.key))
        .writeElementValue(this.contactUpdateItem.value)
        .writeEndElement();
    }
  }

  /**
   * 修改联系人网络通讯联系方式报文构建器
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  private buildUpdateImAddressesXML(soapXmlWriter): void {
    if (this.contactUpdateItem) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IM_ADDRESSES)
        .writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
        .writeAttribute(XmlAttribute.NAME_KEY, (this.contactUpdateItem.value as ImAddress).key)
        .writeElementValue((this.contactUpdateItem.value as ImAddress).elementContent)
        .writeEndElement()
        .writeEndElement();
    }
  }

  /**
   * 修改联系人联系电话报文构建器
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  private buildUpdatePhoneNumbersXML(soapXmlWriter): void {
    if (this.contactUpdateItem) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_PHONE_NUMBER)
        .writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
        .writeAttribute(XmlAttribute.NAME_KEY, (this.contactUpdateItem.value as PhoneNumber).key)
        .writeElementValue((this.contactUpdateItem.value as PhoneNumber).elementContent)
        .writeEndElement()
        .writeEndElement();
    }
  }

  /**
   * 修改联系人邮箱报文构建器
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  private buildUpdateEmailAddressXML(soapXmlWriter): void {
    if (this.contactUpdateItem) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EMAIL_ADDRESSES);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
        .writeAttribute(XmlAttribute.NAME_KEY, (this.contactUpdateItem.value as EmailAddress).key);
      if ((this.contactUpdateItem.value as EmailAddress).name) {
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, (this.contactUpdateItem.value as EmailAddress).name);
      }
      if ((this.contactUpdateItem.value as EmailAddress).routingType) {
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, (this.contactUpdateItem.value as EmailAddress).routingType);
      }
      if ((this.contactUpdateItem.value as EmailAddress).mailboxType) {
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, (this.contactUpdateItem.value as EmailAddress).mailboxType);
      }
      soapXmlWriter.writeElementValue((this.contactUpdateItem.value as EmailAddress).elementContent)
        .writeEndElement()
        .writeEndElement();
    }
  }

  /**
   * 新增联系人网络通讯地址报文构建器
   *
   * @param contact 联系人实例化对象
   * @param soapXmlWriter XML构建器实例化对象
   * @param elementName 联系人键名
   */
  private buildCreateImAddressXML(soapXmlWriter: SoapXmlWriter, elementName: string): void {
    if (this[elementName]) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IM_ADDRESSES);
      this[elementName].forEach(index => {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, index.key)
        soapXmlWriter.writeElementValue(index.elementContent)
        soapXmlWriter.writeEndElement();
      })
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 新增联系人邮箱地址报文构建器
   *
   * @param contact 联系人实例化对象
   * @param soapXmlWriter XML构建器实例化对象
   * @param elementName 联系人键名
   */
  private buildCreateEmailAddressesXML(soapXmlWriter: SoapXmlWriter, elementName: string): void {
    if (this[elementName]) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EMAIL_ADDRESSES);
      this[elementName].forEach(index => {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
          .writeAttribute(XmlAttribute.NAME_KEY, index.key);
        if (index.name) {
          soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, index.name)
        }
        if (index.routingType) {
          soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, index.routingType)
        }
        if (index.mailboxType) {
          soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, index.mailboxType)
        }
        soapXmlWriter.writeElementValue(index.elementContent)
          .writeEndElement();
      })
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 新增联系人物理地址报文构建器
   *
   * @param contact 联系人实例化对象
   * @param soapXmlWriter XML构建器实例化对象
   * @param elementName 联系人键名
   */
  private buildCreatePhysicalAddressesXML(soapXmlWriter: SoapXmlWriter, elementName: string): void {
    if (this[elementName]) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_PHYSICAL_ADDRESSES);
      this[elementName].forEach(index => {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_KEY, index.key)
        for (let address in index.elementContent) {
          soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE,
            this.capitalizeFirstLetter(address),
            index.elementContent[address]
          )
        }
        soapXmlWriter.writeEndElement();
      })
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 新增联系人电话号码报文构建器
   *
   * @param contact 联系人实例化对象
   * @param soapXmlWriter XML构建器实例化对象
   * @param elementName 联系人键名
   */
  private buildCreatePhoneNumbersXML(soapXmlWriter: SoapXmlWriter, elementName: string): void {
    if (this[elementName]) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_PHONE_NUMBERS);
      this[elementName].forEach(index => {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ENTRY)
          .writeAttribute(XmlAttribute.NAME_KEY, index.key)
          .writeElementValue(index.elementContent)
          .writeEndElement();
      })
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 新增联系人常规信息报文构建器
   *
   * @param contact 联系人实例化对象
   * @param soapXmlWriter XML构建器实例化对象
   * @param elementName 联系人键名
   */
  private buildCreateDefaultXML(soapXmlWriter: SoapXmlWriter, elementName: string): void {
    if (this[elementName] &&
      elementName !== 'version' &&
      elementName !== '_contactUpdateItem'
    ) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE,
        this.capitalizeFirstLetter(elementName.slice(1)), this[elementName])
    }
  }
}


/**
 * 更新项数组类型
 *
 * @since 2024-03-24
 */
export interface ContactUpdateItem {
  key: keyof Contact
  value: EmailAddress | PhysicalAddress | PhoneNumber | ImAddress | string
}

/**
 * 联系人电子邮箱联合类型
 *
 * @since 2024-04-09
 */
export interface EmailAddress {
  typeName?: 'emailAddresses'
  key: 'EmailAddress1' | 'EmailAddress2' | 'EmailAddress3'
  name?: string
  routingType?: string
  mailboxType?: string
  elementContent: string
}

/**
 * 联系人物理地址联合类型
 *
 * @since 2024-04-09
 */
export interface PhysicalAddress {
  typeName?: 'physicalAddresses'
  key: 'Business' | 'Home' | 'Other'
  elementContent: PhysicalAddressElementContent
}

/**
 * 联系人联系电话联合类型
 *
 * @since 2024-04-09
 */
export interface PhoneNumber {
  typeName?: 'phoneNumbers'
  key: 'AssistantPhone'
    | 'BusinessFax'
    | 'BusinessPhone'
    | 'BusinessPhone2'
    | 'Callback'
    | 'CarPhone'
    | 'CompanyMainPhone'
    | 'HomeFax'
    | 'HomePhone'
    | 'HomePhone2'
    | 'Isdn'
    | 'MobilePhone'
    | 'OtherFax'
    | 'OtherTelephone'
    | 'Pager'
    | 'PrimaryPhone'
    | 'RadioPhone'
    | 'Telex'
    | 'TtyTddPhone'
    | 'Other'
  elementContent: string
}

/**
 * 联系人通讯地址联合类型
 *
 * @since 2024-04-09
 */
export interface ImAddress {
  typeName?: 'imAddresses'
  key: 'ImAddress1' | 'ImAddress2' | 'ImAddress3'
  elementContent: string
}