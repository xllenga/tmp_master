/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateItem } from '../create_item';
import { Message } from './item_message';

/**
 * 创建邮件模型类
 *
 * @since 2024-03-16
 */
export class CreateMessage extends CreateItem {
  private _message: Message;

  /**
   * 设置邮件
   *
   * @param message 邮件
   */
  public set message(message: Message) {
    this._message = message;
  }

  /**
   * 获取邮件
   *
   * @returns 邮件
   */
  public get message(): Message {
    return this._message;
  }
}
