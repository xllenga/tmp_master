/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase } from '../mode_base';
import { SoapXmlWriter, XmlElement, XmlNamespace, XmlWritable } from '../../xml';
import { JsObjectElement } from '../../xml/soap_xml';
import { Message } from './item_message';

/**
 * 进度变化回调
 */
export type OnProgress = (progress: string) => void;

/**
 * 附件基本模型类
 *
 * @since 2024-03-28
 */
export class BaseAttachments implements XmlWritable {
  private _attachmentId: string; // 附件ID
  private _name: string; // 附件名称
  private _contentType: string; // 附件类型
  private _contentId: string; // 表示附件内容的标识符
  private _contentLocation: string; // 包含与附件内容的位置相对应的统一资源标识符
  private _lastModifiedTime: string; // 表示文件附件的最后修改时间
  private _isInline: boolean; // 表示附件是否内联显示在项目中
  private _size: number; // 附件大小
  private _isContactPhoto: boolean; // 指示文件附件是否是联系人图片

  /**
   * 设置附件ID
   *
   * @param attachmentId 附件ID
   */
  public set attachmentId(value: string) {
    this._attachmentId = value;
  }

  /**
   * 获取附件ID
   *
   * @returns 获取附件 attachmentId
   */
  public get attachmentId(): string {
    return this._attachmentId;
  }

  /**
   * 设置附件名称
   *
   * @param name 附件名称
   */
  public set name(value: string) {
    this._name = value;
  }

  /**
   * 获取附件名称
   *
   * @returns 获取附件名称 name
   */
  public get name(): string {
    return this._name;
  }

  /**
   * 设置附件类型
   *
   * @param contentType 附件类型
   */
  public set contentType(value: string) {
    this._contentType = value;
  }

  /**
   * 获取附件类型
   *
   * @returns 获取附件类型 contentType
   */
  public get contentType(): string {
    return this._contentType;
  }

  /**
   * 设置附件内容的标识符
   *
   * @param contentId 附件内容的标识符
   */
  public set contentId(value: string) {
    this._contentId = value;
  }

  /**
   * 获取附件内容的标识符
   *
   * @returns 获取附件内容的标识符 contentId
   */
  public get contentId(): string {
    return this._contentId;
  }

  /**
   * 设置附件内容的位置相对应的统一资源标识符
   *
   * @param contentLocation 附件内容的位置相对应的统一资源标识符
   */
  public set contentLocation(value: string) {
    this._contentLocation = value;
  }

  /**
   * 获取附件内容的位置相对应的统一资源标识符
   *
   * @returns 获取附件内容的位置相对应的统一资源标识符 contentLocation
   */
  public get contentLocation(): string {
    return this._contentLocation;
  }

  /**
   * 设置附件大小
   *
   * @param size 附件大小
   */
  public set size(value: number) {
    this._size = value;
  }

  /**
   * 获取附件大小
   *
   * @returns 附件大小 size
   */
  public get size(): number {
    return this._size;
  }

  /**
   * 设置文件附件的最后修改时间
   *
   * @param lastModifiedTime 附件的最后修改时间
   */
  public set lastModifiedTime(value: string) {
    this._lastModifiedTime = value;
  }

  /**
   * 获取文件附件的最后修改时间
   *
   * @returns 文件附件的最后修改时间 lastModifiedTime
   */
  public get lastModifiedTime(): string {
    return this._lastModifiedTime;
  }

  /**
   * 设置附件是否内联显示
   *
   * @param isInline 附件是否内联显示
   */
  public set isInline(value: boolean) {
    this._isInline = value;
  }

  /**
   * 获取附件是否内联显示
   *
   * @returns 附件是否内联显示 isInline
   */
  public get isInline(): boolean {
    return this._isInline;
  }

  /**
   * 设置文件附件是否是联系人图片
   *
   * @param isInline 附件是否内联显示
   */
  public set isContactPhoto(value: boolean) {
    this._isContactPhoto = value;
  }

  /**
   * 获取文件附件是否是联系人图片
   *
   * @returns 附件是否内联显示 isInline
   */
  public get isContactPhoto(): boolean {
    return this._isContactPhoto;
  }

  /**
   * 根据当前属性写出对应xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_NAME, this.name);
  }

  /**
   * 根据当前属性写出对应xml
   *
   * @param soapXmlWriter xml写入器
   */
  public readFromXml(itemAttrs: JsObjectElement[]): void {
    itemAttrs?.forEach(item => {
      switch (item._name) {
        case XmlElement.NAME_ATTACHMENT_ID:
          this.attachmentId = item._attributes.Id;
          break;
        case XmlElement.NAME_NAME:
          this.name = item._elements[0]._text;
          break;
        case XmlElement.NAME_CONTENT_TYPE:
          this.contentType = item._elements[0]._text;
          break;
        case XmlElement.NAME_CONTENT_ID:
          this.contentId = item._elements[0]._text;
          break;
        case XmlElement.NAME_SIZE:
          this.size = Number(item._elements[0]._text);
          break;
        case XmlElement.NAME_LAST_MODIFIED_TIME:
          this.lastModifiedTime = item._elements[0]._text;
          break;
        case XmlElement.NAME_IS_INLINE:
          this.isInline = item._elements[0]._text === 'true' ? true: false;
          break;
        case XmlElement.NAME_IS_CONTACT_PHOTO:
          this.isContactPhoto = item._elements[0]._text === 'true' ? true: false
          break;
        default :
          break;
      }
    })
  }
}

/**
 * 文件附件类
 *
 * @since 2024-03-28
 */
export class FileAttachment extends BaseAttachments {
  private _content?: string;

  /**
   * 设置附件base64字符串
   *
   * @param content 附件base64字符串
   */
  public set content(value: string) {
    this._content = value;
  }

  /**
   * 获取文件附件Base64
   *
   * @returns 获取文件附件Base64 content
   */
  public get content(): string {
    return this._content;
  }

  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FILE_ATTACHMENT);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_CONTENT, this.content);
    soapXmlWriter.writeEndElement();
  }

  public readFromXml(itemAttrs: JsObjectElement[]): void {
    super.readFromXml(itemAttrs);
    itemAttrs.forEach(fileItem => {
      if (fileItem._name === XmlElement.NAME_CONTENT) {
        this._content = fileItem._elements[0]._text;
      }
    });
  }
}

/**
 * 项目附件类
 *
 * @since 2024-03-28
 */
export class ItemAttachment extends BaseAttachments {
  private _item?: XmlWritable;

  /**
   * 设置项目附件
   *
   * @param item 项目附件
   */
  public set item(value: XmlWritable) {
    this._item = value;
  }

  /**
   * 获取项目附件
   *
   * @returns 获取文件附件 item
   */
  public get item(): XmlWritable {
    return this._item;
  }

  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ITEM_ATTACHMENT);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_IS_INLINE, String(this.isInline));
    this._item.writeToXml(soapXmlWriter)
    soapXmlWriter.writeEndElement();
  }

  public readFromXml(itemAttrs: JsObjectElement[]): void {
    super.readFromXml(itemAttrs);
    itemAttrs?.forEach(item => {
      switch (item._name) {
        case XmlElement.NAME_MESSAGE:
          const message = new Message();
          message.readFromXml(item._elements);
          this.item = message;
          break;
        case XmlElement.NAME_ITEM:
          break;
        case XmlElement.NAME_CALENDAR_ITEM:
          break;
        case XmlElement.NAME_CONTACT:
          break;
        case XmlElement.NAME_TASK:
          break;
        case XmlElement.NAME_MEETING_MESSAGE:
          break;
        case XmlElement.NAME_MEETING_REQUEST:
          break;
        case XmlElement.NAME_MEETING_RESPONSE:
          break;
        case XmlElement.NAME_MEETING_CANCELLATION:
          break;
        default :
          break;
      }
    })
  }
}

/**
 * 附件类
 *
 * @since 2024-03-28
 */
export class Attachment implements XmlWritable {
  private _attachments: BaseAttachments[] = [];

  /**
   * 添加附件
   *
   * @param attachments 附件
   */
  public addAttachments(attachments: BaseAttachments): void {
    this._attachments.push(attachments);
  }

  /**
   * 获取项目
   *
   * @returns 获取附件 attachments
   */
  public get attachments(): BaseAttachments[] {
    return this._attachments;
  }

  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ATTACHMENTS);
    this._attachments.forEach((data) => {
      data.writeToXml(soapXmlWriter)
    });
    soapXmlWriter.writeEndElement();
  }

  public readFromXml(itemAttrs: JsObjectElement[]): void {
    this._attachments.forEach((data) => {
      data.readFromXml(itemAttrs)
    });
  }
}

/**
 * 创建附件类
 *
 * @since 2024-03-28
 */
export class CreateAttachment extends ModelBase {
  private _parentItemId: string;
  private _attachment: Attachment = new Attachment();

  /**
   * 设置需要添加附件的邮件ID
   *
   * @param parentItemId 邮件ID
   */
  public set parentItemId(value: string) {
    this._parentItemId = value;
  }

  /**
   * 获取邮件ID
   *
   * @returns 获取添加附件的邮件ID parentItemId
   */
  public get parentItemId(): string {
    return this._parentItemId;
  }

  /**
   * 添加附件
   *
   * @param attachment 附件
   */
  public addAttachments(attachment: BaseAttachments): void {
    this._attachment.addAttachments(attachment);
  }

  /**
   * 获取邮件ID
   *
   * @returns 获取添加附件的邮件ID parentItemId
   */
  public get attachment(): Attachment {
    return this._attachment;
  }
}

/**
 * 创建附件响应类
 *
 * @since 2024-03-28
 */
export class CreateAttachmentResp extends ModelBase {
  private _responseMessages: CreateAttachmentResponseMessage[];

  /**
   * 设置附件返回信息
   *
   * @param responseMessages 附件创建返回信息
   */
  public set responseMessages(value: CreateAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  /**
   * 获取附件返回信息
   *
   * @returns 获取附件创建返回信息 responseMessages
   */
  public get responseMessages(): CreateAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}

/**
 * 创建附件响应信息类
 *
 * @since 2024-03-28
 */
export class CreateAttachmentResponseMessage {
  private _responseCode: string;
  private _messageText: string;
  private _attachmentList: Array<FileAttachment | ItemAttachment>;

  /**
   * 设置附件返回Code
   *
   * @param responseCode 附件创建返回Code
   */
  public set responseCode(value: string) {
    this._responseCode = value;
  }

  /**
   * 获取附件返回Code
   *
   * @returns 获取附件创建返回Code responseCode
   */
  public get responseCode(): string {
    return this._responseCode;
  }

  /**
   * 设置附件返回Text 失败或者警告的时候的时候才会
   *
   * @param messageText 附件创建返回Text
   */
  public set messageText(value: string) {
    this._messageText = value;
  }

  /**
   * 获取附件返回Text 失败或者警告的时候的时候才会
   *
   * @returns 获取附件返回Text messageText
   */
  public get messageText(): string {
    return this._messageText;
  }

  /**
   * 设置附件返回列表
   *
   * @param attachmentList 附件返回列表
   */
  public set attachmentList(value: Array<FileAttachment | ItemAttachment>) {
    this._attachmentList = value;
  }

  /**
   * 获取附件返回列表
   *
   * @returns 附件返回列表 attachmentList
   */
  public get attachmentList(): Array<FileAttachment | ItemAttachment> {
    return this._attachmentList;
  }
}

/**
 * 删除附件类
 *
 * 2024-03-28
 */
export class DeleteAttachment extends ModelBase {
  private _attachmentIds: Array<string>;

  /**
   * 设置删除附件ID
   *
   * @param attachmentIds 需要删除附件ID
   */
  public set attachmentIds(value: Array<string>) {
    this._attachmentIds = value;
  }

  /**
   * 获取删除附件ID
   *
   * @returns 删除附件ID attachmentIds
   */
  public get attachmentIds(): Array<string> {
    return this._attachmentIds;
  }
}

/**
 * 删除附件响应类
 *
 * 2024-03-28
 */
export class DeleteAttachmentResp extends ModelBase {
  private _responseMessages: DeleteAttachmentResponseMessage[];

  /**
   * 设置附件返回信息
   *
   * @param responseMessages 附件删除返回信息
   */
  public set responseMessages(value: DeleteAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  /**
   * 获取附件返回信息
   *
   * @returns 获取附件删除返回信息 responseMessages
   */
  public get responseMessages(): DeleteAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}

/**
 * 删除附件响应信息类
 *
 * @since 2024-03-28
 */
export class DeleteAttachmentResponseMessage {
  private _responseCode: string;
  private _messageText: string;

  /**
   * 设置附件返回Code
   *
   * @param responseCode 附件删除返回Code
   */
  public set responseCode(value: string) {
    this._responseCode = value;
  }

  /**
   * 获取附件返回Code
   *
   * @returns 获取附件删除返回Code responseCode
   */
  public get responseCode(): string {
    return this._responseCode;
  }

  /**
   * 设置附件返回Text 失败或者警告的时候的时候才会
   *
   * @param messageText 附件返回Text
   */
  public set messageText(value: string) {
    this._messageText = value;
  }

  /**
   * 获取附件返回Text 失败或者警告的时候的时候才会
   *
   * @returns 获取附件返回Text messageText
   */
  public get messageText(): string {
    return this._messageText;
  }
}

/**
 * 获取附件类
 *
 * @since 2024-03-28
 */
export class GetAttachment extends ModelBase {
  private _attachmentIds: Array<string>;

  /**
   * 设置需要获取估计的ID
   *
   * @param attachmentIds 需要获取附件ID
   */
  public set attachmentIds(value: Array<string>) {
    this._attachmentIds = value;
  }

  /**
   * 获取需要获取附件的ID
   *
   * @returns 获取附件的ID attachmentIds
   */
  public get attachmentIds(): Array<string> {
    return this._attachmentIds;
  }
}

/**
 * 获取附件响应类
 *
 * 2024-03-28
 */
export class GetAttachmentResp extends ModelBase {
  private _responseMessages: GetAttachmentResponseMessage[];

  /**
   * 设置附件返回信息
   *
   * @param responseMessages 附件获取返回信息
   */
  public set responseMessages(value: GetAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  /**
   * 获取附件返回信息
   *
   * @returns 获取附件获取返回信息 responseMessages
   */
  public get responseMessages(): GetAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}

/**
 * 获取附件响应信息类
 *
 * @since 2024-03-28
 */
export class GetAttachmentResponseMessage {
  private _responseCode: string;
  private _messageText: string;
  private _attachmentList: Array<FileAttachment | ItemAttachment>;

  /**
   * 设置附件返回Code
   *
   * @param responseCode 附件删除返回Code
   */
  public set responseCode(value: string) {
    this._responseCode = value;
  }

  /**
   * 获取附件返回Code
   *
   * @returns 获取附件删除返回Code responseCode
   */
  public get responseCode(): string {
    return this._responseCode;
  }

  /**
   * 设置附件返回Text 失败或者警告的时候的时候才会
   *
   * @param messageText 附件返回Text
   */
  public set messageText(value: string) {
    this._messageText = value;
  }

  /**
   * 获取附件返回Text 失败或者警告的时候的时候才会
   *
   * @returns 获取附件返回Text messageText
   */
  public get messageText(): string {
    return this._messageText;
  }

  /**
   * 设置获取的附件列表
   *
   * @param attachmentList 设置获取的附件列表
   */
  public set attachmentList(value: Array<FileAttachment | ItemAttachment>) {
    this._attachmentList = value;
  }

  /**
   * 获取获取的附件列表
   *
   * @returns 获取的附件列表 attachmentList
   */
  public get attachmentList(): Array<FileAttachment | ItemAttachment> {
    return this._attachmentList;
  }
}

/**
 * 下载附件类
 *
 * @since 2024-03-28
 */
export class DownLoadAttachment extends ModelBase {
  private _path: string;
  private _attachmentSize: number;
  private _attachmentIds: Array<string>;
  private _onProgress: OnProgress;

  /**
   * 设置需要下载附件的大小
   *
   * @param attachmentSize 附件的大小
   */
  public set attachmentSize(value: number) {
    this._attachmentSize = value;
  }

  /**
   * 获取需要下载附件的大小
   *
   * @returns 获取需要下载附件的大小 attachmentSize
   */
  public get attachmentSize(): number {
    return this._attachmentSize;
  }

  /**
   * 设置附件下载路径
   *
   * @param path 附件下载路径
   */
  public set path(value: string) {
    this._path = value;
  }

  /**
   * 获取附件下载路径
   *
   * @returns 获取附件下载路径 path
   */
  public get path(): string {
    return this._path;
  }

  /**
   * 设置下载附件的ID
   *
   * @param attachmentIds 下载附件的ID
   */
  public set attachmentIds(value: Array<string>) {
    this._attachmentIds = value;
  }

  /**
   * 获取下载附件的ID
   *
   * @returns 获取下载附件的ID attachmentIds
   */
  public get attachmentIds(): Array<string> {
    return this._attachmentIds;
  }

  /**
   * 设置下载附件的进度
   *
   * @param onDataReceive 下载附件的进度
   */
  public set onDataReceive(value: OnProgress) {
    this._onProgress = value;
  }

  /**
   * 获取下载附件的进度
   *
   * @returns 获取下载附件的进度 onDataReceive
   */
  public get onDataReceive(): OnProgress {
    return this._onProgress;
  }
}

/**
 * 下载附件响应类
 *
 * @since 2024-03-28
 */
export class DownLoadAttachmentResp extends ModelBase {
  private _responseMessages: GetAttachmentResponseMessage[];

  /**
   * 设置附件获取返回信息
   *
   * @param responseMessages 附件获取返回信息
   */
  public set responseMessages(value: GetAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  /**
   * 获取附件返回信息
   *
   * @returns 获取附件获取返回信息 responseMessages
   */
  public get responseMessages(): GetAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}





