/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SoapXmlWriter, XmlAttribute, XmlElement, XmlNamespace, XmlWritable } from '../../xml';
import { ModelBase, UniteId } from '../mode_base';
import { Body, BodyType, Mailbox } from './item_message';

/**
 * 转发和回复的基类
 *
 * @since 2024-04-13
 */
export class ForwardReplyBase extends ModelBase implements XmlWritable {
  private _subject: string; // 主题
  private _body: Body; // 邮件正文内容
  private _toRecipients: Mailbox[]; // 收件人
  private _ccRecipients: Mailbox[]; // 抄送人
  private _bccRecipients: Mailbox[]; // 密送人
  private _isReadReceiptRequested: 'true' | 'false'; // 发件人是否请求已读回执
  private _isDeliveryReceiptRequested: 'true' | 'false'; // 发件人是否请求送达回执
  private _from: Mailbox; // 发送消息的地址
  private _referenceItemId: UniteId; // 响应对象引用的项
  private _newBodyContent: Body; // 邮件的新正文内容
  private _receivedBy: Mailbox; // 委托访问方案中的委托
  private _receivedRepresenting: Mailbox; // 委托访问方案中的主体

  /**
   * 获取主题
   *
   * @returns 主题
   */
  public get subject(): string | undefined {
    return this._subject;
  }

  /**
   * 设置主题
   *
   * @param value 主题
   */
  public set subject(value: string) {
    this._subject = value;
  }

  /**
   * 获取邮件正文内容
   *
   * @returns 邮件正文内容
   */
  public get body(): Body | undefined {
    return this._body;
  }

  /**
   * 设置邮件正文内容
   *
   * @param value 邮件正文内容
   */
  public set body(value: Body) {
    this._body = value;
  }

  /**
   * 获取收件人集合
   *
   * @returns 收件人集合
   */
  public get toRecipients(): Mailbox[] | undefined {
    return this._toRecipients;
  }

  /**
   * 设置收件人集合
   *
   * @param value 收件人集合
   */
  public set toRecipients(value: Mailbox[]) {
    this._toRecipients = value;
  }

  /**
   * 获取抄送人集合
   *
   * @returns 抄送人集合
   */
  public get ccRecipients(): Mailbox[] | undefined {
    return this._ccRecipients;
  }

  /**
   * 设置抄送人集合
   *
   * @param value 抄送人集合
   */
  public set ccRecipients(value: Mailbox[]) {
    this._ccRecipients = value;
  }

  /**
   * 获取密送人集合
   *
   * @returns 密送人集合
   */
  public get bccRecipients(): Mailbox[] | undefined {
    return this._bccRecipients;
  }

  /**
   * 设置密送人集合
   *
   * @param value 密送人集合
   */
  public set bccRecipients(value: Mailbox[]) {
    this._bccRecipients = value;
  }

  /**
   * 获取发件人是否请求已读回执
   *
   * @returns 发件人是否请求已读回执
   */
  public get isReadReceiptRequested(): 'true' | 'false' | undefined {
    return this._isReadReceiptRequested;
  }

  /**
   * 设置发件人是否请求已读回执
   *
   * @param value 发件人是否请求已读回执
   */
  public set isReadReceiptRequested(value: 'true' | 'false') {
    this._isReadReceiptRequested = value;
  }

  /**
   * 获取发件人是否请求送达回执
   *
   * @returns 发件人是否请求送达回执
   */
  public get isDeliveryReceiptRequested(): 'true' | 'false' | undefined {
    return this._isDeliveryReceiptRequested;
  }

  /**
   * 设置发件人是否请求送达回执
   *
   * @param value 发件人是否请求送达回执
   */
  public set isDeliveryReceiptRequested(value: 'true' | 'false') {
    this._isDeliveryReceiptRequested = value;
  }

  /**
   * 获取发送消息的地址
   *
   * @returns 发送消息的地址
   */
  public get from(): Mailbox | undefined {
    return this._from;
  }

  /**
   * 设置发送消息的地址
   *
   * @param value 发送消息的地址
   */
  public set from(value: Mailbox) {
    this._from = value;
  }

  /**
   * 获取响应对象引用的项
   *
   * @returns 响应对象引用的项
   */
  public get referenceItemId(): UniteId | undefined {
    return this._referenceItemId;
  }

  /**
   * 设置响应对象引用的项
   *
   * @param value 响应对象引用的项
   */
  public set referenceItemId(value: UniteId) {
    this._referenceItemId = value;
  }

  /**
   * 获取邮件的新正文内容
   *
   * @returns 邮件的新正文内容
   */
  public get newBodyContent(): Body | undefined {
    return this._newBodyContent;
  }

  /**
   * 设置邮件的新正文内容
   *
   * @param value 邮件的新正文内容
   */
  public set newBodyContent(value: Body) {
    this._newBodyContent = value;
  }

  /**
   * 获取委托访问方案中的委托
   *
   * @returns 委托访问方案中的委托
   */
  public get receivedBy(): Mailbox | undefined {
    return this._receivedBy;
  }

  /**
   * 设置委托访问方案中的委托
   *
   * @param value 委托访问方案中的委托
   */
  public set receivedBy(value: Mailbox) {
    this._receivedBy = value;
  }

  /**
   * 获取委托访问方案中的主体
   *
   * @returns 委托访问方案中的主体
   */
  public get receivedRepresenting(): Mailbox | undefined {
    return this._receivedRepresenting;
  }

  /**
   * 设置委托访问方案中的主体
   *
   * @param value 委托访问方案中的主体
   */
  public set receivedRepresenting(value: Mailbox) {
    this._receivedRepresenting = value;
  }

  /**
   * 写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    if (this.subject) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_SUBJECT, this.subject);
    }
    if (this.body) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_BODY)
        .writeAttribute(XmlAttribute.NAME_BODY_TYPE, BodyType[this.body.bodyType])
        .writeElementValue(this.body.value)
        .writeEndElement();
    }
    if (this.toRecipients?.length) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TO_RECIPIENTS);
      this.toRecipients.forEach(item => {
        item.writeToXml(soapXmlWriter);
      })
      soapXmlWriter.writeEndElement();
    }
    if (this.ccRecipients?.length) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CC_RECIPIENTS);
      this.ccRecipients.forEach(item => {
        item.writeToXml(soapXmlWriter);
      })
      soapXmlWriter.writeEndElement();
    }
    if (this.bccRecipients?.length) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_BCC_RECIPIENTS);
      this.bccRecipients.forEach(item => {
        item.writeToXml(soapXmlWriter);
      })
      soapXmlWriter.writeEndElement();
    }
    if (this.isReadReceiptRequested) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_IS_READ_RECEIPT_REQUESTED,
        this.isReadReceiptRequested);
    }
    if (this.isDeliveryReceiptRequested) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_IS_DELIVERY_RECEIPT_REQUESTED,
        this.isDeliveryReceiptRequested);
    }
    if (this.from) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FROM);
      this.from.writeToXml(soapXmlWriter);
      soapXmlWriter.writeEndElement();
    }
    if (this.referenceItemId) {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_REFERENCE_ITEM_ID, new Map([
        [XmlAttribute.NAME_ID, this.referenceItemId.id],
        [XmlAttribute.NAME_CHANGE_KEY, this.referenceItemId.changeKey],
      ]));
    }
    if (this.newBodyContent) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_NEW_BODY_CONTENT)
        .writeAttribute(XmlAttribute.NAME_BODY_TYPE, BodyType[this.newBodyContent.bodyType])
        .writeElementValue(this.newBodyContent.value)
        .writeEndElement();
    }
    if (this.receivedBy) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_RECEIVED_BY);
      this.receivedBy.writeToXml(soapXmlWriter);
      soapXmlWriter.writeEndElement();
    }
    if (this.receivedRepresenting) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_RECEIVED_REPRESENTING);
      this.receivedRepresenting.writeToXml(soapXmlWriter);
      soapXmlWriter.writeEndElement();
    }
  }
}

/**
 * 转发的请求
 *
 * @since 2024-04-13
 */
export class ForwardItem extends ForwardReplyBase implements XmlWritable {
  /**
   * 写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FORWARD_ITEM);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 回复的请求
 *
 * @since 2024-04-13
 */
export class ReplyAllToItem extends ForwardReplyBase implements XmlWritable {
  /**
   * 写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_REPLY_ALL_TO_ITEM);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}