/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase, UniteId } from '../mode_base';
import {
  FolderName,
  SoapXmlWriter,
  XmlAttribute,
  XmlElement,
  XmlNamespace,
  XmlWritable,
} from '../../xml';

/**
 * 发送草稿模型类
 *
 * @since 2024-03-16
 */
export class SendItem extends ModelBase implements XmlWritable {
  private _items: UniteId[] = []; // 要发送的邮件信息
  private _distinguishedFolderId?: FolderName; // 系统目录ID
  private _folderId?: string; // 用户创建的目录ID

  /**
   * 获取系统目录ID
   *
   * @returns 系统目录ID
   */
  public get distinguishedFolderId(): FolderName | undefined {
    return this._distinguishedFolderId;
  }

  /**
   * 设置系统目录ID
   *
   * @param value 系统目录ID
   */
  public set distinguishedFolderId(value: FolderName) {
    this._distinguishedFolderId = value;
  }

  /**
   * 获取用户创建的目录ID
   *
   * @returns 用户创建的目录ID
   */
  public get folderId(): string | undefined {
    return this._folderId;
  }

  /**
   * 设置用户创建的目录ID
   *
   * @param value 用户创建的目录ID
   */
  public set folderId(value: string) {
    this._folderId = value;
  }

  /**
   * 获取要发送的邮件信息
   *
   * @returns 要发送的邮件信息
   */
  public get items(): UniteId[] {
    return this._items;
  }

  /**
   * 设置要发送的邮件信息
   * @param value 要发送的邮件信息
   */
  public set items(value: UniteId[]) {
    this._items = value;
  }

  /**
   * 写入自身xml节点
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter) {
    soapXmlWriter
      .writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SEND_ITEM)
      .writeAttribute(XmlAttribute.NAME_SAVE_ITEM_TO_FOLDER, String(Boolean(this.distinguishedFolderId)));

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_IDS);
    for (const element of this.items) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ITEM_ID)
        .writeAttribute(XmlAttribute.NAME_ID, element.id)
        .writeAttribute(XmlAttribute.NAME_CHANGE_KEY, element.changeKey)
        .writeEndElement();
    }
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SAVED_ITEM_FOLDER_ID);
    if (this.distinguishedFolderId) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID)
        .writeAttribute(XmlAttribute.NAME_ID, FolderName[this.distinguishedFolderId])
        .writeEndElement();
    } else if (this.folderId) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID)
        .writeAttribute(XmlAttribute.NAME_ID, this.folderId)
        .writeEndElement();
    }
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
  }
}
