/*
 * Copyright © 2023-2024 Coremail论客.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { MessageDisposition, SendMeetingInvitations } from '../utils/common_enum';
import { XmlWritable } from '../xml';
import { FolderId } from './folder/folder';
import { ModelBase, UniteId } from './mode_base';
import { ExtendedFieldURI } from './extended_property';

export class UpdateItem extends ModelBase {
  private _itemChanges: ChangeItem[]; // 更新项
  private _savedItemFolderId?: FolderId; // 更新、发送和创建项目的操作的目标文件夹。

  // 标识更新期间要尝试的冲突解决类型。 默认值为 AutoResolve
  private _conflictResolution?: ConflictResolution = ConflictResolution.AutoResolve;

  // 描述更新后如何处理项。 MessageDisposition 属性对于消息项是必需的，包括会议消息，例如会议取消、会议请求和会议响应
  private _messageDisposition?: MessageDisposition;

  // 描述更新日历项后如何传达会议更新,日历项和日历项匹配项需要此属性
  private _sendMeetingInvitationsOrCancellations?: SendMeetingInvitations;

  // 指示是否应取消已更新项目的已读回执
  private _suppressReadReceipts?: 'true' | 'false';

  /**
   * 设置更新项
   *
   * @param itemChange 更新项集合
   */
  public set itemChanges(itemChange: ChangeItem[]) {
    this._itemChanges = [...itemChange];
  }

  /**
   * 获取更新项
   *
   * @returns 更新项集合
   */
  public get itemChanges(): ChangeItem[] {
    return this._itemChanges;
  }

  /**
   * 设置冲突处理方式
   *
   * @param conflictResolution
   */
  public set conflictResolution(conflictResolution: ConflictResolution) {
    this._conflictResolution = conflictResolution;
  }

  /**
   * 获取冲突处理方式
   *
   * @returns 冲突解决类型
   */
  public get conflictResolution(): ConflictResolution {
    return this._conflictResolution;
  }

  /**
   * 设置如何处理更新项
   *
   * @param messageDisposition 更新项处理方式
   */
  public set messageDisposition(messageDisposition: MessageDisposition) {
    this._messageDisposition = messageDisposition;
  }

  /**
   * 获取更新后如何处理项
   *
   * @returns 更新后处理项的方式
   */
  public get messageDisposition(): MessageDisposition {
    return this._messageDisposition;
  }

  /**
   * 设置传达会议更新的方式
   *
   * @param sendMeetingInvitationsOrCancellations 传达会议更新的方式
   */
  public set sendMeetingInvitationsOrCancellations(sendMeetingInvitationsOrCancellations: SendMeetingInvitations) {
    this._sendMeetingInvitationsOrCancellations = sendMeetingInvitationsOrCancellations;
  }

  /**
   * 获取传达会议更新的方式
   *
   * @returns sendMeetingInvitationsOrCancellations 传达会议更新的方式
   */
  public get sendMeetingInvitationsOrCancellations(): SendMeetingInvitations {
    return this._sendMeetingInvitationsOrCancellations;
  }

  /**
   * 设置是否取消已读回执
   *
   * @param suppressReadReceipts 是否取消已读回执
   */
  public set suppressReadReceipts(suppressReadReceipts: 'true' | 'false') {
    this._suppressReadReceipts = suppressReadReceipts;
  }

  /**
   * 获取是否取消已读回执
   *
   * @returns 是否取消已读回执
   */
  public get suppressReadReceipts(): 'true' | 'false' {
    return this._suppressReadReceipts;
  }

  /**
   * 设置更新、发送和创建项目的操作的目标文件夹
   * @param savedItemFolderId
   */
  public set savedItemFolderId(savedItemFolderId: FolderId) {
    this._savedItemFolderId = savedItemFolderId;
  }

  public get savedItemFolderId(): FolderId {
    return this._savedItemFolderId;
  }
}

/**
 * UpdateItem方法响应
 *
 * @since 2024-03-28
 */
export class UpdateItemResp extends ModelBase {
  private _responses: UpdateResponse[]; // 响应结果

  /**
   * 获取更新响应
   *
   * @returns 更新响应
   */
  public get response(): UpdateResponse[] {
    return this._responses;
  }

  /**
   * 设置更新响应结果
   *
   * @param responses
   */
  public set response(responses: UpdateResponse[]) {
    this._responses = responses;
  }
}

/**
 * 同步方法的响应结果类型
 */
export class UpdateResponse {
  private _code: string; // 状态码
  private _items: ChangeItem[]; // 更新后的返回信息
  private _messageText: string; // 响应结果文本
  private _responseClass: string; // 响应类

  /**
   * 获取响应状态码
   *
   * @returns 响应状态码
   */
  public get code(): string {
    return this._code;
  }

  /**
   * 获设置响应状态码
   *
   * @param code 响应状态码
   */
  public set code(code: string) {
    this._code = code;
  }

  /**
   * 获取更新结果
   *
   * @returns 更新结果集合
   */
  public get items(): ChangeItem[] {
    return this._items;
  }

  /**
   * 设置更新结果集合
   *
   * @param items 更新结果集合
   */
  public set items(items: ChangeItem[]) {
    this._items = items;
  }

  /**
   * 获取响应文本
   *
   * @returns 响应文本
   */
  public get messageText(): string {
    return this._messageText;
  }

  /**
   * 设置响应文本
   *
   * @param messageText 响应文本
   */
  public set messageText(messageText: string) {
    this._messageText = messageText;
  }

  /**
   * 获取响应类
   *
   * @returns 响应类型
   */
  public get responseClass(): string {
    return this._responseClass;
  }

  /**
   * 设置响应类
   *
   * @param responseClass 响应类型
   */
  public set responseClass(responseClass: string) {
    this._responseClass = responseClass;
  }
}

/**
 * 需要更新的Item,一个Item里可以包含多个更新操作，对应一个ItemField
 */
export class ChangeItem {
  private _itemId: UniteId;
  private _occurrenceItemId: OccurrenceItemId;
  private _recurringMasterItemId: RecurringMasterItemId;
  private _itemFields: ItemField[];

  /**
   * 设置Id,包含Id和ChangeKey
   *
   * @param itemId
   */
  public set itemId(itemId: UniteId) {
    this._itemId = itemId;
  }

  /**
   * 获取Id,包含Id和ChangeKey
   *
   * @returns itemId 项目Id
   */
  public get itemId(): UniteId {
    return this._itemId;
  }

  /**
   * 设置定期项目的单个匹配项ID
   *
   * @param occurrenceItemId 单个匹配项Id
   */
  public set occurrenceItemId(occurrenceItemId: OccurrenceItemId) {
    this._occurrenceItemId = occurrenceItemId;
  }

  /**
   * 获取定期项目的单个匹配项Id
   *
   * @returns occurrenceItemId 单个匹配项Id
   */
  public get occurrenceItemId(): OccurrenceItemId {
    return this._occurrenceItemId;
  }

  /**
   * 设置其相关事件项之一的标识符来标识定期主项目的标识
   *
   * @param occurrenceItemId
   */
  public set recurringMasterItemId(recurringMasterItemId: RecurringMasterItemId) {
    this._recurringMasterItemId = recurringMasterItemId;
  }

  /**
   * 获取其相关事件项之一的标识符来标识定期主项目的标识
   *
   * @returns occurrenceItemId 其相关事件项之一的标识符来标识定期主项目的标识
   */
  public get recurringMasterItemId(): RecurringMasterItemId {
    return this._recurringMasterItemId;
  }

  /**
   * 设置更新项的集合
   * 注：为每个ItemId对应的更新集合
   *
   * @param itemFields
   */
  public set itemFields(itemFields: ItemField[]) {
    this._itemFields = [...itemFields];
  }


  /**
   * 获取单个ItemId对应的一组更新
   *
   * @returns 单个ItemId对应的一组更新
   */
  public get itemFields(): ItemField[] {
    return this._itemFields;
  }
}

/**
 * 更新操作类型枚举类
 */
export enum ItemOperateType {
  AppendToItemField,
  SetItemField,
  DeleteItemField,
}

/**
 * 单个ItemId对应的更新元素类型
 */
export type ItemField = {
  operateType: ItemOperateType; // 操作类型，包含增删改三种
  fieldURI?: string;
  indexedFieldURI?: IndexedFieldURI;
  extendedFieldURI?: ExtendedFieldURI;
  item?: XmlWritable; // 邮件/联系人/...
}

/**
 * 标识字典的各个成员
 */
export interface IndexedFieldURI {
  fieldURI: string,
  fieldIndex: string,
}

/**
 * 更新期间的冲突解决类型
 */
export enum ConflictResolution {
  NeverOverwrite = 1,
  AutoResolve = 2,
  AlwaysOverwrite = 3,
}

/**
 * 描述更新日历项后如何传达会议更新
 */
export enum SendMeetingInvitationsOrCancellations {
  SendToNone = 1,
  SendOnlyToAll = 2,
  SendOnlyToChanged = 3,
  SendToAllAndSaveCopy = 4,
  SendToChangedAndSaveCopy = 5,
}


export interface OccurrenceItemId {
  recurringMasterId: string, // 标识定期项目的定期主控项
  instanceIndex: string, // 标识项目出现的索引
  changeKey?: string, // 标识定期主控项或项目事件的特定版本
}

export interface RecurringMasterItemId {
  occurrenceId: string, // 定期主控项的单个匹配项
  changeKey?: string, // 定期主项的单个匹配项的特定版本
}
