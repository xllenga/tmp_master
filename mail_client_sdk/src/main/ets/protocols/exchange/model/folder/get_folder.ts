/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ResponseMessage } from '../../response/response_base';
import { PropertySet } from '../../utils/common_enum';
import { ModelBase } from '../mode_base';
import { Folder, FolderId } from './folder';

/**
 * 获取文件夹model
 *
 * @since 2024-03-19
 */
export class GetFolder extends ModelBase {
  private _baseShape: PropertySet;
  private _folderIds: FolderId[];

  /**
   * 设置在项目或文件夹响应中需要返回的属性集类型
   *
   * @param value
   */
  public set baseShape(value: PropertySet) {
    this._baseShape = value;
  }

  /**
   * 获取在项目或文件夹响应中需要返回的属性集类型
   *
   * @returns 属性集类型
   */
  public get baseShape(): PropertySet {
    return this._baseShape;
  }

  /**
   * 设置获取文件夹Id集合
   *
   * @param value 文件夹Id集合
   */
  public set folderIds(value: FolderId[]) {
    this._folderIds = value;
  }

  /**
   * 获取文件夹Id集合
   *
   * @returns 文件夹Id集合
   */
  public get folderIds(): FolderId[] {
    return this._folderIds;
  }
}

/**
 * 获取文件夹响应model
 */
export class GetFolderResp extends ModelBase {
  private _responseMessages: ResponseMessage<Folder>[];

  /**
   * 设置响应数据
   *
   * @param value 响应数据
   */
  public set responseMessages(value: ResponseMessage<Folder>[]) {
    this._responseMessages = value;
  }

  /**
   * 获取响应数据
   *
   * @returns 响应数据
   */
  public get responseMessages(): ResponseMessage<Folder>[] {
    return this._responseMessages;
  }
}


