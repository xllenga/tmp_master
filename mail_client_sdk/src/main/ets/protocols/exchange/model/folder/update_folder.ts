/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ResponseMessage } from '../../response/response_base';
import { ModelBase } from '../mode_base';
import { Folder } from './folder';
import { FolderChange } from './folder_change';

/**
 * 更新文件夹model
 *
 * @since 2024-03-19
 */
export class UpdateFolder extends ModelBase {
  private _folderChanges: FolderChange[];

  /**
   * 设置文件夹更改信息集合
   *
   * @param folders 文件夹更改信息集合
   */
  public set folderChanges(folders: FolderChange[]) {
    this._folderChanges = folders;
  }

  /**
   * 获取文件夹更改信息集合
   *
   * @returns 文件夹更改信息集合
   */
  public get folderChanges(): FolderChange[] {
    return this._folderChanges;
  }
}

/**
 * 更新文件夹响应model
 *
 * @since 2024-03-19
 */
export class UpdateFolderResp extends ModelBase {
  private _responseMessages: ResponseMessage<Folder>[];

  /**
   * 设置响应数据
   *
   * @param value 响应数据
   */
  public set responseMessages(value: ResponseMessage<Folder>[]) {
    this._responseMessages = value;
  }

  /**
   * 获取响应数据
   *
   * @returns 响应数据
   */
  public get responseMessages(): ResponseMessage<Folder>[] {
    return this._responseMessages;
  }
}