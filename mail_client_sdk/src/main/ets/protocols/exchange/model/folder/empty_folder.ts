/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Folder, FolderId } from './folder';
import { ModelBase } from '../mode_base';
import { DeleteType } from '../../utils/common_enum';
import { ResponseMessage } from '../../response/response_base';

/**
 * 清空文件夹方法类
 *
 * @since 2024-03-28
 */
export class EmptyFolder extends ModelBase {
  private _deleteType: DeleteType;
  private _deleteSubFolders: 'true' | 'false';
  private _folderIds: FolderId[];

  /**
   * 获取清空文件夹的方式
   *
   * @returns 清空方式
   */
  public get deleteType(): DeleteType {
    return this._deleteType;
  }

  /**
   * 设置清空文件夹的方式
   *
   * @param deleteType 清空方式
   */
  public set deleteType(deleteType: DeleteType) {
    this._deleteType = deleteType;
  }

  /**
   * 获取是否要删除子文件夹
   *
   * @returns 是否要删除子文件夹
   */
  public get deleteSubFolders(): 'true' | 'false' {
    return this._deleteSubFolders;
  }

  /**
   * 设置是否要删除子文件夹
   *
   * @param deleteSubFolders 是否要删除子文件夹
   */
  public set deleteSubFolders(deleteSubFolders) {
    this._deleteSubFolders = deleteSubFolders;
  }

  /**
   * 设置清空文件夹Id集合
   *
   * @param value 文件夹Id集合
   */
  public set folderIds(value: FolderId[]) {
    this._folderIds = value;
  }

  /**
   * 获取文件夹Id集合
   *
   * @returns 文件夹Id集合
   */
  public get folderIds(): FolderId[] {
    return this._folderIds;
  }
}

/**
 * 清空文件夹方法响应结果
 *
 * @since 2024-03-28
 */
export class EmptyFolderResp extends ModelBase {
  private _responseMessages: ResponseMessage<Folder>[];

  /**
   * 设置响应数据
   *
   * @param value 响应数据
   */
  public set responseMessages(value: ResponseMessage<Folder>[]) {
    this._responseMessages = value;
  }

  /**
   * 获取响应数据
   *
   * @returns 响应数据
   */
  public get responseMessages(): ResponseMessage<Folder>[] {
    return this._responseMessages;
  }
}
