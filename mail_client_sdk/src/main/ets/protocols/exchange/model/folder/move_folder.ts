/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Folder, FolderId } from './folder';
import { ModelBase } from '../mode_base';
import { ResponseMessage } from '../../response/response_base';

/**
 * 移动文件夹方法类
 *
 * @since 2024-03-28
 */
export class MoveFolder extends ModelBase {
  private _toFolderId: FolderId; // 目标文件夹
  private _folderIds: FolderId[]; // 要操作文件夹数组

  /**
   * 获取目标文件夹
   *
   * @returns 目标文件夹Id
   */
  public get toFolderId(): FolderId {
    return this._toFolderId;
  }

  /**
   * 设置目标文件夹
   *
   * @param toFolderId 目标文件夹Id
   */
  public set toFolderId(toFolderId: FolderId) {
    this._toFolderId = toFolderId;
  }

  /**
   * 获取当前要移动的文件夹集合
   *
   * @returns 当前要移动的文件夹集合
   */
  public get folderIds(): FolderId[] {
    return this._folderIds;
  }

  /**
   * 设置当前要移动的文件夹集合
   *
   * @param folderIds 当前要移动的文件夹集合
   */
  public set folderIds(folderIds: FolderId[]) {
    this._folderIds = folderIds;
  }
}

/**
 * 移动文件夹响应结果
 *
 * @since 2024-03-28
 */
export class MoveFolderResp extends ModelBase {
  private _responseMessages: ResponseMessage<Folder>[];

  /**
   * 设置响应数据
   *
   * @param value 响应数据
   */
  public set responseMessages(value: ResponseMessage<Folder>[]) {
    this._responseMessages = value;
  }

  /**
   * 获取响应数据
   *
   * @returns 响应数据
   */
  public get responseMessages(): ResponseMessage<Folder>[] {
    return this._responseMessages;
  }
}
