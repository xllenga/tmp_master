/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Folder, FolderId } from './folder';
import { ModelBase } from '../mode_base';
import { ResponseMessage } from '../../response/response_base';

/**
 * 文件夹修改为已读方法类
 *
 * @since 2024-03-28
 */
export class MarkAllItemsAsRead extends ModelBase {
  private _readFlag: string; // 已读未读状态
  private _suppressReadReceipts: string; // 是否应取消已读回执
  private _folderIds: FolderId[]; // 文件夹Id

  /**
   * 获取读取状态
   *
   * @returns 已读未读状态
   */
  public get readFlag(): string {
    return this._readFlag;
  }

  /**
   * 设置获取读取状态
   *
   * @param readFlag 已读未读状态
   */
  public set readFlag(readFlag) {
    this._readFlag = readFlag;
  }

  /**
   * 获取元素是否应取消已读回执
   *
   * @returns 是否应取消已读回执
   */
  public get suppressReadReceipts(): string {
    return this._suppressReadReceipts
  }

  /**
   * 设置元素是否应取消已读回执
   *
   * @param suppressReadReceipts 是否应取消已读回执
   */
  public set suppressReadReceipts(suppressReadReceipts) {
    this._suppressReadReceipts = suppressReadReceipts;
  }

  /**
   * 设置已读文件夹Id集合
   *
   * @param value 文件夹Id集合
   */
  public set folderIds(value: FolderId[]) {
    this._folderIds = value;
  }

  /**
   * 获取文件夹Id集合
   *
   * @returns 文件夹Id集合
   */
  public get folderIds(): FolderId[] {
    return this._folderIds;
  }
}

/**
 * 文件夹修改为已读方法响应结果
 *
 * @since 2024-03-28
 */
export class MarkAllItemsAsReadResp extends ModelBase {
  private _responseMessages: ResponseMessage<Folder>[];

  /**
   * 设置响应数据
   *
   * @param value 响应数据
   */
  public set responseMessages(value: ResponseMessage<Folder>[]) {
    this._responseMessages = value;
  }

  /**
   * 获取响应数据
   *
   * @returns 响应数据
   */
  public get responseMessages(): ResponseMessage<Folder>[] {
    return this._responseMessages;
  }
}
