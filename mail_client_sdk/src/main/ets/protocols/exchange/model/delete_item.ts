/*
 * Copyright © 2023-2024 Coremail论客.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ResponseMessage } from '../response/response_base';
import { DeleteType } from '../utils/common_enum';
import { ModelBase, UniteId } from './mode_base';

/**
 * 删除邮件、联系人、日历等公共请求的model类
 *
 * @since 2024-04-12
 */
export class DeleteItem extends ModelBase {
  private _itemIds: UniteId[]; // 具体ItemId数组
  private _deleteType: DeleteType = DeleteType.SoftDelete; // 删除类型

  // 指示是否抑制已删除项目的已读回执。 文本值为 true，表示已读回执被抑制。 false 值表示 将已读回执发送给发件人。 此特性是可选的。
  private _suppressReadReceipts: boolean = false;

  // 描述是否向与会者传达日历项目删除。 删除日历项目时，此属性是必需的。 如果删除非日历项目，则此属性是可选的。
  private _sendMeetingCancellations?: SendCancellationsType = null;

  // 描述任务实例或任务母版是否由 DeleteItem 操作删除。 删除任务时，此属性是必需的。 当删除非任务项目时，此属性是可选的。
  private _affectedTaskOccurrence?: AffectedTaskOccurrence = null;

  /**
   * 设置删除类型
   *
   * @param value 删除类型
   */
  public set deleteType(value: DeleteType) {
    this._deleteType = value;
  }

  /**
   * 获取删除类型
   *
   * @returns DeleteType 删除类型
   */
  public get deleteType(): DeleteType {
    return this._deleteType;
  }

  /**
   * 设置指示是否抑制已删除项目的已读回执。 文本值为 true，表示已读回执被抑制。 false 值表示 将已读回执发送给发件人。 此特性是可选的。
   *
   * @param value 是否抑制已删除项目的已读回执
   */
  public set suppressReadReceipts(value: boolean) {
    this._suppressReadReceipts = value;
  }

  /**
   * 获取指示是否抑制已删除项目的已读回执。 文本值为 true，表示已读回执被抑制。 false 值表示 将已读回执发送给发件人。 此特性是可选的。
   *
   * @returns 是否抑制已删除项目的已读回执
   */
  public get suppressReadReceipts(): boolean {
    return this._suppressReadReceipts;
  }

  /**
   * 设置具体ItemId数组
   *
   * @param value 具体ItemId数组
   */
  public set itemIds(value: UniteId[]) {
    this._itemIds = value;
  }

  /**
   * 获取具体ItemId数组
   *
   * @returns UniteId[] 具体ItemId数组
   */
  public get itemIds(): UniteId[] {
    return this._itemIds;
  }

  /**
   * 设置描述是否向与会者传达日历项目删除。 删除日历项目时，此属性是必需的。 如果删除非日历项目，则此属性是可选的。
   *
   * @param value 是否向与会者传达日历项目
   */
  public set sendMeetingCancellations(value: SendCancellationsType) {
    this._sendMeetingCancellations = value;
  }

  /**
   * 获取是否向与会者传达日历项目删除。 删除日历项目时，此属性是必需的。 如果删除非日历项目，则此属性是可选的。
   *
   * @returns SendCancellationsType 是否向与会者传达日历项目
   */
  public get sendMeetingCancellations(): SendCancellationsType {
    return this._sendMeetingCancellations;
  }

  /**
   * 设置描述任务实例或任务母版是否由 DeleteItem 操作删除。 删除任务时，此属性是必需的。 当删除非任务项目时，此属性是可选的。
   *
   * @param value 是否由 DeleteItem 操作删除
   */
  public set affectedTaskOccurrence(value: AffectedTaskOccurrence) {
    this._affectedTaskOccurrence = value;
  }

  /**
   * 获取描述任务实例或任务母版是否由 DeleteItem 操作删除。 删除任务时，此属性是必需的。 当删除非任务项目时，此属性是可选的。
   *
   * @returns AffectedTaskOccurrence 是否由 DeleteItem 操作删除
   */
  public get affectedTaskOccurrence(): AffectedTaskOccurrence {
    return this._affectedTaskOccurrence;
  }
}

/**
 * 删除item返回结构
 *
 * @since 2024-04-15
 */
export class DeleteItemResp extends ModelBase {
  private _responseMessages: ResponseMessage[];  // item返回结构数组

  /**
   * 设置item返回结构数组
   *
   * @param value item返回结构数组
   */
  public set responseMessages(value: ResponseMessage[]) {
    this._responseMessages = value;
  }

  /**
   * 获取item返回结构数组
   *
   * @returns ResponseMessage[] item返回结构数组
   */
  public get responseMessages(): ResponseMessage[] {
    return this._responseMessages;
  }
}

/**
 * 描述任务实例或任务母版是否由 DeleteItem 操作删除。 删除任务时，此属性是必需的。 当删除非任务项目时，此属性是可选的。
 */
export enum SendCancellationsType {
  // 在不发送取消消息的情况下删除日历项目
  SendToNone = 'SendToNone',

  // 日历项目将被删除，并且会向所有与会者发送取消邮件
  SendOnlyToAll = 'SendOnlyToAll',

  // 日历项目将被删除，并且会向所有与会者发送取消邮件。 取消邮件的副本保存在"已发送项目"文件夹中
  SendToAllAndSaveCopy = 'SendToAllAndSaveCopy',
}

/**
 * 描述任务实例或任务母版是否由 DeleteItem 操作删除。 删除任务时，此属性是必需的。 当删除非任务项目时，此属性是可选的。
 */
export enum AffectedTaskOccurrence {
  // 删除项目请求将删除主任务，因此删除与主任务关联的所有定期任务
  AllOccurrences = 'AllOccurrences',

  // 删除项目请求仅删除任务的特定匹配项
  SpecifiedOccurrenceOnly = 'SpecifiedOccurrenceOnly',
}
