/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Verifiable } from '../request/request_base';
import { ExchangeVersion } from '../utils/common_enum';

/**
 * 数据模型基类
 * 数据模型主要用于存储xml报文数据，Request将这些数据组合成请求报文
 *
 * @since 2024-03-19
 */
export abstract class ModelBase implements Verifiable {
  private _version: ExchangeVersion = ExchangeVersion.Exchange2013_SP1;
  private _timeZoneDefinitionId?: string;

  /**
   * 设置xml协议版本号
   *
   * @param version 协议版本号
   */
  public set version(version: ExchangeVersion) {
    this._version = version;
  }

  /**
   * 获取xml协议版本号
   *
   * @returns 协议版本号
   */
  public get version(): ExchangeVersion {
    return this._version;
  }

  /**
   * 获取xml协议版本号
   *
   * @returns 协议版本号字串
   */
  public getVersionString(): string {
    return ExchangeVersion[this.version];
  }

  /**
   * 设置xml时区
   *
   * @param timeZoneDefinitionId 时区
   */
  public set timeZoneDefinitionId(timeZoneDefinitionId: string) {
    this._timeZoneDefinitionId = timeZoneDefinitionId;
  }

  /**
   * 获取xml时区
   *
   * @returns 时区
   */
  public get timeZoneDefinitionId(): string {
    return this._timeZoneDefinitionId;
  }

  public validate(): void {
  }
}

/**
 * 联合id
 * 用于标识item,folder的唯一性
 *
 * @since 2024-03-23
 */
export interface UniteId {
  /**
   * item,folder唯一标识
   */
  id: string;

  /**
   * 版本标识
   */
  changeKey?: string;
}