/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getLogger, Logger } from '../../../utils/log';
import { GetStreamingEventsResp } from '../model/streaming_subscription';
import { JsObjectElement, XmlElement } from '../xml';
import { ResponseBase } from './response_base';

const logger: Logger = getLogger('StreamingEventsResponse');

// 响应类型标识 eventResponse为事件的响应 connectionResponse连接请求响应
const EVENT_RESPONSE: string = 'eventResponse';
const CONNECTION_RESPONSE: string = 'connectionResponse';

/**
 * 流式处理通知响应器
 *
 * @since 2024-03-19
 */
export class GetStreamingEventsResponse extends ResponseBase<GetStreamingEventsResp> {
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    let responseMessage: JsObjectElement[] = this.getResponseMessage(bodyElement,
      XmlElement.NAME_GET_STREAMING_EVENTS_RESPONSE_MESSAGE);
    logger.info('GetStreamingEventsResp', responseMessage);
    const code: JsObjectElement | null = responseMessage.find(s => s._name === XmlElement.NAME_RESPONSE_CODE) ?? null;
    this.respCode = code?._elements?.[0]?._text;
    if (this.isSuccessful()) {
      const connectionInfo: JsObjectElement | null = responseMessage.find(c => c._name === XmlElement
        .NAME_CONNECTION_STATUS) ?? null;
      if (!connectionInfo) {
        this.data.responseType = EVENT_RESPONSE;
        let notificationsElement: JsObjectElement | null = responseMessage.find(c => c._name === XmlElement
          .NAME_NOTIFICATIONS) ?? null;
        if (notificationsElement?._elements && notificationsElement?._elements.length > 0) {
          this.data.readFromXml(notificationsElement?._elements);
        }
      } else {
        this.data.connectionStatus = connectionInfo._elements?.[0]?._text ?? '';
        this.data.responseType = CONNECTION_RESPONSE;
      }
    }
  }

  /**
   * 解析Xml字符串为json
   *
   * @param bodyElement xml字符串
   * @returns responseMessage 解析后的json
   */
  private getResponseMessage(bodyElement: JsObjectElement, messageName: string): JsObjectElement[] {
    let elementsName: string = bodyElement?._name;
    let jsonElement: JsObjectElement = bodyElement?._elements[0];
    let responseMessage: JsObjectElement[] = [];
    while (elementsName !== messageName) {
      jsonElement = jsonElement?._elements[0];
      elementsName = jsonElement?._elements?.[0]?._name;
    }
    responseMessage = jsonElement?._elements?.[0]?._elements;
    return responseMessage;
  }
}
