import { ModelBase } from '../model/mode_base';
import http from '@ohos.net.http';
import { CustomConvertXml, XmlQuery, XmlTag } from '../xml/xmlElement';
import { ErrorMsgInfo } from './response_base';


export abstract class ResponseBase2<T extends ModelBase | ModelBase[]> {
  protected data?: T;
  protected error?: Error;
  protected errorList?: ErrorMsgInfo[]

  public constructor(protected response: http.HttpResponse) {
  }

  public getData(): T | null {
    return this.data ?? null;
  }

  public getError() {
    return this.error ?? null;
  }


  abstract parseXml(): void;

  public validateResponse(q: XmlQuery<XmlTag>,
                          successCallback: (q: XmlQuery<XmlTag>) => void,
                          errorCallBack?: (q: XmlQuery<XmlTag>) => void) {
    const messageTag = q.queryFirst('ResponseMessages');
    for (let responseTag of messageTag.children) {
      const responseClass = responseTag.getAttribute('ResponseClass');
      const query = responseTag.initQuery();
      if (responseClass === 'Success') {
        successCallback(query);
      } else {
        if (errorCallBack) {
          errorCallBack(query);
        } else {
          const queryRes = query.multiQueryAll(['MessageText', 'ResponseCode']);
          if (!this.errorList) { this.errorList = []; }
          this.errorList.push({
            errorMessage: queryRes['MessageText'][0].getText(),
            errorCode: queryRes['ResponseCode'][0].getText()
          })
        }
      }
    }
  }

  public getXmlQuery(): XmlQuery<XmlTag> | null {
    const xmlConvert = new CustomConvertXml();
    if (this.response && typeof this.response.result === 'string') {
      const xmlTree = xmlConvert.convertToXmlTag(this.response.result)
      const xmlQuery = xmlTree.initQuery();
      return xmlQuery;
    }
    return null;
  };

  public get isSuccessful() {
    return this.response.responseCode === http.ResponseCode.OK;
  }

}