/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UnSubscribeResp } from '../model/streaming_subscription';
import { parseResponse } from '../utils/parse_jsobj_for_text';
import { JsObjectElement, XmlElement } from '../xml';
import { ResponseBase } from './response_base';

/**
 * 取消订阅响应器
 *
 * @since 2024-03-19
 */
export class UnSubscribeResponse extends ResponseBase<UnSubscribeResp> {
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMap: Map<string, string> = parseResponse(bodyElement);
    this.respCode = responseMap.get(XmlElement.NAME_RESPONSE_CODE);
  }
}
