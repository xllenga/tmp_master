/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { MoveCopyItemResp } from '../model/move_copy_item';
import { JsObjectElement } from './../xml/soap_xml';
import { ResponseBase, ResponseMessage } from './response_base';
import { Message } from '../model/mail/item_message';

/**
 * 移动文件夹响应
 *
 * @since 2024-03-19
 */
export class MoveCopyItemResponse extends ResponseBase<MoveCopyItemResp> {
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMessages: JsObjectElement[] = bodyElement?._elements?.[0]?._elements?.[0]?._elements ?? [];
    this.data?.readFromXml(responseMessages);

    // 根据拿到所有的code判断请求的code
    if (this.data.responseMessages.every((item: ResponseMessage<Message>) => {
      return item.responseCode === 'NoError';
    })) {
      this.respCode = 'NoError';
    } else {
      this.respCode = 'Error';
    }
  }
}