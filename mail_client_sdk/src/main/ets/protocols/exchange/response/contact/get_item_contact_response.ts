/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CompleteName,
  EmailAddress,
  GetContact,
  GetItemContactResponseModel,
  ImAddress,
  ImAddressesKey,
  PhoneNumber,
  PhoneNumbersKey,
  PhysicalAddressElementContent,
  PhysicalAddress
} from '../../model/contact/get_contact';
import { EffectiveRights } from '../../model/contact/get_contact';
import { JsObjectElement, XmlElement } from '../../xml';
import { ResponseBase } from '../response_base';
import { lowercaseFirstLetter } from '../../utils/sync_util';

/**
 * 查找联系人返回XML解析类
 *
 * @since 2024-04-09
 */
export class GetItemContactResponse extends ResponseBase<GetItemContactResponseModel> {
  /**
   * 查询联系人返回XML报文解析函数
   *
   * @param bodyElement Xml协议转换成JS对象的数据结构 此处为返回报文主体标签节点对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    if (!bodyElement._elements?.[0]?._elements?.[0]?._elements) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,get ResponseMessage failed');
    }

    const responseContact: JsObjectElement[] = bodyElement?._elements?.[0]?._elements?.[0]?._elements;
    const getItemResponseContactArr: GetContact[] = [];

    responseContact?.forEach(responseContactElement => {
      const responseContact: JsObjectElement[] = responseContactElement?._elements;
      const getItemResponseContact: GetContact = new GetContact();
      responseContact[1]?._elements[0]?._elements?.forEach(contactItem => {
        if (responseContactElement._attributes.ResponseClass === 'Error') {
          getItemResponseContact.responseClass = responseContactElement._attributes.ResponseClass;
          getItemResponseContact.responseCode = responseContact?.[0]?._elements?.[1]?._text;
          getItemResponseContact.messageText = responseContact?.[0]?._elements?.[0]?._text;
        } else if (responseContactElement._attributes.ResponseClass === 'Success') {
          getItemResponseContact.responseClass = responseContactElement._attributes.ResponseClass;
          getItemResponseContact.responseCode = responseContact?.[0]?._elements?.[0]?._text;
          switch (contactItem._name) {
            case XmlElement.NAME_ITEM_ID:
              this.resolveItemIdXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_PARENT_FOLDER_ID:
              this.resolveParentFolderIdXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_BODY:
              this.resolveBodyXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_EFFECTIVE_RIGHTS:
              this.resolveEffectiveRightsXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_CONVERSATION_ID:
              this.resolveConversationIdXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_FLAG:
              this.resolveFlagXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_COMPLETE_NAME:
              this.resolveCompleteNameXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_EMAIL_ADDRESSES:
              this.resolveEmailAddressXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_PHYSICAL_ADDRESSES:
              this.resolvePhysicalAddressXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_PHONE_NUMBERS:
              this.resolvePhoneNumberXML(getItemResponseContact, contactItem);
              break;
            case XmlElement.NAME_IM_ADDRESSES:
              this.resolveImAddressXML(getItemResponseContact, contactItem);
              break;
            default:
            /**
             * 常规标签与异常标签处理
             */
              this.resolveDefaultXML(getItemResponseContact, contactItem);
              break;
          }
        }
      })
      getItemResponseContactArr.push(getItemResponseContact);
    })
    const codeFlag: boolean = getItemResponseContactArr.every((item: GetContact) => {
      return item.responseCode === 'NoError';
    })
    if (codeFlag) {
      this.respCode = 'NoError';
    } else {
      this.respCode = 'Error';
    }
    this.data.contacts = getItemResponseContactArr;
  }

  /**
   * ItemIdXML解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveItemIdXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    getItemResponseContact.itemId = {
      id: contactItem._attributes.Id,
      changeKey: contactItem._attributes.ChangeKey
    };
  }

  /**
   * ParentFolderId解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveParentFolderIdXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    getItemResponseContact.parentFolderId = {
      id: contactItem._attributes.Id,
      changeKey: contactItem._attributes.ChangeKey
    };
  }

  /**
   * Body解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveBodyXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    getItemResponseContact.body = {
      bodyType: contactItem._attributes.BodyType,
      isTruncated: contactItem._attributes.IsTruncated
    };
  }

  /**
   * EffectiveRights解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveEffectiveRightsXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    let _effectiveRight: EffectiveRights = new EffectiveRights();
    contactItem._elements.forEach(effectiveRightItem => {
      _effectiveRight[lowercaseFirstLetter(effectiveRightItem._name)] =
        effectiveRightItem._elements?.[0]?._text;
    });
    getItemResponseContact.effectiveRights = _effectiveRight;
  }

  /**
   * ConversationId解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveConversationIdXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    getItemResponseContact.conversationId = {
      id: contactItem._attributes?.Id,
    };
  }

  /**
   * Flag解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveFlagXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    getItemResponseContact.flag = {
      flagStatus: contactItem._elements?.[0]?._elements?.[0]?._text
    };
  }

  /**
   * CompleteName解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveCompleteNameXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    let _completeName: CompleteName = new CompleteName();
    contactItem._elements.forEach(completeNameItem => {
      _completeName[lowercaseFirstLetter(completeNameItem._name)] =
        completeNameItem._elements?.[0]?._text;
    });
    getItemResponseContact.completeName = _completeName;
  }

  /**
   * EmailAddress解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveEmailAddressXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    let _emailAddresses: EmailAddress[] = [];
    contactItem._elements.forEach(_emailAddress => {
      _emailAddresses.push({
        key: _emailAddress._attributes.Key,
        name: _emailAddress._attributes.Name,
        routingType: _emailAddress._attributes.RoutingType,
        mailboxType: _emailAddress._attributes.MailboxType,
        elementContent: _emailAddress._elements?.[0]?._text
      });
    });
    getItemResponseContact.emailAddresses = _emailAddresses;
  }

  /**
   * PhysicalAddress解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolvePhysicalAddressXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    let _physicalAddresses: PhysicalAddress[] = [];
    contactItem._elements.forEach(_physicalAddress => {
      let _physicalAddressElementContent: PhysicalAddressElementContent = {};
      _physicalAddress._elements.forEach(_physicalAddressEntry => {
        _physicalAddressElementContent[lowercaseFirstLetter(_physicalAddressEntry._name)] =
          _physicalAddressEntry._elements?.[0]?._text;
      });
      _physicalAddresses.push({
        key: _physicalAddress._attributes.Key,
        elementContent: _physicalAddressElementContent
      });
    });
    getItemResponseContact.physicalAddresses = _physicalAddresses;
  }

  /**
   * PhoneNumber解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolvePhoneNumberXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    let _phoneNumbers: PhoneNumber[] = [];
    contactItem._elements.forEach(_phoneNumber => {
      if (_phoneNumber._elements) {
        _phoneNumbers.push({
          key: _phoneNumber._attributes.Key as PhoneNumbersKey,
          elementContent: _phoneNumber._elements?.[0]?._text
        });
      }
    });
    getItemResponseContact.phoneNumbers = _phoneNumbers;
  }

  /**
   * ImAddress解析器
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveImAddressXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    let _imAddresses: ImAddress[] = [];
    contactItem._elements.forEach(_imAddress => {
      if (_imAddress._elements) {
        _imAddresses.push({
          key: _imAddress._attributes.Key as ImAddressesKey,
          elementContent: _imAddress._elements?.[0]?._text
        })
      }
    })
    getItemResponseContact.imAddresses = _imAddresses;
  }

  /**
   * Default常规信息解析器
   * 通过判断改标签的子元素与属性是否存在来确定该标签是否为特殊标签
   * 如果都不存在则为单标签,赋值为null
   * 如果存在属性则为未做判断的特殊标签
   * 如果存在子元素,判断子元素类型是否为Text,如果是则为常规信息标签,如果不是则为未做判断的特殊标签
   *
   * @param getItemResponseContact Contact类实例化对象
   * @param contactItem 单组Contact信息集合
   */
  private resolveDefaultXML(getItemResponseContact: GetContact, contactItem: JsObjectElement) {
    if (contactItem._elements) {
      if (contactItem._elements[0]?._type !== 'text') {
        this.resolveUnknownXML(contactItem)
      } else {
        getItemResponseContact[lowercaseFirstLetter(contactItem._name)] = contactItem._elements?.[0]?._text
      }
    } else if (contactItem._attributes) {
      this.resolveUnknownXML(contactItem)
    } else {
      getItemResponseContact[lowercaseFirstLetter(contactItem._name)] = null
    }
  }

  /**
   * Unknown异常信息解析器
   * 弹出报错，报错信息为switch未做判断的特殊标签信息
   *
   * @param contactItem 单组Contact信息集合
   */
  private resolveUnknownXML(contactItem) {
    const errorRes: string = `The tag is unknown, please check the get_item_contact_response.ts, tag info:
          name:${contactItem?._name},
          type:${contactItem?._type},
          text:${contactItem?._text},
          attributes:${JSON.stringify(contactItem?._attributes)},
          elements:${JSON.stringify(contactItem?._elements)},`
    this.handleXmlDecodedError(errorRes);
  }
}