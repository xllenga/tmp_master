/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CreateContactResp,
  CreateContactResponseData,
  CreateContactResponseItem
} from '../../model/contact/create_contact';
import { JsObjectElement } from '../../xml';
import { ResponseBase } from '../response_base';

/**
 * 创建联系人返回XML报文解析类
 *
 * @since 2024-04-11
 */
export class CreateContactResponse extends ResponseBase<CreateContactResp> {
  /**
   * 创建联系人返回XML报文解析API
   *
   * @param bodyElement Xml协议转换成JS对象的数据结构 此结构为convertXml解析后的固定格式。
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    if (!bodyElement._elements[0]?._elements[0]?._elements) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,get ResponseMessage failed');
    }

    this.respCode = 'NoError';
    const responseMessage: JsObjectElement[] = bodyElement._elements[0]?._elements[0]?._elements;
    const createItemResponseContactArr: CreateContactResponseData[] = [];
    responseMessage?.forEach(responseContactElement => {
      const responseContact: JsObjectElement[] = responseContactElement?._elements;
      const createContactResponseContact: CreateContactResponseData = new CreateContactResponseData();
      switch (responseContactElement._attributes.ResponseClass) {
        case 'Success':
          createContactResponseContact.responseClass = responseContactElement._attributes.ResponseClass;
          createContactResponseContact.responseCode = responseContact[0]?._elements[0]?._text;
          const items: CreateContactResponseItem = {
            id: responseContact[1]?._elements[0]?._elements[0]?._attributes.Id,
            changeKey: responseContact[1]?._elements[0]?._elements[0]?._attributes.ChangeKey
          };
          createContactResponseContact.items = items;
          break;
        case 'Error':
          createContactResponseContact.responseClass = responseContactElement._attributes.ResponseClass;
          createContactResponseContact.responseCode = responseContact[0]?._elements[1]?._text;
          createContactResponseContact.messageText = responseContact[0]?._elements[0]?._text;
          break;
      }
      createItemResponseContactArr.push(createContactResponseContact);
    })
    this.data.contacts = createItemResponseContactArr;
  }
}