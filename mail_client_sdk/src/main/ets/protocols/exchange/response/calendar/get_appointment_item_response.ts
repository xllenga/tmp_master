/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetAppointmentItemResponseMessage} from '../../model/calendar/get_calendar_item_message';
import { FindItemResponseMessage, RootFolder } from '../../model/calendar/calendar_response_types';
import { fillObjectFields } from '../../utils/calendar_util';
import { JsObjectElement } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { ResponseBase } from '../response_base'
import { Calendar } from '../../model/calendar/item_calendar';

export class GetAppointmentItemResponse extends ResponseBase<GetAppointmentItemResponseMessage> {
  public constructor() {
    super();
    this.data = new GetAppointmentItemResponseMessage();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    this.parserElements(bodyElement._elements);
  }

  parserElements(elements?: JsObjectElement[], fillFindItemResponseMessage?: FindItemResponseMessage): void {
    elements?.forEach(element => {
      switch (element._name) {
        case XmlElement.NAME_RESPONSE_MESSAGES:
          this.data.responseMessages = [];
          break;
        case XmlElement.NAME_FIND_ITEM_RESPONSE_MESSAGE:
          fillFindItemResponseMessage = new FindItemResponseMessage();
          fillFindItemResponseMessage.responseClass = element._attributes.ResponseClass;
          this.data.responseMessages.push(fillFindItemResponseMessage);
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          fillFindItemResponseMessage[element._name] = element._elements[0]._text;
          break;
        case XmlElement.NAME_ROOT_FOLDER:
          fillFindItemResponseMessage.rootFolder = element._attributes as any as RootFolder;
          break;
        case XmlElement.NAME_ITEMS:
          fillFindItemResponseMessage.items = [];
          break;
        case XmlElement.NAME_CALENDAR_ITEM:
          let calendarItem = new Calendar();
          fillObjectFields(element, calendarItem);
          fillFindItemResponseMessage.items.push(calendarItem);
          break;
      }
      this.parserElements(element._elements, fillFindItemResponseMessage);
    });
  }
}