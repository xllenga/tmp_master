/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetAppointmentResponseMessage } from '../../model/calendar/get_calendar_message';
import {
  CalendarFolder,
  CalendarPermission,
  CalendarPermissions,
  EffectiveRights,
  FolderResponseMessage,
  PermissionSet,
  UserId
} from '../../model/calendar/calendar_response_types';
import { fillObjectFields } from '../../utils/calendar_util';
import { JsObjectElement } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { ResponseBase } from '../response_base'

export class GetAppointmentResponse extends ResponseBase<GetAppointmentResponseMessage> {
  public constructor() {
    super();
    this.data = new GetAppointmentResponseMessage();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    this.parserElements(bodyElement._elements);
  }

  parserElements(elements?: JsObjectElement[], fillFolderResponseMessage?: FolderResponseMessage, fillCalendarFolder?: CalendarFolder,
                 fillCalendarPermission?: CalendarPermission) : void {
    elements?.forEach(element => {
      switch (element._name) {
        case XmlElement.NAME_RESPONSE_MESSAGES:
          this.data.responseMessages = [];
          break;
        case XmlElement.NAME_GET_FOLDER_RESPONSE_MESSAGE:
          fillFolderResponseMessage = new FolderResponseMessage();
          fillFolderResponseMessage.responseClass = element._attributes.ResponseClass;
          this.data.responseMessages.push(fillFolderResponseMessage);
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          fillFolderResponseMessage[element._name] = element._elements[0]._text;
          break;
        case XmlElement.NAME_FOLDERS:
          fillFolderResponseMessage.folders = [];
          break;
        case XmlElement.NAME_CALENDAR_FOLDER:
          fillCalendarFolder = new CalendarFolder()
          fillObjectFields(element, fillCalendarFolder);
          fillFolderResponseMessage.folders.push(fillCalendarFolder);
          break;
        case XmlElement.NAME_EFFECTIVE_RIGHTS:
          fillCalendarFolder.effectiveRights = new EffectiveRights();
          fillObjectFields(element, fillCalendarFolder.effectiveRights);
          break;
        case XmlElement.NAME_PERMISSION_SET:
          fillCalendarFolder.permissionSet = new PermissionSet();
          break;
        case XmlElement.NAME_CALENDAR_PERMISSIONS:
          fillCalendarFolder.permissionSet.calendarPermissions = new CalendarPermissions();
          fillCalendarFolder.permissionSet.calendarPermissions.calendarPermission = [];
          break;
        case XmlElement.NAME_CALENDAR_PERMISSION:
          fillCalendarPermission = new CalendarPermission();
          fillObjectFields(element, fillCalendarPermission);
          fillCalendarFolder.permissionSet.calendarPermissions.calendarPermission.push(fillCalendarPermission);
          break;
        case XmlElement.NAME_USER_ID:
          fillCalendarPermission.userId = new UserId();
          fillObjectFields(element, fillCalendarPermission.userId);
          break;
      }
      this.parserElements(element._elements, fillFolderResponseMessage, fillCalendarFolder, fillCalendarPermission);
    });
  }
}