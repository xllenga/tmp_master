/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetItemBatchResponseMessage } from '../../model/calendar/get_item_batch_message';
import {
  Attendee,
  EffectiveRights,
  FlagStatus,
  ItemResponseMessage,
  Mailbox,
  Organizer
} from '../../model/calendar/calendar_response_types';
import { fillObjectFields } from '../../utils/calendar_util';
import { JsObjectElement } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { ResponseBase } from '../response_base'
import { Calendar } from '../../model/calendar/item_calendar';

export class GetItemBatchResponse extends ResponseBase<GetItemBatchResponseMessage> {
  public constructor() {
    super();
    this.data = new GetItemBatchResponseMessage();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    this.parserElements(bodyElement._elements);
  }

  parserElements(elements?: JsObjectElement[], fillItemResponseMessage?: ItemResponseMessage): void {
    elements?.forEach(element => {
      switch (element._name) {
        case XmlElement.NAME_RESPONSE_MESSAGES:
          this.data.responseMessages = [];
          break;
        case XmlElement.NAME_GET_ITEM_RESPONSE_MESSAGE:
          fillItemResponseMessage = new ItemResponseMessage();
          fillItemResponseMessage.responseClass = element._attributes.ResponseClass;
          this.data.responseMessages.push(fillItemResponseMessage);
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          fillItemResponseMessage[element._name] = element._elements[0]._text;
          break;
        case XmlElement.NAME_CALENDAR_ITEM:
          fillItemResponseMessage.calendar = new Calendar();
          fillObjectFields(element, fillItemResponseMessage.calendar);
          break;
        case XmlElement.NAME_EFFECTIVE_RIGHTS:
          fillItemResponseMessage.calendar.effectiveRights = new EffectiveRights();
          fillObjectFields(element, fillItemResponseMessage.calendar.effectiveRights);
          break;
        case XmlElement.NAME_FLAG:
          fillItemResponseMessage.calendar.flag = new FlagStatus();
          fillObjectFields(element, fillItemResponseMessage.calendar.flag);
          break;
        case XmlElement.NAME_ORGANIZER:
          fillItemResponseMessage.calendar.organizer = new Organizer();
          element._elements.forEach(element => {
            fillItemResponseMessage.calendar.organizer.mailbox = new Mailbox();
            fillObjectFields(element, fillItemResponseMessage.calendar.organizer.mailbox);
          });
          break
        case XmlElement.NAME_REQUIRED_ATTENDEES:
          fillItemResponseMessage.calendar.requiredAttendees = [];
          let attendee: Attendee;
          element._elements.forEach(element => {
            attendee = new Attendee();
            element._elements.forEach(childElement => {
              if (childElement._name === XmlElement.NAME_MAILBOX) {
                attendee.mailbox = new Mailbox();
                fillObjectFields(childElement, attendee.mailbox);
              }else if (childElement._name === XmlElement.NAME_RESPONSE_TYPE) {
                attendee.responseType = childElement._elements[0]._text;
              }
            });
            fillItemResponseMessage.calendar.requiredAttendees.push(attendee);
          });
          break
      }
      this.parserElements(element._elements, fillItemResponseMessage);
    });
  }
}