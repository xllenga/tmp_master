/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetRoomsResponseMessage } from '../../model/calendar/get_rooms_message';
import { RoomsResponseMessage, Room, Mailbox } from '../../model/calendar/calendar_response_types';
import { fillObjectFields } from '../../utils/calendar_util';
import { JsObjectElement } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { ResponseBase } from '../response_base'

export class GetRoomsResponse extends ResponseBase<GetRoomsResponseMessage> {
  public constructor() {
    super();
    this.data = new GetRoomsResponseMessage();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    console.log('liwang---->' + JSON.stringify(bodyElement));
    this.printEle(bodyElement._elements);
  }

  printEle(elements?: JsObjectElement[], item?: RoomsResponseMessage, room?: Room): void {
    elements?.forEach(element => {
      switch (element._name) {
        case XmlElement.NAME_GET_ROOMS_RESPONSE:
          item = new RoomsResponseMessage();
          item.responseClass = element._attributes.ResponseClass;
          this.data.setResponseMessages(item);
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          item[element._name] = element._elements[0]._text;
          break;
        case XmlElement.NAME_ROOMS:
          item.rooms = [];
          break;
        case XmlElement.NAME_ROOM:
          room = new Room();
          item.rooms.push(room);
          break;
        case XmlElement.NAME_ID:
          room.id = new Mailbox();
          fillObjectFields(element, room.id);
          break;
      }
      this.printEle(element._elements, item, room);
    });
  }
}