/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getLogger, Logger } from '../../../utils/log';
import { DeleteItemResp } from '../model/delete_item';
import { XmlElement } from '../xml';
import { JsObjectElement } from '../xml/soap_xml';
import { ResponseBase, ResponseMessage } from './response_base';

const logger: Logger = getLogger("DeleteItemResponse");

/**
 * 删除邮件、联系人、日历等公共请求响应类
 *
 * @since 2024-04-08
 */
export class DeleteItemResponse extends ResponseBase<DeleteItemResp> {
  /**
   * 解析返回报文
   *
   * @param bodyElement
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    logger.info('bodyElement' + JSON.stringify(bodyElement));
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error, body element is null');
    }

    // 获取需要解析的ResponseMessages节点数据
    const responseMessagesNode: JsObjectElement[] = bodyElement._elements?.[0]?._elements?.[0]?._elements;
    if (!responseMessagesNode || responseMessagesNode.length < 1) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error, get ResponseMessage failed');
    }

    // 解析后的responseMessage数组
    const responseMessagesObject: ResponseMessage[] = responseMessagesNode.map(responseMessageElement => {
      return this.generateDeleteItemResponseMessage(responseMessageElement);
    });

    // 判断整体是否成功
    const allNoError: Boolean = responseMessagesObject?.every((responseMessage) => {
      return responseMessage.responseCode === 'NoError';
    });

    // 子元素有任意一个失败，则整体失败
    this.respCode = allNoError ? 'NoError' : 'Error';
    this.data.responseMessages = responseMessagesObject;
  }

  /**
   * 根据节点数据生成ResponseMessage对象
   *
   * @param responseMessageElement 解析后的responseMessage节点数据
   * @returns ResponseMessage<Message> 生成的responseMessage对象
   */
  private generateDeleteItemResponseMessage(responseMessageElement: JsObjectElement): ResponseMessage {
    let deleteItemResponseMessage: ResponseMessage = new ResponseMessage();
    deleteItemResponseMessage.responseClass = responseMessageElement?._attributes?.ResponseClass;
    responseMessageElement?._elements?.forEach(resMsgElement => {
      this.processDeleteItemResponseMessage(resMsgElement, deleteItemResponseMessage);
    });
    return deleteItemResponseMessage;
  }

  /**
   * 处理 getDeleteResponseMessage 的具体属性
   *
   * @param resMsgElement 解析后的responseMessage节点数据
   * @param deleteItemResponseMessage 具体的responseMessage对象
   */
  private processDeleteItemResponseMessage(resMsgElement: JsObjectElement,
                                           deleteItemResponseMessage: ResponseMessage): void {
    switch (resMsgElement._name) {
      case XmlElement.NAME_RESPONSE_CODE:
        deleteItemResponseMessage.responseCode = resMsgElement._elements[0]?._text;
        break;
      case XmlElement.NAME_MESSAGE_TEXT:
        deleteItemResponseMessage.messageText = resMsgElement._elements[0]?._text;
        break;
      case XmlElement.NAME_DESCRIPTIVE_LINK_KEY:
        deleteItemResponseMessage.descriptiveLinkKey = resMsgElement._elements[0]?._text;
        break;
      default:
        logger.info('resMsgElement._name is not correct.');
        break;
    }
  }
}