/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UniteId } from '../../model/mode_base';
import {
  IEffectiveRights,
  IFlag,
  IMailBox,
  FindItemResp,
  IFindItemResp,
} from '../../model/search/mail_search_findItems';
import { getResponseMessage, lowercaseFirstLetter } from '../../utils/sync_util';
import { JsObjectElement, XmlElement } from '../../xml';
import { ResponseBase } from '../response_base';

/**
 * 搜索邮件响应类
 *
 * @since 2024-04-08
 */
export class SearchMailResponse extends ResponseBase<FindItemResp> {
  /**
   * 解析响应报文体
   *
   * @param {bodyElement} 报文体json对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    this.data = new FindItemResp();
    const res: JsObjectElement[] = getResponseMessage(bodyElement, XmlElement.NAME_FIND_ITEM_RESPONSE_MESSAGE);
     for(let item of res){
       const elementName:string | undefined = item._name;
       const elementValue:string | undefined = item._elements?.[0]?._text;
       switch (elementName) {
         case XmlElement.NAME_RESPONSE_CODE:
           this.respCode = elementValue;
           break;
         case XmlElement.NAME_ROOT_FOLDER:
           const attr = item._attributes;
             this.data.includesLastItemInRange = attr.IncludesLastItemInRange;
             this.data.indexedPagingOffset = attr.IndexedPagingOffset;
             this.data.totalItemsInView = attr.IndexedPagingOffset;
           const findItems:JsObjectElement[] | undefined = item._elements?.[0]?._elements;
           if (!findItems) {
             break;
           }
           this.data.findItemMessages = [];
           for (let findItem of findItems) {
             const properties:JsObjectElement[] | undefined = findItem._elements;
             const findItemValue:IFindItemResp = this.readFindItems(properties);
             this.data.findItemMessages.push(findItemValue);
           }
           break;
         default:
           break;
       }
     }
  }

  /**
   * 解析搜索邮件返回项目集合
   *
   * @param properties 邮件元素
   * @returns 解析完成邮件项目JSON数据
   */
  private readFindItems(properties: JsObjectElement[]): IFindItemResp {
    let findItems = {};
    for (let proItem of properties) {
      const tagName:string | undefined = proItem._name;
      const attrName: string = lowercaseFirstLetter(tagName);
      const findElements:JsObjectElement[] | undefined = proItem._elements;
      if (tagName === XmlElement.NAME_ITEM_ID || tagName === XmlElement.NAME_PARENT_FOLDER_ID) {
        const itemIdValue: UniteId = {
          id: proItem?._attributes.Id,
          changeKey: proItem?._attributes.ChangeKey,
        }
        findItems[attrName] = itemIdValue;
      } else if (tagName === XmlElement.NAME_EFFECTIVE_RIGHTS) {
        findItems[attrName] = this.readEffectiveRights(findElements);
      } else if (tagName === XmlElement.NAME_FLAG) {
        findItems[attrName] = this.readFlag(findElements);
      } else if (tagName === XmlElement.NAME_FROM ||
        tagName === XmlElement.NAME_SENDER ||
        tagName === XmlElement.NAME_RECEIVED_BY ||
        tagName === XmlElement.NAME_RECEIVED_REPRESENTING) {
        findItems[attrName] = this.readReport(findElements);
      } else {
        if (!findElements) {
          continue;
        }
        findItems[attrName] = findElements?.[0]?._text;
      }
    }
    return findItems as IFindItemResp;
  }

  /**
   * 解析EffectiveRights对象
   *
   * @param effectiveRightsElements EffectiveRights对象属性
   * @returns Flag对象属性集合
   */
  private readEffectiveRights(effectiveRightsElements: JsObjectElement[]): IEffectiveRights {
    let effectiveRights = {};
    for (let effItem of effectiveRightsElements) {
      const effectiveName:string | undefined = effItem._name;
      const effectiveValue:string | undefined = effItem._elements?.[0]?._text;
      effectiveRights[lowercaseFirstLetter(effectiveName)] = effectiveValue;
    }
    return effectiveRights;
  }

  /**
   * 解析Flag对象
   *
   * @param flagElements Flag对象属性
   * @returns Flag对象属性集合
   */
  private readFlag(flagElements: JsObjectElement[]): IFlag {
    let flags = {};
    for (let flagItem of flagElements) {
      const flagName:string | undefined = flagItem._name;
      const flagValue:string | undefined = flagItem._elements?.[0]?._text;
      flags[lowercaseFirstLetter(flagName)] = flagValue;
    }
    return flags as IFlag;
  }

  /**
   * 解析MailBox对象
   *
   * @param MailBoxElements MailBox对象属性
   * @returns MailBox对象
   */
  private readReport(MailBoxElements: JsObjectElement[]): IMailBox {
    let mailBoxes = {};
    const mailBox: JsObjectElement[] | undefined = MailBoxElements?.[0]?._elements;
    for (let mailBoxItem of mailBox) {
      const mailBoxName:string | undefined = mailBoxItem._name;
      const mailBoxValue:string | undefined = mailBoxItem._elements?.[0]?._text;
      mailBoxes[lowercaseFirstLetter(mailBoxName)] = mailBoxValue;
    }
    return mailBoxes;
  }
}