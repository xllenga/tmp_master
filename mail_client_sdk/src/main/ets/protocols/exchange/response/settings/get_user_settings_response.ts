/* Copyright © 2023 - 2024 Coremail论客.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UserSettingMessageResp } from '../../model/settings/autodiscover_message';
import { JsObjectElement } from '../../xml/soap_xml';
import { ResponseBase } from '../response_base';

/**
 * GetUserSettingsResponse 处理自动发现请求，接口响应
 *
 * @since 2024-04-12
 */
export class GetUserSettingsResponse extends ResponseBase<UserSettingMessageResp> {
  public readBodyFromXml(bodyElement: JsObjectElement): void {
    this.respCode = 'NoError';
    this.getElementText(bodyElement);
  }

  private getElementText(elementParameter: JsObjectElement): void {
      elementParameter?._elements?.forEach((item) => {
        if (item._name == 'Value' && item?._elements?.length === 1) {
          this.data.ewsEndpoint = item?._elements[0]._text;
          return;
        }
        this.getElementText(item)
      })
  }
}