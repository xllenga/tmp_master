/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CreateAttachmentResp,
  DeleteAttachmentResp,
  FileAttachment,
  GetAttachmentResp,
  ItemAttachment,
  GetAttachmentResponseMessage,
  DownLoadAttachmentResp,
  CreateAttachmentResponseMessage,
  DeleteAttachmentResponseMessage
} from '../../model/mail/attachments';
import { JsObjectElement } from '../../xml/soap_xml'
import { ResponseBase } from '../response_base'
import { getLogger, Logger } from '../../../../utils/log';
import { XmlElement } from '../../xml/xml_element';

const logger: Logger = getLogger('AttachmentResponse');

/**
 * 创建附件响应类
 *
 * @since 2024-04-01
 */
export class CreateAttachmentResponse extends ResponseBase<CreateAttachmentResp> {
  public constructor() {
    super();
    this.data = new CreateAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMessage = bodyElement._elements[0]._elements[0]._elements;
    logger.info('CreateAttachmentResponse',responseMessage);
    const createAttachmentResponseMessageArr: CreateAttachmentResponseMessage[] = [];
    responseMessage?.forEach(attachmentResponseMessage => {
      const createAttachmentResponseMessage = new CreateAttachmentResponseMessage();
      this.parseResponseMessage(attachmentResponseMessage,createAttachmentResponseMessage);
      createAttachmentResponseMessageArr.push(createAttachmentResponseMessage);
    })

    // 根据拿到所有的responseCode判断请求的respCode
    if (createAttachmentResponseMessageArr.every(item => { return item.responseCode === 'NoError'})) {
      this.respCode = 'NoError';
    } else {
      this.respCode = 'Error';
    }
    this.data.responseMessages = createAttachmentResponseMessageArr;
  }

  /**
   * 解析返回的信息
   *
   * @param attachmentResponseMessage 返回的每一组信息
   * @param createAttachmentResponseMessage 创建返回信息的model
   */
  private parseResponseMessage(attachmentResponseMessage: any,
                               createAttachmentResponseMessage: CreateAttachmentResponseMessage) {
    const elements = attachmentResponseMessage._elements;
    switch (attachmentResponseMessage._attributes.ResponseClass) {
      case 'Success':
        this.parseSuccess(elements, createAttachmentResponseMessage);
        break;
      case 'Error':
        this.parseError(elements, createAttachmentResponseMessage);
        break;
      default:
        break;
    }
  }

  /**
   * 解析返回成功的信息
   *
   * @param elements 返回的信息
   * @param createAttachmentResponseMessage 创建返回信息的model
   */
  private parseSuccess(elements, createAttachmentResponseMessage) {
    elements.forEach(item =>{
      switch (item._name) {
        case XmlElement.NAME_RESPONSE_CODE:
          createAttachmentResponseMessage.responseCode = item._elements[0]._text;
          break;
        case XmlElement.NAME_ATTACHMENTS:
          const attachments = item._elements;
          this.parseAttachments(attachments, createAttachmentResponseMessage);
          break;
        default:
          break;
      }
    })
  }

  /**
   * 解析返回失败的信息
   *
   * @param elements 返回的信息
   * @param createAttachmentResponseMessage 创建返回信息的model
   */
  private parseError(elements, createAttachmentResponseMessage) {
    elements.forEach(item => {
      switch(item._name) {
        case XmlElement.NAME_MESSAGE_TEXT:
          createAttachmentResponseMessage.messageText = item._elements[0]._text;
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          createAttachmentResponseMessage.responseCode = item._elements[0]._text;
          break;
        default:
          break;
      }
    })
  }

  /**
   * 解析返回的附件信息
   *
   * @param attachments 返回的信息
   * @param createAttachmentResponseMessage 创建附件返回信息的model
   */
  private parseAttachments(attachments: any, createAttachmentResponseMessage: CreateAttachmentResponseMessage) {
    attachments.forEach(attachment => {
      if (attachment._name === XmlElement.NAME_FILE_ATTACHMENT) {
        const fileAttachments = attachment._elements;
        const fileAttachment = new FileAttachment();
        fileAttachment.readFromXml(fileAttachments);
        createAttachmentResponseMessage.attachmentList = [fileAttachment];
      } else {
        const itemAttachments = attachment._elements;
        const itemAttachment = new ItemAttachment();
        itemAttachment.readFromXml(itemAttachments);
        createAttachmentResponseMessage.attachmentList = [itemAttachment];
      }
    })
  }
}

/**
 * 删除附件响应类
 *
 * @since 2024-04-01
 */
export class DeleteAttachmentResponse extends ResponseBase<DeleteAttachmentResp> {
  public constructor() {
    super();
    this.data = new DeleteAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMessage = bodyElement._elements[0]._elements[0]._elements;
    logger.info('DeleteAttachmentResponse',responseMessage);
    const deleteAttachmentResponseMessageArr: DeleteAttachmentResponseMessage[] = [];
    responseMessage.forEach(attachmentResponseMessage => {
      const deleteAttachmentResponseMessage = new DeleteAttachmentResponseMessage();
      this.parseResponseMessage(attachmentResponseMessage, deleteAttachmentResponseMessage);
      deleteAttachmentResponseMessageArr.push(deleteAttachmentResponseMessage);
    })

    // 根据拿到所有的responseCode判断请求的respCode
    if (deleteAttachmentResponseMessageArr.every(item => { return item.responseCode === 'NoError'})) {
      this.respCode = 'NoError';
    } else {
      this.respCode = 'Error';
    }
    this.data.responseMessages = deleteAttachmentResponseMessageArr;
  }

  /**
   * 解析返回的信息
   *
   * @param attachmentResponseMessage 返回的每一组信息
   * @param deleteAttachmentResponseMessage 删除返回信息的model
   */
  private parseResponseMessage(attachmentResponseMessage: any,
                               deleteAttachmentResponseMessage: DeleteAttachmentResponseMessage) {
    const elements = attachmentResponseMessage._elements;
    switch (attachmentResponseMessage._attributes.ResponseClass) {
      case 'Success':
        this.parseSuccess(elements, deleteAttachmentResponseMessage);
        break;
      case 'Error':
        this.parseError(elements, deleteAttachmentResponseMessage);
        break;
      default:
        break;
    }
  }

  /**
   * 解析返回成功的信息
   *
   * @param elements 返回的信息
   * @param DeleteAttachmentResponseMessage 删除返回信息的model
   */
  private parseSuccess(elements: any, deleteAttachmentResponseMessage: DeleteAttachmentResponseMessage) {
    elements.forEach(item =>{
      if (item._name === XmlElement.NAME_RESPONSE_CODE) {
        deleteAttachmentResponseMessage.responseCode = item._elements[0]._text;
      }
    })
  }

  /**
   * 解析返回失败的信息
   *
   * @param elements 返回的信息
   * @param deleteAttachmentResponseMessage 删除附件返回信息的model
   */
  private parseError(elements: any, deleteAttachmentResponseMessage: DeleteAttachmentResponseMessage) {
    elements.forEach(item =>{
      switch(item._name) {
        case XmlElement.NAME_MESSAGE_TEXT:
          deleteAttachmentResponseMessage.messageText = item._elements[0]._text;
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          deleteAttachmentResponseMessage.responseCode = item._elements[0]._text;
          break;
        default:
          break;
      }
    })
  }
}

/**
 * 获取附件响应类
 *
 * @since 2024-04-01
 */
export class GetAttachmentResponse extends ResponseBase<GetAttachmentResp> {
  public constructor() {
    super();
    this.data = new GetAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMessage = bodyElement._elements[0]._elements[0]._elements;
    logger.info('GetAttachmentResponse',responseMessage);
    const getAttachmentResponseMessageArr: GetAttachmentResponseMessage[] = [];
    responseMessage.forEach(attachmentResponseMessage => {
      const getAttachmentResponseMessage = new GetAttachmentResponseMessage();
      this.parseResponseMessage(attachmentResponseMessage,getAttachmentResponseMessage);
      getAttachmentResponseMessageArr.push(getAttachmentResponseMessage);
    })

    // 根据拿到所有的responseCode判断请求的respCode
    if (getAttachmentResponseMessageArr.every(item => { return item.responseCode === 'NoError'})) {
      this.respCode = 'NoError';
    } else {
      this.respCode = 'Error';
    }
    this.data.responseMessages = getAttachmentResponseMessageArr;
  }

  /**
   * 解析返回的信息
   *
   * @param attachmentResponseMessage 返回的每一组信息
   * @param getAttachmentResponseMessage 获取附件返回信息的model
   */
  private parseResponseMessage(attachmentResponseMessage: any,
                               getAttachmentResponseMessage: GetAttachmentResponseMessage) {
    const elements = attachmentResponseMessage._elements;
    switch (attachmentResponseMessage._attributes.ResponseClass) {
      case 'Success':
        this.parseSuccess(elements, getAttachmentResponseMessage);
        break;
      case 'Error':
        this.parseError(elements, getAttachmentResponseMessage);
        break;
      default:
        break;
    }
  }

  /**
   * 解析返回成功的信息
   *
   * @param elements 返回的信息
   * @param GetAttachmentResponseMessage 获取附件返回信息的model
   */
  private parseSuccess(elements: any, getAttachmentResponseMessage: GetAttachmentResponseMessage) {
    elements.forEach(item =>{
      switch(item._name) {
        case XmlElement.NAME_RESPONSE_CODE:
          getAttachmentResponseMessage.responseCode = item._elements[0]._text;
          break;
        case XmlElement.NAME_ATTACHMENTS:
          const attachments = item._elements;
          this.parseAttachments(attachments, getAttachmentResponseMessage);
          break;
        default:
          break;
      }
    })
  }

  /**
   * 解析返回失败的信息
   *
   * @param elements 返回的信息
   * @param getAttachmentResponseMessage 获取附件返回信息的model
   */
  private parseError(elements: any, getAttachmentResponseMessage: GetAttachmentResponseMessage) {
    elements.forEach(item =>{
      switch(item._name) {
        case XmlElement.NAME_MESSAGE_TEXT:
          getAttachmentResponseMessage.messageText = item._elements[0]._text;
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          getAttachmentResponseMessage.responseCode = item._elements[0]._text;
          break;
        default:
          break;
      }
    })
  }

  /**
   * 解析返回的附件信息
   *
   * @param attachments 返回的信息
   * @param getAttachmentResponseMessage 获取附件返回信息的model
   */
  private parseAttachments(attachments: any, getAttachmentResponseMessage: GetAttachmentResponseMessage) {
    attachments.forEach(attachment => {
      if (attachment._name === XmlElement.NAME_FILE_ATTACHMENT) {
        const fileAttachments = attachment._elements;
        const fileAttachment = new FileAttachment();
        fileAttachment.readFromXml(fileAttachments);
        getAttachmentResponseMessage.attachmentList = [fileAttachment];
      } else {
        const itemAttachments = attachment._elements;
        const itemAttachment = new ItemAttachment();
        itemAttachment.readFromXml(itemAttachments);
        getAttachmentResponseMessage.attachmentList = [itemAttachment];
      }
    })
  }
}

/**
 * 下载附件响应类
 *
 * @since 2024-04-01
 */
export class DownLoadAttachmentResponse extends ResponseBase<DownLoadAttachmentResp> {
  public downLoadPath:string;

  public constructor() {
    super();
    this.data = new DownLoadAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    } else {
      this.respCode = 'NoError';
    }
  }
}
