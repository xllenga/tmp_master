/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateItem } from '../../model/create_item';
import { ModelBase } from '../../model/mode_base';
import { parseResponse } from '../../utils/parse_jsobj_for_text';
import { XmlElement, JsObjectElement } from '../../xml';
import { ResponseBase } from '../response_base';

/**
 * 发送邮件响应
 *
 * @since 2024-03-16
 */
export class SendMailResponse extends ResponseBase<CreateItem> {
  private id: string = '';  // TODO：这两个字段用来调试草稿，后期删除。同步开发结束后，通过同步可获取。
  private changeKey: string = '';

  public getId(): string {
    return this.id;
  }

  public getChangeKey(): string {
    return this.changeKey;
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMap: Map<string, string> = parseResponse(bodyElement);
    this.respCode = responseMap.get(XmlElement.NAME_RESPONSE_CODE);

    try {
      const resMessage = bodyElement._elements[0]._elements[0]._elements[0];
      this.id = resMessage._elements[1]._elements[0]._elements[0]._attributes.Id;
      this.changeKey = resMessage._elements[1]._elements[0]._elements[0]._attributes.ChangeKey;
    } catch {}
  }
}

/**
 * 发送草稿响应
 *
 * @since 2024-03-16
 */
export class SendItemResponse extends ResponseBase<ModelBase> {
  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMap: Map<string, string> = parseResponse(bodyElement);
    this.respCode = responseMap.get(XmlElement.NAME_RESPONSE_CODE);
  }
}