import { ModelBase } from '../../model/mode_base';
import { parseResponse } from '../../utils/parse_jsobj_for_text';
import { JsObjectElement, XmlElement } from '../../xml';
import { ResponseBase } from '../response_base';

/**
 * 转发回复
 *
 * @since 2024-04-20
 */
export class ForwardReplyItemResponse extends ResponseBase<ModelBase> {
  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMap: Map<string, string> = parseResponse(bodyElement);
    this.respCode = responseMap.get(XmlElement.NAME_RESPONSE_CODE);
  }
}