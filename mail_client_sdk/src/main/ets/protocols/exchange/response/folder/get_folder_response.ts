/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getLogger, Logger } from '../../../../utils/log';
import { ResponseBase, ResponseMessage } from '../response_base';
import { GetFolderResp } from '../../model/folder/get_folder';
import { JsObjectElement, XmlElement } from '../../xml/index';
import { Folder } from '../../model/folder/folder';
import { ContactsFolder } from '../../model/folder/contacts_folder';
import { CalendarFolder } from '../../model/folder/calendar_folder';
import { SearchFolder } from '../../model/folder/search_folder';
import { TasksFolder } from '../../model/folder/tasks_folder';

const logger: Logger = getLogger('GetFolderResponse');
const SUCCESS_RESPONSE = 'Success';

/**
 * 获取文件夹响应
 *
 * @since 2024-03-19
 */
export class GetFolderResponse extends ResponseBase<GetFolderResp> {
  public constructor() {
    super();
    this.data = new GetFolderResp();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    // decode body
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMessages = bodyElement?._elements?.[0]?._elements?.[0]?._elements;
    let responses: ResponseMessage<Folder>[] = [];
    responseMessages?.forEach(item => {
      this.readFolderMassage(item, responses);
    })
    if (responses.every((item: ResponseMessage<Folder>) => {
      return item.responseCode === 'NoError';
    })) {
      this.respCode = 'NoError';
    } else {
      this.respCode = 'Error';
    }
    logger.info(responses);
    this.data.responseMessages = responses;
  }

  /**
   * 解析Folder响应
   *
   * @param folderMassage 文件夹响应信息
   * @param responses 解析结果列表
   */
  private readFolderMassage(folderMassage: JsObjectElement, responses: ResponseMessage<Folder>[]) {
    let response: ResponseMessage<Folder> = new ResponseMessage();
    let responseMessage = folderMassage?._elements;
    response.responseClass = folderMassage?._attributes?.ResponseClass;
    responseMessage?.forEach(folderInfo => {
      this.readFolderInfo(folderInfo, response);
    })
    responses.push(response);
  }

  /**
   * 解析文件夹信息
   *
   * @param folderInfo 文件夹信息
   * @param response 单个解析结果
   */
  private readFolderInfo(folderInfo: JsObjectElement, response: ResponseMessage<Folder>) {
    let elementsName = folderInfo?._name;
    switch (elementsName) {
      case XmlElement.NAME_RESPONSE_CODE:
        response.responseCode = folderInfo?._elements?.[0]?._text;
        break;
      case XmlElement.NAME_MESSAGE_TEXT:
        response.messageText = folderInfo?._elements?.[0]?._text;
        break;
      case XmlElement.NAME_FOLDERS:
        let folders: Folder[] = []
        if (folderInfo._elements && response.responseClass === SUCCESS_RESPONSE) {
          let folderType = folderInfo?._elements?.[0]?._name;
          this.readFolder(folderType, folders, folderInfo);
        }
        response.items = folders;
        break;
      default:
        break;
    }
  }

  /**
   * 解析不同类型的文件夹
   *
   * @param folderType 文件夹类型
   * @param folders 文件夹列表
   * @param folderInfo 文件夹信息列表
   */
  private readFolder(folderType: string, folders: Folder[], folderInfo: JsObjectElement) {
    switch (folderType) {
      case XmlElement.NAME_FOLDER:
        const folder = new Folder();
        folder.readFromXml(folderInfo._elements);
        folders.push(folder);
        break;
      case XmlElement.NAME_CONTACTS_FOLDER:
        const contactsFolder = new ContactsFolder();
        contactsFolder.readFromXml(folderInfo._elements);
        folders.push(contactsFolder);
        break;
      case XmlElement.NAME_CALENDAR_FOLDER:
        const calendarFolder = new CalendarFolder();
        calendarFolder.readFromXml(folderInfo._elements);
        folders.push(calendarFolder);
        break;
      case XmlElement.NAME_SEARCH_FOLDER:
        const searchFolder = new SearchFolder();
        searchFolder.readFromXml(folderInfo._elements);
        folders.push(searchFolder);
        break;
      case XmlElement.NAME_TASKS_FOLDER:
        const tasksFolder = new TasksFolder();
        tasksFolder.readFromXml(folderInfo._elements);
        folders.push(tasksFolder);
        break;
      default:
        break;
    }
  }
}