/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CalendarChange,
  ContactChange,
  FolderItemsResp,
  ItemChange,
  MeetingCancellationChange,
  MessageChange,
  Mailbox,
  MeetingRequestChange,
  EffectiveRights
} from '../../model/sync/folder_items';
import { JsObjectElement } from '../../xml/soap_xml';
import { ResponseBase } from '../response_base';
import { UniteId } from '../../model/mode_base';
import { getResponseMessage, lowercaseFirstLetter, readSpecProp } from '../../utils/sync_util';
import { XmlElement } from '../../xml/index';
import {
  CompleteName,
  ConversationId,
  EmailAddress,
  Flag,
  ImAddress,
  PhoneNumber,
  PhysicalAddress,
  PhysicalAddressElementContent
} from '../../model/contact/get_contact';
import { getLogger } from '../../../../utils/log';

const logger = getLogger('SyncFolderItems');

/**
 * 处理同步文件夹内容项的响应
 *
 * @since 2024-03-24
 */
export class SyncFolderItemsResponse extends ResponseBase<FolderItemsResp> {
  /**
   * 解析响应报文体
   *
   * @param bodyElement 报文体json对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    logger.log('read syncFolderItemsResponse: ' + JSON.stringify(bodyElement));
    let responseMessage = getResponseMessage(bodyElement, XmlElement.NAME_SYNC_FOLDER_ITEMS_RESPONSE_MESSAGE);
    let length = responseMessage.length;
    for (let i = 0; i < length; i++) {
      let elementName = responseMessage[i]._name;
      let elementValue = responseMessage[i]._elements?.[0]?._text;
      switch (elementName) {
        case XmlElement.NAME_RESPONSE_CODE:
          this.respCode = elementValue;
          break;
        case XmlElement.NAME_SYNC_STATE:
          this.data.syncState = elementValue;
          break;
        case XmlElement.NAME_INCLUDES_LAST_ITEM_IN_RANGE:
          this.data.includesLastItemInRange = elementValue;
          break;
        case XmlElement.NAME_CHANGES:
          this.data.itemChanges = this.readChanges(responseMessage[i]);
          break;
        default:
          break;
      }
    }
  }

  /**
   * 解析文件夹中变更的项目
   *
   * @param changeElement 报文changes下的元素
   * @returns 变更的内容
   */
  private readChanges(changeElement: JsObjectElement): ItemChange[] {
    let itemChanges: ItemChange[] = [];
    let changeElements = changeElement._elements;
    if (!changeElements) {
      return [];
    }
    changeElements.forEach(changeElement => {
      let changeType = changeElement._name;
      let changeClass = changeElement._elements[0]._name;
      let changeItem;
      switch (changeClass) {
        case XmlElement.NAME_CONTACT:
          changeItem = {} as ContactChange;
          break;
        case XmlElement.NAME_MESSAGE:
          changeItem = {} as MessageChange;
          break;
        case XmlElement.NAME_MEETING_CANCELLATION:
          changeItem = {} as MeetingCancellationChange;
          break;
        case XmlElement.NAME_MEETING_REQUEST:
          changeItem = {} as MeetingRequestChange;
          break;
        case XmlElement.NAME_CALENDAR_ITEM:
          changeItem = {} as CalendarChange;
          break;
        default:
          break;
      }
      this.readItemChange(changeItem, changeType, changeElement);
      itemChanges.push(changeItem);
    })
    return itemChanges;
  }

  /**
   * 解析变更的文件夹项目
   *
   * @param itemChange 变更的项目
   * @param changeType 变更的类型（增删改）
   * @param changeElement 变更的元素
   */
  private readItemChange(itemChange: ItemChange, changeType: string, changeElement: JsObjectElement): void {
    itemChange.changeType = changeType;
    let itemProperties = changeElement._elements[0]._elements;
    itemProperties.forEach(itemProperty => {
      let tagName = itemProperty._name;
      let attrName = lowercaseFirstLetter(tagName);
      let properties = itemProperty._elements;
      let attributes = itemProperty._attributes;
      if (!properties && !attributes) {
        logger.info('there has no property or attribute.')
        return;
      }
      if (tagName === XmlElement.NAME_ID_ITEM || tagName === XmlElement.NAME_PARENT_FOLDER_ID) {
        let folderId: UniteId = {
          id: attributes.Id,
          changeKey: attributes.ChangeKey
        };
        itemChange[attrName] = folderId;
      } else if (tagName === XmlElement.NAME_EFFECTIVE_RIGHTS) {
        itemChange[attrName] = readSpecProp(properties, {} as EffectiveRights);
      } else if (tagName === XmlElement.NAME_CONVERSATION_ID) {
        let conversationId: ConversationId = { id: attributes.Id };
        itemChange[attrName] = conversationId;
      } else if (tagName === XmlElement.NAME_FLAG) {
        itemChange[attrName] = readSpecProp(properties, {} as Flag);
      } else if (tagName === XmlElement.NAME_COMPLETE_NAME) {
        itemChange[attrName] = readSpecProp(properties, {} as CompleteName);
      } else if (tagName === XmlElement.NAME_EMAIL_ADDRESS) {
        itemChange[attrName] = this.readEmailAddresses(properties);
      } else if (tagName === XmlElement.NAME_PHYSICAL_ADDRESSES) {
        itemChange[attrName] = this.readPhysicalAddresses(properties);
      } else if (tagName === XmlElement.NAME_PHONE_NUMBERS) {
        itemChange[attrName] = this.readPhoneNumbers(properties);
      } else if (tagName === XmlElement.NAME_IM_ADDRESSES) {
        itemChange[attrName] = this.readImAddresses(properties);
      } else if (tagName === XmlElement.NAME_FROM || tagName === XmlElement.NAME_ORGANIZER ||
        tagName === XmlElement.NAME_RECEIVED_BY || tagName === XmlElement.NAME_SENDER ||
        tagName === XmlElement.NAME_RECEIVED_REPRESENTING) {
        itemChange[attrName] = this.readMailBoxItem(properties);
      } else {
        itemChange[attrName] = properties?.[0]?._text;
      }
    })
  }

  /**
   * 解析联系人电子邮件集合
   *
   * @param properties 电子邮件地址属性
   * @returns 电子邮件地址集合
   */
  private readEmailAddresses(properties: JsObjectElement[]): EmailAddress[] {
    let emailAddresses: EmailAddress[] = [];
    properties.forEach(property => {
      let emailAddress: EmailAddress = { key: 'EmailAddress1', elementContent: '' };
      emailAddress.key = property._attributes.Key as typeof emailAddress.key;
      emailAddress.elementContent = property._elements?.[0]?._text;
      emailAddresses.push(emailAddress);
    })
    return emailAddresses;
  }

  /**
   * 解析联系人关联的物理地址集合
   *
   * @param properties 物理地址属性
   * @returns 物理地址集合
   */
  private readPhysicalAddresses(properties: JsObjectElement[]): PhysicalAddress[] {
    let physicalAddresses: PhysicalAddress[] = [];
    properties.forEach(property => {
      let physicalAddress: PhysicalAddress = { key: 'Home', elementContent: {} };
      physicalAddress.key = property._attributes.Key as typeof physicalAddress.key;
      let addressProperties = property._elements;
      let phyAddress: PhysicalAddressElementContent = {};
      addressProperties.forEach(addressProperty => {
        if (addressProperty) {
          phyAddress[addressProperty._name] = addressProperty._elements?.[0]?._text;
        }
      })
      physicalAddress.elementContent = phyAddress;
      physicalAddresses.push(physicalAddress);
    })
    return physicalAddresses;
  }

  /**
   * 联系人电话号码集合
   *
   * @param properties 电话号码属性
   * @returns 电话号码集合
   */
  private readPhoneNumbers(properties: JsObjectElement[]): PhoneNumber[] {
    let phoneNumbers: PhoneNumber[] = [];
    properties.forEach(property => {
      let phoneNumber: PhoneNumber = { key: 'HomePhone', elementContent: '' };
      phoneNumber.key = property._attributes.Key as typeof phoneNumber.key;
      if (property._elements) {
        phoneNumber.elementContent = property._elements[0]?._text;
      }
      phoneNumbers.push(phoneNumber);
    })
    return phoneNumbers;
  }

  /**
   * 联系人即时消息地址集合
   *
   * @param properties 即时消息地址属性
   * @returns 即时消息地址集合
   */
  private readImAddresses(properties: JsObjectElement[]): ImAddress[] {
    let imAddresses: ImAddress[] = [];
    properties.forEach(property => {
      let imAddress: ImAddress = { key: 'ImAddress1', elementContent: '' };
      imAddress.key = property._attributes.Key as typeof imAddress.key;
      imAddress.elementContent = property._elements?.[0]?._text;
      imAddresses.push(imAddress);
    })
    return imAddresses;
  }

  /**
   * 解析MailBox对象
   *
   * @param properties MailBox对象属性
   * @returns MailBox对象
   */
  private readMailBoxItem(properties: JsObjectElement[]): Mailbox {
    let mailbox: Mailbox = {};
    let mailBoxProperties = properties[0]?._elements;
    mailBoxProperties.forEach(mailBoxProperty => {
      let name = mailBoxProperty._name;
      let value = mailBoxProperty._elements?.[0]?._text;
      mailbox[lowercaseFirstLetter(name)] = value;
    })
    return mailbox;
  }
}