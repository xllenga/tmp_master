/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  FolderItem,
  FolderHierarchyResp,
  FolderChange,
  EffectiveRights,
  Permission,
  UserId
} from '../../model/sync/folder_hierarchy';
import { UniteId } from '../../model/mode_base';
import { getResponseMessage, lowercaseFirstLetter, readSpecProp } from '../../utils/sync_util';
import { XmlElement } from '../../xml/index';
import { JsObjectElement } from '../../xml/soap_xml';
import { ResponseBase } from '../response_base';
import { getLogger } from '../../../../utils/log';

const logger = getLogger('SyncFolderHierarchy');

/**
 * 处理同步文件夹列表的响应
 *
 * @since 2024-03-25
 */
export class SyncFolderHierarchyResponse extends ResponseBase<FolderHierarchyResp> {
  /**
   * 解析响应报文体
   *
   * @param bodyElement 报文体json对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    logger.log('enter read syncFolderHierarchyResponse.');
    let responseMessage = getResponseMessage(bodyElement, XmlElement.NAME_SYNC_FOLDER_HIERARCHY_RESPONSE_MESSAGE);
    responseMessage.forEach(respMsg => {
      let elementName = respMsg._name;
      let elements = respMsg._elements;
      if (!elements) {
        logger.warn('syncFolderHierarchyResponse, there is no elements.')
        return;
      }
      let elementValue = respMsg._elements![0]._text;
      switch (elementName) {
        case XmlElement.NAME_RESPONSE_CODE:
          this.respCode = elementValue;
          break;
        case XmlElement.NAME_SYNC_STATE:
          if (respMsg._elements) {
            this.data.syncState = elementValue;
          }
          break;
        case XmlElement.NAME_INCLUDES_LAST_FOLDER_IN_RANGE:
          this.data.includesLastItemInRange = elementValue;
          break;
        case XmlElement.NAME_CHANGES:
          this.data.folders = [];
          elements.forEach(changeItem => {
            let folderChange: FolderChange = {};
            folderChange.changeType = changeItem._name;
            let properties = changeItem._elements![0]._elements;
            this.readFolderChange(properties, folderChange);
            this.data.folders.push(folderChange);
          })
          break;
        default:
          break;
      }
    })
  }

  /**
   * 解析变更的文件夹项目
   *
   * @param properties 文件夹变更项属性
   * @param folderChange 文件变更项实例
   */
  private readFolderChange(properties: JsObjectElement[], folderChange: FolderChange) {
    let folderItem: FolderItem = {};
    properties.forEach(property => {
      let tagName = property._name;
      let attrName = lowercaseFirstLetter(tagName);
      let propElements = property._elements;
      if (tagName === XmlElement.NAME_FOLDER_ID || tagName === XmlElement.NAME_PARENT_FOLDER_ID) {
        let folderId: UniteId = {
          id: property._attributes.Id,
          changeKey: property._attributes.ChangeKey
        }
        folderItem[attrName] = folderId;
      } else if (tagName === XmlElement.NAME_EFFECTIVE_RIGHTS) {
        folderItem[attrName] = readSpecProp(propElements, {} as EffectiveRights);
      } else if (tagName === XmlElement.NAME_PERMISSION_SET) {
        let permissionElements = propElements[0]._elements;
        folderItem[attrName] = this.readPermissionSet(permissionElements);
      } else {
        folderItem[attrName] = propElements![0]._text;
      }
      folderChange.folder = folderItem;
    })
  }

  /**
   * 解析文件夹权限集合
   *
   * @param permissionElements 权限元素
   * @returns 权限集合
   */
  private readPermissionSet(permissionElements: JsObjectElement[]): Permission[] {
    let permissions: Permission[] = [];
    permissionElements.forEach(permissionElement => {
      let permission: Permission = {};
      let permissionProperties = permissionElement._elements;
      permissionProperties.forEach(permissionProperty => {
        let propName = permissionProperty._name;
        let propValue = permissionProperty._elements[0]._text;
        if (propName === XmlElement.NAME_USER_ID) {
          let userIdElements = permissionProperty._elements;
          let userId: UserId = {};
          userIdElements.forEach(userIdElement => {
            let name = userIdElement._name;
            let value = userIdElement._elements[0]._text;
            userId[lowercaseFirstLetter(name)] = value;
          })
          permission.userId = userId;
        } else {
          permission[lowercaseFirstLetter(propName)] = propValue;
        }
      })
      permissions.push(permission);
    })
    return permissions;
  }
}