/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SoapXmlWriter } from '../../xml/soap_xml';
import { FieldURISchema, FolderName, XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';
import { Filters, FindItems } from '../../model/search/mail_search_findItems';
import {
  ContainmentMode,
  ComparisonMode,
  ExcludesBitmask,
  ContainsSubstring,
  Exists,
  IsEqualTo,
  IsNotEqualTo,
  IsGreaterThan,
  IsGreaterThanOrEqual,
  IsLessThan,
  IsLessThanOrEqual,
  Not,
  Composite,
  LogicalOperator,
  SearchMailFilter,
} from '../../model/search/mail_search_filter';
import {
  BasePoint,
  IItemView,
  IPropertySet,
  ISortOrder,
  SearchView,
  SortOrder
} from '../../model/search/mail_search_view';
import { UniteId } from '../../model/mode_base';
import { PropertySet, PropertyType } from '../../utils/common_enum';

/**
 * 关系测试筛选器类型别名
 */
type RelationshipFilterType = IsGreaterThan | IsGreaterThanOrEqual | IsLessThan | IsLessThanOrEqual;

export class SearchMailRequest extends RequestBase<FindItems<Filters>> {
  /**
   * 将协议头写入
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 根据参数构建ews协议报文体
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    const resView: SearchView = this.rawData.view; // 获取页面参数
    const resFilter: Filters = this.rawData.filter; // 获取筛选器
    const resQueryString: string = this.rawData.queryString; // 获取查询字符串
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FIND_ITEM)
      .writeAttribute(XmlAttribute.NAME_TRAVERSAL, resView.itemTraversal);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE);
    const baseShape: PropertySet = resView.baseShape;
    if (baseShape) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, resView.baseShape);
    }
    const fieldURI: (IPropertySet | FieldURISchema)[] = resView.fieldURIArr;
    if (fieldURI) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ADDITIONAL_PROPERTIES);
      fieldURI?.forEach(item => {
        if (typeof item === 'string') {
          soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
            .writeAttribute(XmlAttribute.NAME_FIELD_URI, item)
            .writeEndElement();
        } else {
          if (resFilter) {
            this.setExtendedFieldURI(soapXmlWriter, resFilter);
          }
        }
      })
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    }

    // 设置分页属性
    this.setItemView(soapXmlWriter, resView);

    // 设置搜索筛选器
    if (resFilter) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_RESTRICTION);
      this.setSearchFilters(soapXmlWriter, resFilter);
      soapXmlWriter.writeEndElement();
    }

    // 设置排序方式
    this.setOrderBy(soapXmlWriter, resView);

    // 设置文件夹
    this.setFolder(soapXmlWriter);

    // 设置要搜索的查询字符串
    if (resQueryString) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.MESSAGE, XmlElement.NAME_QUERY_STRING, resQueryString);
    }
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 设置搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 搜索筛选器
   */
  private setSearchFilters(soapXmlWriter: SoapXmlWriter, resFilter: Filters): void {
    const filterHandlers = new Map<Function, (soapXmlWriter: SoapXmlWriter, resFilter: Filters) => void>([
      [SearchMailFilter.ContainsSubstring, this.containsSubstringFilter],
      [SearchMailFilter.ExcludesBitmask, this.excludesBitmaskFilter],
      [SearchMailFilter.Exists, this.existsFilter],
      [SearchMailFilter.IsEqualTo, this.isEqualToOrIsNotEqualToFilter],
      [SearchMailFilter.IsNotEqualTo, this.isEqualToOrIsNotEqualToFilter],
      [SearchMailFilter.IsGreaterThan, this.relationshipFilter],
      [SearchMailFilter.IsGreaterThanOrEqual, this.relationshipFilter],
      [SearchMailFilter.IsLessThan, this.relationshipFilter],
      [SearchMailFilter.IsLessThanOrEqual, this.relationshipFilter],
      [SearchMailFilter.Not, this.notFilter],
      [SearchMailFilter.Composite, this.compositeFilter],
    ]);
    if (filterHandlers) {
      const handler = filterHandlers.get(resFilter.constructor);
      handler(soapXmlWriter, resFilter);
    }
  }

  /**
   * 包含搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 包含搜索筛选器
   */
  private containsSubstringFilter(soapXmlWriter: SoapXmlWriter, resFilter: ContainsSubstring): void {
    if (resFilter.containmentMode || resFilter.comparisonMode) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONTAINS)
        .writeAttribute(XmlAttribute.NAME_CONTAINMENT_MODE, ContainmentMode[resFilter.containmentMode])
        .writeAttribute(XmlAttribute.NAME_CONTAINMENT_COMPARISON, ComparisonMode[resFilter.comparisonMode]);
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONTAINS);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
      .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      .writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONSTANT)
      .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.value)
      .writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 位掩码排除搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 位掩码排除搜索筛选器
   */
  private excludesBitmaskFilter(soapXmlWriter: SoapXmlWriter, resFilter: ExcludesBitmask): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXCLUDES);
    if (typeof resFilter.fieldURIString === 'string') {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      soapXmlWriter.writeEndElement();
    } else {
      this.setExtendedFieldURI(soapXmlWriter, resFilter);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_BITMASK)
      .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.bitmask)
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 存在搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 存在搜索筛选器
   */
  private existsFilter(soapXmlWriter: SoapXmlWriter, resFilter: Exists): void {
    const fieldURIString: FieldURISchema = resFilter.fieldURIString;
    if (typeof fieldURIString === 'string') {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXISTS);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXISTS);
      this.setExtendedFieldURI(soapXmlWriter, resFilter)
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 相等和不相等搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 相等和不相等搜索筛选器
   */
  private isEqualToOrIsNotEqualToFilter(soapXmlWriter: SoapXmlWriter, resFilter: IsEqualTo | IsNotEqualTo): void {
    if (resFilter instanceof SearchMailFilter.IsEqualTo) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_EQUAL_TO);
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_NOT_EQUAL_TO);
    }

    const fieldURIString: FieldURISchema = resFilter.fieldURIString;
    if (typeof fieldURIString === 'string') {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI_OR_CONSTANT);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONSTANT)
        .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.value);
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    } else {
      this.setExtendedFieldURI(soapXmlWriter, resFilter);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI_OR_CONSTANT);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.value)
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    }
    soapXmlWriter.writeEndElement();
  }

  /**
   * 关系测试搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 关系测试搜索筛选器
   */
  private relationshipFilter(soapXmlWriter: SoapXmlWriter, resFilter: RelationshipFilterType): void {
    if (resFilter instanceof SearchMailFilter.IsGreaterThan) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_GREATER_THAN);
    } else if (resFilter instanceof SearchMailFilter.IsGreaterThanOrEqual) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_GREATER_THAN_OR_EQUAL_TO);
    } else if (resFilter instanceof SearchMailFilter.IsLessThan) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_LESS_THAN);
    } else if (resFilter instanceof SearchMailFilter.IsLessThanOrEqual) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_LESS_THAN_OR_EQUAL_TO);
    }
    this.setExtendedFieldURI(soapXmlWriter, resFilter);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI_OR_CONSTANT);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONSTANT)
      .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.value);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 否定搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 否定搜索筛选器
   */
  private notFilter(soapXmlWriter: SoapXmlWriter, resFilter: Not): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_NOT);
    this.setSearchFilters(soapXmlWriter, resFilter.searchFilter);
    soapXmlWriter.writeEndElement();
  }

  /**
   * 复合搜索筛选器
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 复合搜索筛选器
   */
  private compositeFilter(soapXmlWriter: SoapXmlWriter, resFilter: Composite): void {
    const logicalOperator: LogicalOperator = resFilter.logicalOperator;
    if (!logicalOperator) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_AND);
      resFilter?.searchFilters.forEach((item) => {
        this.setSearchFilters(soapXmlWriter, item);
      })
      soapXmlWriter.writeEndElement();
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_OR);
      resFilter?.searchFilters.forEach((item) => {
        this.setSearchFilters(soapXmlWriter, item);
      })
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 设置分页参数
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resView} 搜索视图
   */
  private setItemView(soapXmlWriter: SoapXmlWriter, resView: SearchView): void {
    const itemView: IItemView = resView.itemView;
    if (itemView && itemView.maxEntriesReturned) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_INDEXED_PAGE_ITEM_VIEW)
        .writeAttribute(XmlAttribute.NAME_MAX_ENTRIES_RETURNED, itemView.maxEntriesReturned)
        .writeAttribute(XmlAttribute.NAME_OFFSET, itemView.offset)
        .writeAttribute(XmlAttribute.NAME_BASE_POINT, BasePoint[itemView.basePoint])
        .writeEndElement();
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_INDEXED_PAGE_ITEM_VIEW)
        .writeAttribute(XmlAttribute.NAME_OFFSET, itemView.offset)
        .writeAttribute(XmlAttribute.NAME_BASE_POINT, BasePoint[itemView.basePoint])
        .writeEndElement();
    }
  }

  /**
   * 设置排序方式
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resView} 搜索视图
   */
  private setOrderBy(soapXmlWriter: SoapXmlWriter, resView: SearchView): void {
    const orderBy: ISortOrder = resView.orderBy;
    if (orderBy) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SORT_ORDER);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_ORDER)
        .writeAttribute(XmlAttribute.NAME_ORDER, SortOrder[orderBy.orderBy]);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, orderBy.FieldURI)
        .writeEndElement();
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 判断是否UniteId
   *
   * @param obj
   */
  private isUniteId(obj: FolderName | UniteId): obj is UniteId {
    return typeof obj === 'object' && obj !== null && 'id' in obj && "changeKey" in obj;
  }

  /**
   * 设置搜索的文件夹
   *
   * @param {soapXmlWriter} xml写入器
   */
  private setFolder(soapXmlWriter: SoapXmlWriter): void {
    const resFolder: FolderName | UniteId = this.rawData.folder; // 获取要搜索的文件夹
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_PARENT_FOLDER_IDS);
    if (typeof resFolder === 'string') {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID)
        .writeAttribute(XmlAttribute.NAME_ID, FolderName[resFolder])
        .writeEndElement();
      soapXmlWriter.writeEndElement();

    } else if (this.isUniteId(resFolder)) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID)
        .writeAttribute(XmlAttribute.NAME_ID, resFolder.id)
        .writeAttribute(XmlAttribute.NAME_CHANGE_KEY, resFolder.changeKey);
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    }
  }

  /**
   * 设置公共扩展属性
   *
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 搜索筛选器
   */
  private setExtendedFieldURI(soapXmlWriter:SoapXmlWriter, resFilter:Filters):void {
    if (resFilter.propertySet) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_TAG, resFilter.propertySet.PropertyTag)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_SET_ID, resFilter.propertySet.propertySetId)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_NAME, resFilter.propertySet.propertyName)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_TYPE, PropertyType[resFilter.propertySet.propertyType])
        .writeEndElement();
    }
  }
}
