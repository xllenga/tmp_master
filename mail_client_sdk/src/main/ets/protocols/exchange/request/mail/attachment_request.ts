/*
 * Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateAttachment, DeleteAttachment, DownLoadAttachment, GetAttachment } from '../../model/mail/attachments';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';

/**
 * 创建附件请求类
 *
 * @since 2024-04-01
 */
export class CreateAttachmentRequest extends RequestBase<CreateAttachment> {
  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_CREATE_ATTACHMENT);
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.MESSAGE, XmlElement.NAME_PARENT_ITEM_ID,
      new Map([[XmlAttribute.NAME_ID, this.rawData.parentItemId]]));
    this.rawData.attachment.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 删除附件请求类
 *
 * @since 2024-04-01
 */
export class DeleteAttachmentRequest extends RequestBase<DeleteAttachment> {
  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_DELETE_ATTACHMENT);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ATTACHMENT_IDS);
    this.rawData.attachmentIds?.forEach((item) =>{
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_ATTACHMENT_ID ,
        new Map([[XmlAttribute.NAME_ID, item]]));
    })
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 获取附件请求类
 *
 * @since 2024-04-01
 */
export class GetAttachmentRequest extends RequestBase<GetAttachment> {
  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_ATTACHMENT);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ATTACHMENT_IDS);
    this.rawData.attachmentIds?.forEach((item) =>{
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_ATTACHMENT_ID ,
        new Map([[XmlAttribute.NAME_ID, item]]));
    })
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 下载附件请求类
 *
 * @since 2024-04-01
 */
export class DownLoadAttachmentRequest extends RequestBase<DownLoadAttachment> {
  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_ATTACHMENT);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ATTACHMENT_IDS);
    this.rawData.attachmentIds?.forEach((item) =>{
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_ATTACHMENT_ID ,
        new Map([[XmlAttribute.NAME_ID, item]]));
    })
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}
