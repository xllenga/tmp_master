/*
 * Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ForwardItem, ReplyAllToItem } from '../../model/mail/forward_reply_item';
import { SoapXmlWriter, XmlElement, XmlNamespace } from '../../xml';
import { RequestBase } from '../request_base';

/**
 * 转发
 *
 * @since 2024-04-20
 */
export class ForwardItemRequest extends RequestBase<ForwardItem> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    if (!this.rawData) {
      this.handleXmlEncodedError('encode xml body error,rawData is null');
    }
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    this.rawData.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 回复
 *
 * @since 2024-04-20
 */
export class ReplyItemRequest extends RequestBase<ReplyAllToItem> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    if (!this.rawData) {
      this.handleXmlEncodedError('encode xml body error,rawData is null');
    }
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    this.rawData.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}