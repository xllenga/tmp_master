/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateContact } from '../../model/contact/create_contact';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { CreateItemRequest } from '../create_item_request';

/**
 * 新增联系人请求XML构建类
 *
 * @since 2024-4-9
 */
export class CreateContactRequest extends CreateItemRequest<CreateContact> {

  /**
   * 创建联系人请求XML报文Header构建器
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 创建联系人请求XML报文Contact主体构建器
   *
   * @param soapXmlWriter xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   */
  protected writeItemsChildren(soapXmlWriter: SoapXmlWriter) {
    this.rawData.contacts.forEach(contact => {
      contact.writeToXml(soapXmlWriter, 'Create');
    })
  }
}