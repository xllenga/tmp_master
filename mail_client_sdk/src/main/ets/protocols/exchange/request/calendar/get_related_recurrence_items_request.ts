/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetRelatedRecurrenceItemsMessage } from '../../model/calendar/get_related_recurrence_items_message';
import { SoapXmlWriter } from '../../xml/soap_xml'
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base'

/**
 * 访问定期系列（使用 EWS 托管 API 获取相关的重复日历项）
 */
export class GetRelatedRecurrenceItemsRequest extends RequestBase<GetRelatedRecurrenceItemsMessage> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_ITEM);

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, new Map(), this.rawData.getBaseShape().valueOf());

    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ADDITIONAL_PROPERTIES);
    this.rawData.getFieldURIs().forEach(element => {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI, new Map([[XmlAttribute.NAME_FIELD_URI, element.valueOf()]]));
    });
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_IDS);
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.MESSAGE, XmlElement.NAME_OCCURRENCE_ITEM_ID, new Map([
      [XmlAttribute.NAME_RECURRING_MASTER_ID, this.rawData.getOccurrenceItemId().recurringMasterId],
      [XmlAttribute.NAME_INSTANCE_INDEX, String(this.rawData.getOccurrenceItemId().instanceIndex)],
    ]));
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}