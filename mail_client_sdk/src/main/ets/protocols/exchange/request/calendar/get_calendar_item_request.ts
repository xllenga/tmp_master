/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetCalendarItemMessage } from '../../model/calendar/get_calendar_item_message';
import { SoapXmlWriter } from '../../xml/soap_xml'
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base'

/**
 * 获取日历项详细数据报文
 */
export class GetCalendarItemRequest extends RequestBase<GetCalendarItemMessage> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FIND_ITEM)
      .writeAttribute(XmlAttribute.NAME_TRAVERSAL, this.rawData.traversal.valueOf())
      .writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE)
      .writeIntactElement(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, new Map(), this.rawData.baseShape.valueOf());
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ADDITIONAL_PROPERTIES);
    this.rawData.fieldUris.forEach(element => {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI, new Map([[XmlAttribute.NAME_FIELD_URI, element.valueOf()]]));
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.MESSAGE, XmlElement.NAME_CALENDAR_VIEW, new Map([
      [XmlAttribute.NAME_MAX_ENTRIES_RETURNED, this.rawData.calendarView.maxEntriesReturned],
      [XmlAttribute.NAME_START_DATE, this.rawData.calendarView.startDate],
      [XmlAttribute.NAME_END_DATE, this.rawData.calendarView.endDate]
    ]));
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_PARENT_FOLDER_IDS);
    this.rawData.parentFolderIds.forEach(element => {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID
        , new Map([
          [XmlAttribute.NAME_ID, element.id],
          [XmlAttribute.NAME_CHANGE_KEY, element.changeKey]
        ]))
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}