/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { XmlNamespace } from '../../xml/xml_namespace';
import { SoapXmlWriter, XmlAttribute, XmlElement } from '../../xml/index';
import { CreateItemRequest } from '../create_item_request';
import { BodyType } from '../../model/mail/item_message';
import { CancelMeetingMessage } from '../../model/calendar/cancel_meeting_message';

/*
 * 构建取消会议请求报文
 * */
export class CancelMeetingRequest extends CreateItemRequest<CancelMeetingMessage> {
  protected writeItemsChildren(soapXmlWriter: SoapXmlWriter): void {
    for (const item of this.rawData.items) {
      soapXmlWriter.writeElementWithAttribute(
        XmlNamespace.TYPE,
        XmlElement.NAME_CANCEL_CALENDAR_ITEM,
        new Map([[XmlNamespace.XMLNS, XmlNamespace.SCHEMA_TYPE]]),
        false
      );
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_REFERENCE_ITEM_ID)
        .writeAttribute(XmlAttribute.NAME_ID, item.id)
        .writeAttribute(XmlAttribute.NAME_CHANGE_KEY, item.changeKey)
        .writeEndElement();

      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_NEW_BODY_CONTENT)
        .writeAttribute(XmlAttribute.NAME_BODY_TYPE, BodyType[BodyType.HTML])
        .writeElementValue(item.content)
        .writeEndElement();

      soapXmlWriter.writeEndElement();
    }
  }
}
