/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AppointmentMessage } from '../../model/calendar/find_appointments';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { FolderName } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';

export class FindAppointmentsRequest extends RequestBase<AppointmentMessage> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY); //1
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FIND_ITEM) //2

    if (this.rawData.calendarView.Traversal == 0) {
      soapXmlWriter.writeAttribute(XmlElement.NAME_TRAVERSAL, 'Shallow');
    } else if (this.rawData.calendarView.Traversal == 1) {
      soapXmlWriter.writeAttribute(XmlElement.NAME_TRAVERSAL, 'SoftDeleted');
    } else if (this.rawData.calendarView.Traversal == 2) {
      soapXmlWriter.writeAttribute(XmlElement.NAME_TRAVERSAL, 'Associated');
    }

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE)
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, 'Default')
    soapXmlWriter.writeEndElement();
    let calendarData = this.rawData.calendarView;
    if (calendarData.StartDate && calendarData.EndDate && calendarData.getMaxItemsReturned()) {
      let attributeMap: Map<string, string> = new Map();
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.MESSAGE, XmlElement.NAME_CALENDAR_VIEW, attributeMap.set('MaxEntriesReturned', JSON.stringify(calendarData.getMaxItemsReturned())).set('StartDate', calendarData.StartDate.format()).set('EndDate', calendarData.EndDate.format()));
    } else if ((calendarData.StartDate && calendarData.EndDate) || calendarData.getMaxItemsReturned()) {
      let attributeMap: Map<string, string> = new Map();
      if (calendarData.StartDate && calendarData.EndDate) {
        soapXmlWriter.writeElementWithAttribute(XmlNamespace.MESSAGE, XmlElement.NAME_CALENDAR_VIEW, attributeMap.set('StartDate', calendarData.StartDate.format()).set('EndDate', calendarData.EndDate.format()));
      } else {
        soapXmlWriter.writeElementWithAttribute(XmlNamespace.MESSAGE, XmlElement.NAME_CALENDAR_VIEW, attributeMap.set('MaxEntriesReturned', JSON.stringify(calendarData.getMaxItemsReturned())));
      }
    }

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_PARENT_FOLDER_IDS)
    if (this.rawData.FolderId == FolderName.calendar) {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID, new Map().set('Id', 'calendar'))
    } else {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID, new Map().set('Id', this.rawData.FolderId))
    }
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}