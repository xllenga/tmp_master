/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { StreamingSubscription } from '../model/streaming_subscription';
import { SoapXmlWriter, XmlElement, XmlNamespace } from '../xml';
import { RequestBase } from './request_base';

/**
 * 流式处理通知协议
 *
 * @since 2024-03-19
 */
export class GetStreamingEventsRequest extends RequestBase<StreamingSubscription> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_STREAMING_EVENTS);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SUBSCRIPTION_IDS)
      .writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_SUBSCRIPTION_ID, this.rawData.subscriptionId)
      .writeEndElement();
    soapXmlWriter.writeElementWithValue(XmlNamespace.MESSAGE, XmlElement.NAME_CONNECTION_TIMEOUT,
      this.rawData.connectionTime);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}
