/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetItem } from '../model/get_item';
import { UniteId } from '../model/mode_base';
import { SoapXmlWriter, XmlAttribute, XmlElement, XmlNamespace } from '../xml';
import { RequestBase } from './request_base';

/**
 * 获取item请求类
 *
 * @since 2024-03-19
 */
export class GetItemRequest extends RequestBase<GetItem> {
  /**
   * 写入公共头部
   *
   * @param soapXmlWriter xml写入工具类
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 写入请求的body
   *
   * @param soapXmlWriter  xml写入工具类
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    // 获取参数，拼装xml
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_ITEM);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE);
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, this.rawData.baseShape);
    if (this.rawData.includeMimeContent) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE,
        XmlElement.NAME_INCLUDE_MIME_CONTENT, this.rawData.includeMimeContent);
    }

    // 有任意一个，就开始写入
    if (this.rawData.fieldURI || this.rawData.extendedFieldURI) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ADDITIONAL_PROPERTIES);
      if (this.rawData.fieldURI) {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I);
        soapXmlWriter.writeAttribute(XmlElement.NAME_FIELD_UR_I, this.rawData.fieldURI);
        soapXmlWriter.writeEndElement();
      }
      if (this.rawData.indexFiledURL) {
        this.rawData.indexFiledURL.writeToXml(soapXmlWriter);
      }
      if (this.rawData.extendedFieldURI) {
        this.rawData.extendedFieldURI.writeToXml(soapXmlWriter);
      }
      soapXmlWriter.writeEndElement();
    }
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_IDS);
    this.setItemIds(soapXmlWriter);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 设置ItemIds节点
   *
   * @param soapXmlWriter  xml写入工具类
   */
  private setItemIds(soapXmlWriter: SoapXmlWriter): void {
    let itemIds: UniteId[] = this.rawData.itemIds;
    if (itemIds && itemIds.length) {
      itemIds.forEach(itemId => {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ID_ITEM);
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_ID, itemId.id);
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_CHANGE_KEY, itemId.changeKey ?? '');
        soapXmlWriter.writeEndElement();
      });
    }
  }
}