/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderId } from '../../model/folder/folder';
import { GetFolder } from '../../model/folder/get_folder';
import { SoapXmlWriter, XmlElement, XmlNamespace } from '../../xml/index';
import { RequestBase } from '../request_base';

/**
 * 获取文件夹请求
 *
 * @since 2024-03-19
 */
export class GetFolderRequest extends RequestBase<GetFolder> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_FOLDER);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDER_SHAPE);
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, this.rawData.baseShape);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDER_IDS);
    let folderIds: FolderId[] = this.rawData.folderIds;
    folderIds?.forEach(folder => {
      folder.writeToXml(soapXmlWriter);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}