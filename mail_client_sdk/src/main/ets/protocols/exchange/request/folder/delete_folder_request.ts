/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DeleteFolder } from '../../model/folder/delete_folder';
import { FolderId } from '../../model/folder/folder';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';

/**
 * 删除文件夹请求
 *
 * @since 2024-03-19
 */
export class DeleteFolderRequest extends RequestBase<DeleteFolder> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_DELETE_FOLDER)
      .writeAttribute(XmlAttribute.NAME_DELETE_TYPE, this.rawData.deleteType);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDER_IDS);
    let deleteFolders: FolderId[] = this.rawData.folderIds;
    deleteFolders?.forEach(folder => {
      folder.writeToXml(soapXmlWriter);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}