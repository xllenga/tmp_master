/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CreateFolder } from '../../model/folder/create_folder';
import { Folder, FolderId, FolderIPFType } from '../../model/folder/folder';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';

/**
 * 创建文件夹请求
 *
 * @since 2024-03-19
 */
export class CreateFolderRequest extends RequestBase<CreateFolder> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_CREATE_FOLDER);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_PARENT_FOLDER_ID);
    let parentFolderId: FolderId = this.rawData.parentFolderId;
    parentFolderId?.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDERS);
    let folders: Folder[] = this.rawData.folders;
    folders?.forEach((folder) => {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, folder.xmlElementName);
      if (folder.folderClass) {
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_CLASS, FolderIPFType[folder.folderClass]);
      }
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_DISPLAY_NAME, folder.displayName);
      soapXmlWriter.writeEndElement();
    })
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}