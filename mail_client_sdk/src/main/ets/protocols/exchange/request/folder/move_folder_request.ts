/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderId } from '../../model/folder/folder';
import { MoveFolder } from '../../model/folder/move_folder';
import { SoapXmlWriter, XmlElement, XmlNamespace } from '../../xml/index';
import { RequestBase } from '../request_base';

/**
 * 移动文件夹，xml请求报文
 *
 * @since 2024-03-28
 */
export class MoveFolderRequest extends RequestBase<MoveFolder> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartSingleElement(XmlElement.NAME_MOVE_FOLDER)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_MESSAGE);
    soapXmlWriter.writeStartSingleElement(XmlElement.NAME_TO_FOLDER_ID);
    let toFolder: FolderId = this.rawData.toFolderId;
    toFolder?.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartSingleElement(XmlElement.NAME_FOLDER_IDS);
    let folders: FolderId[] = this.rawData.folderIds;
    folders?.forEach(folder => {
      folder.writeToXml(soapXmlWriter);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}
