/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderId } from '../../model/folder/folder';

import { FolderHierarchy } from '../../model/sync/folder_hierarchy';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';

/**
 * 获取所有文件夹或更改的文件夹列表
 *
 * @since 2024-03-24
 */
export class SyncFolderHierarchyRequest extends RequestBase<FolderHierarchy> {
  /**
   * 将协议头写入
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 根据参数构建ews协议报文体
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    let data = this.rawData;
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SYNC_FOLDER_HIERARCHY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDER_SHAPE);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE);
    soapXmlWriter.writeElementValue(data.propertySet);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    let syncFolderId: FolderId = data.syncFolderId;
    if (syncFolderId) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SYNC_FOLDER_ID);
      data.syncFolderId.writeToXml(soapXmlWriter);
      soapXmlWriter.writeEndElement();
    }
    if (data.syncState) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.MESSAGE, XmlElement.NAME_SYNC_STATE, data.syncState);
    }
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}