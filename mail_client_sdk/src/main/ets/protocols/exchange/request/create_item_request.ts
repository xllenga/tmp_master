/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateItem} from '../model/create_item';
import { MessageDisposition, SendMeetingInvitations } from '../utils/common_enum';
import {
  FolderName,
  SoapXmlWriter,
  XmlAttribute,
  XmlElement,
  XmlNamespace,
} from '../xml';
import { RequestBase } from './request_base';

/**
 * CreateItem标签请求封装
 *
 * @since 2024-03-16
 */
export abstract class CreateItemRequest<T extends CreateItem> extends RequestBase<T> {
  /**
   * 写入xml中Items的子节点
   *
   * @param soapXmlWriter xml写入器
   */
  protected abstract writeItemsChildren(soapXmlWriter: SoapXmlWriter): void;

  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY)
      .writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_CREATE_ITEM);
    if (this.rawData.messageDisposition !== undefined && this.rawData.messageDisposition !== null) {
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_MESSAGE_DISPOSITION,
        MessageDisposition[this.rawData.messageDisposition]);
    } else if (this.rawData.sendInvitationsMode !== undefined && this.rawData.sendInvitationsMode !== null) {
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_SEND_MEETING_INVITATIONS,
        SendMeetingInvitations[this.rawData.sendInvitationsMode]);
    }

    if (this.rawData.distinguishedFolderId || this.rawData.folderId) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SAVED_ITEM_FOLDER_ID);
      if (this.rawData.distinguishedFolderId) {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID)
          .writeAttribute(XmlAttribute.NAME_ID, FolderName[this.rawData.distinguishedFolderId])
          .writeEndElement();
      } else {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID)
          .writeAttribute(XmlAttribute.NAME_ID, this.rawData.folderId)
          .writeEndElement();
      }
      soapXmlWriter.writeEndElement();
    }

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEMS)
    this.writeItemsChildren(soapXmlWriter);
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement()
      .writeEndElement();
  }
}
