/*
 * Copyright © 2023-2024 Coremail论客.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { DeleteItem } from '../model/delete_item';
import { SoapXmlWriter, XmlAttribute, XmlElement, XmlNamespace } from '../xml';
import { RequestBase } from './request_base';

/**
 * 删除邮件、联系人、日历等公共请求request类
 *
 * @since 2024-03-19
 */
export class DeleteItemRequest extends RequestBase<DeleteItem> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    if (!this.rawData) {
      this.handleXmlEncodedError('encode xml body error,rawData is null');
    }
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY)
      .writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_DELETE_ITEM);
    soapXmlWriter.writeAttribute(XmlAttribute.NAME_DELETE_TYPE, this.rawData.deleteType);
    if (this.rawData.sendMeetingCancellations) {
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_SEND_MEETING_CANCELLATIONS, this.rawData.sendMeetingCancellations);
    }
    if (this.rawData.affectedTaskOccurrence) {
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_AFFECTED_TASK_OCCURRENCES, this.rawData.affectedTaskOccurrence);
    }
    if (this.rawData.suppressReadReceipts) {
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_SUPPRESS_READ_RECEIPTS, `${this.rawData.suppressReadReceipts}`);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_IDS)

    // 设置itemId数组
    this.rawData.itemIds?.forEach(itemId => {
      this.writeUniteId(XmlElement.NAME_ID_ITEM, itemId, soapXmlWriter);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement()
      .writeEndElement();
  }
}