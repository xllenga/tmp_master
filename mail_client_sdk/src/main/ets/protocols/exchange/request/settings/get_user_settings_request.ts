/* Copyright © 2023 - 2024 Coremail论客.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SoapXmlWriter } from '../../xml/soap_xml';
import { RequestBase } from '../request_base';
import { AutoDiscoverMessage } from '../../model/settings/autodiscover_message';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';

/**
 * 自动发现xml生成类
 *
 * @since 2024-04-12
 */
export class GetUserSettingsRequest extends RequestBase<AutoDiscoverMessage> {
  /**
   * 自动发现xml生成类
   *
   * @param soapXmlWriter 构造对象
   */
  public writeEnvelopeAttr(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.AUTO_DISCOVER, XmlNamespace.SCHEMA_AUTO_DISCOVER)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.WSA, XmlNamespace.SCHEMA_WSA)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.XSI, XmlNamespace.SCHEMA_XSI)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.SOAP, XmlNamespace.SCHEMA_SOAP);
  }


  /**
   * 生成 xml Header
   *
   * @param soapXmlWriter 构造对象
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    let domainMessage = this.rawData.userSettingMessage;
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_HEADER);
    soapXmlWriter.writeElementWithValue(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_REQUESTED_SERVER_VERSION,
      domainMessage.requestedServerVersion);
    soapXmlWriter.writeElementWithValue(XmlNamespace.WSA, XmlElement.NAME_ACTION, domainMessage.action);
    soapXmlWriter.writeElementWithValue(XmlNamespace.WSA, XmlElement.NAME_TO, domainMessage.to);
    soapXmlWriter.writeEndElement();
  }

  /**
   * 生成 xml Body
   *
   * @param soapXmlWriter 构造对象
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    let domainMessage = this.rawData.userSettingMessage;
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_GET_USER_SETTINGS_REQUEST_MESSAGE);
    soapXmlWriter.writeAttribute(`${XmlNamespace.XMLNS}${XmlNamespace.COLON}${XmlNamespace.AUTO_DISCOVER}`,
      XmlNamespace.SCHEMA_AUTO_DISCOVER);
    soapXmlWriter.writeStartElement(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_REQUEST);
    soapXmlWriter.writeStartElement(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_USERS);
    soapXmlWriter.writeStartElement(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_USER);
    soapXmlWriter.writeElementWithValue(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_MAILBOX,
      domainMessage.emailAddress);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_REQUESTED_SETTINGS);
    soapXmlWriter.writeElementWithValue(XmlNamespace.AUTO_DISCOVER, XmlElement.NAME_SETTING, domainMessage.setting);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}