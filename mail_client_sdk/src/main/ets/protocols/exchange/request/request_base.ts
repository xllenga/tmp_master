/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ClientErrorCode, ExchangeError } from '../error/exchange_error';
import { ModelBase, UniteId } from '../model/mode_base';
import { SoapXmlWriter } from '../xml/soap_xml';
import { XmlAttribute } from '../xml/xml_attribute';
import { XmlElement } from '../xml/xml_element';
import { XmlNamespace } from '../xml/xml_namespace';

/**
 * 服务端请求基类，业务请求服务端应该继承此类，在此类中对model中的报文数据进行结构封装。
 *
 * @param T 请求的model类型
 * @since 2024-03-19
 */
export abstract class RequestBase<T extends ModelBase> implements Verifiable {
  /**
   * 构造器
   *
   * @param rawData 数据模型
   */
  public constructor(protected rawData: T) {
  }

  /**
   * 获取数据模型
   */
  public getRawData(): T {
    return this.rawData;
  }

  /**
   * 将协议头数据写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  protected abstract writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void;

  /**
   * 将协议体写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  protected abstract writeBodyToXml(soapXmlWriter: SoapXmlWriter): void;

  /**
   * 写 ENVELOPE 的属性（命名空间定义）；子类可定制
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeEnvelopeAttr(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.XSI, XmlNamespace.SCHEMA_XSI)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.XSD, XmlNamespace.SCHEMA_XSD)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.MESSAGE, XmlNamespace.SCHEMA_MESSAGE)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.TYPE, XmlNamespace.SCHEMA_TYPE)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.SOAP, XmlNamespace.SCHEMA_SOAP);
  }

  /**
   * 将整个协议包写入xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartDocument();

    // <soap:Envelope>
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_ENVELOPE);
    this.writeEnvelopeAttr(soapXmlWriter);
    this.writeHeaderToXml(soapXmlWriter);
    this.writeBodyToXml(soapXmlWriter);

    // </soap:Envelope>
    soapXmlWriter.writeEndElement();
  }

  /**
   * 请求校验
   */
  public validate(): void {
    this.rawData?.validate();
  }

  /**
   * 将xml协议器中数据转换为文本数据
   *
   * @param soapXmlWriter xml写入器
   * @returns 协议文本数据，string类型
   */
  public xmlToString(soapXmlWriter: SoapXmlWriter): string {
    return soapXmlWriter.getXmlString();
  }

  /**
   * 向xml写入一个公共的头，封装相同头部写入操作
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeCommonHeader(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_HEADER);
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_REQUEST_SERVER_VERSION,
      new Map([[XmlAttribute.NAME_VERSION, this.rawData.getVersionString()]]));
    if (this.rawData.timeZoneDefinitionId) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TIME_ZONE_CONTEXT);
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_TIME_ZONE_DEFINITION,
        new Map([[XmlAttribute.NAME_ID, this.rawData.timeZoneDefinitionId]]));
      soapXmlWriter.writeEndElement();
    }
    soapXmlWriter.writeEndElement();
  }

  /**
   * 写入一个联合id到xml写入器
   *
   * @param elementName 元素名称
   * @param uniteId 联合id对象
   * @param soapXmlWriter 写入器
   */
  protected writeUniteId(elementName: string, uniteId: UniteId, soapXmlWriter: SoapXmlWriter): void {
    let itemIdAttr: Map<string, string> = new Map;
    itemIdAttr.set(XmlElement.NAME_ID, uniteId.id);
    if (uniteId.changeKey) {
      itemIdAttr.set(XmlElement.NAME_CHANGE_KEY, uniteId.changeKey);
    }
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, elementName, itemIdAttr);
  }

  /**
   * 抛出一个xml生成异常
   *
   * @param msg 异常信息
   */
  protected handleXmlEncodedError(msg: string): void {
    throw {
      name: 'ClientError',
      code: ClientErrorCode.XML_ENCODED_ERROR,
      message: msg
    } as ExchangeError;
  }
}

/**
 * 可验证接口，用于需要校验的请求
 */
export interface Verifiable {
  validate(): void;
}
