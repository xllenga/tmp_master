/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MoveItem, CopyItem } from '../model/move_copy_item';
import { SoapXmlWriter, XmlElement, XmlNamespace } from '../xml';
import { RequestBase } from './request_base';

/**
 * 移动邮件、联系人、日历等公共请求request类
 *
 * @since 2024-03-19
 */
export class MoveItemRequest extends RequestBase<MoveItem> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    if (!this.rawData) {
      this.handleXmlEncodedError('encode xml body error,rawData is null');
    }
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    this.rawData.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 复制邮件、联系人、日历等公共请求request类
 *
 * @since 2024-03-19
 */
export class CopyItemRequest extends RequestBase<CopyItem> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    if (!this.rawData) {
      this.handleXmlEncodedError('encode xml body error,rawData is null');
    }
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    this.rawData.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}