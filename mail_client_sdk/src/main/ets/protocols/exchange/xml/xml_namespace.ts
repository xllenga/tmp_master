/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Xml命名空间
 */
export enum XmlNamespace {
  // 命名空间值
  SCHEMA_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/' ,
  SCHEMA_TYPE = 'http://schemas.microsoft.com/exchange/services/2006/types' ,
  SCHEMA_MESSAGE = 'http://schemas.microsoft.com/exchange/services/2006/messages' ,
  SCHEMA_XSI = 'http://www.w3.org/2001/XMLSchema-instance' ,
  SCHEMA_AUTO_DISCOVER = 'http://schemas.microsoft.com/exchange/2010/Autodiscover' ,
  SCHEMA_WSA = 'http://www.w3.org/2005/08/addressing' ,
  SCHEMA_XSD = 'http://www.w3.org/2001/XMLSchema' ,

  // 命名空间前缀
  SOAP = 'soap' ,
  SOAP_SHORT = 's' ,
  TYPE = 't' ,
  MESSAGE = 'm' ,
  XSI = 'xsi' ,
  XSD = 'xsd' ,
  AUTO_DISCOVER = 'a' ,
  WSA = 'wsa' ,
  XMLNS = 'xmlns' ,
  COLON = ':' ,
}
