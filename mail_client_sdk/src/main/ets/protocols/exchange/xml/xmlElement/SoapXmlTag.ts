import { createXmlTag, getByteLengthFromString, XmlTag } from '.';
import { ExchangeVersion } from '../../utils/common_enum';
import { EwsUtilities } from './ewsConstans';

export interface HeaderOption {
  requestedServerVersion: ExchangeVersion;
  timezone: string;
}

export interface NamespaceAttrOption {
  defaultNs: boolean;
}

export enum TagType {
  message, type, autoDiscover
}

const DeclarationLength = getByteLengthFromString('<?xml version="1.0" encoding="utf-8"?>');

export class SoapXmlTag extends XmlTag {

  static createHeader(option?: Partial<HeaderOption>): SoapXmlTag {
    const header = new SoapXmlTag(`${EwsUtilities.EwsSoapNamespacePrefix}:Header`)
    if (option) {
      if (typeof option.requestedServerVersion === 'number') {
        header.appendChild(
          createXmlTag('t:RequestServerVersion').setAttribute('Version', ExchangeVersion[option.requestedServerVersion])
        );
      };
      if (option.timezone) {
        header.appendChild(
          createXmlTag('t:TimeZoneContext').appendChild(
            createXmlTag('t:TimeZoneDefinition').setAttribute('Id', option.timezone)
          )
        );
      };
    }
    return header;
  }

  static createBody(): SoapXmlTag {
    return new SoapXmlTag(`${EwsUtilities.EwsSoapNamespacePrefix}:Body`)
  }

  // 标签类型
  protected readonly tagType: TagType | null = null;

  private handleNamespaceOption(prefix: string, opt?: NamespaceAttrOption) {
    let namespaceKey = 'xmlns';
    if (opt && opt.defaultNs) {
      return namespaceKey;
    };
    return namespaceKey + ':' + prefix;
  }

  public setNamespaceAttrXsi() {
    return this.setAttribute(`xmlns:${EwsUtilities.EwsXmlSchemaInstanceNamespacePrefix}`, EwsUtilities.EwsXmlSchemaInstanceNamespace)
  }

  public setNamespaceAttrXsd() {
    return this.setAttribute(`xmlns:${EwsUtilities.EwsXmlSchemaNamespacePrefix}`, EwsUtilities.EwsXmlSchemaNamespace)
  }

  public setNamespaceAttrAutoDiscover(opt?: NamespaceAttrOption) {
    let namespaceKey = this.handleNamespaceOption(EwsUtilities.AutodiscoverSoapNamespacePrefix, opt);
    return this.setAttribute(namespaceKey, EwsUtilities.AutodiscoverSoapNamespace);
  }

  public setNamespaceAttrWsa() {
    return this.setAttribute(`xmlns:${EwsUtilities.WSAddressingNamespacePrefix}`, EwsUtilities.WSAddressingNamespace)
  }

  public setNamespaceAttrType(opt?: NamespaceAttrOption) {
    let namespaceKey = this.handleNamespaceOption(EwsUtilities.EwsTypesNamespacePrefix, opt);
    return this.setAttribute(namespaceKey, EwsUtilities.EwsTypesNamespace)
  }

  public setNamespaceAttrMessage(opt?: NamespaceAttrOption) {
    let namespaceKey = this.handleNamespaceOption(EwsUtilities.EwsMessagesNamespacePrefix, opt);
    return this.setAttribute(namespaceKey, EwsUtilities.EwsMessagesNamespace)
  }
}

export class SoapEnvelope extends SoapXmlTag {

  static createEnvelope(): SoapEnvelope {
    return new SoapEnvelope(`${EwsUtilities.EwsSoapNamespacePrefix}:Envelope`)
      .setAttribute(`xmlns:${EwsUtilities.EwsSoapNamespacePrefix}`, EwsUtilities.EwsSoapNamespace)
  }

  private writeDeclaration: boolean = true;

  public isWriteDeclaration() {
    return this.writeDeclaration;
  }
  public setWriteDeclaration(opt: boolean) {
    this.writeDeclaration = opt;
  }

  public getXmlLength(): number {
    let len = super.getXmlLength();
    if (this.isWriteDeclaration()) {
      len += DeclarationLength;
    }
    return len;
  }

  public toString(): string {
    if (this.isWriteDeclaration()) {
      XmlTag.xmlSerializer.setDeclaration();
    };
    return super.toString();
  }
}

export const createSoapXmlTag = (tagName: string) => new SoapXmlTag(tagName);
export const createSoapEnvelope = () => SoapEnvelope.createEnvelope();
export const createSoapHeader = (option?: Partial<HeaderOption>) => SoapXmlTag.createHeader(option);
export const createSoapBody = () => SoapXmlTag.createBody();