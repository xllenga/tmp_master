import { SoapXmlTag, TagType } from '../SoapXmlTag';


class MeetingTimeZoneTag extends SoapXmlTag {
  readonly tagType = TagType.type;
  public setTimeZoneName(s: string) {
    this.setAttribute('TimeZoneName', s);
    return this;
  }
}

export const $MeetingTimeZone = () => new MeetingTimeZoneTag('MeetingTimeZone');