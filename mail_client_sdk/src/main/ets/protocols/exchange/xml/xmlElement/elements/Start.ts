import { SoapXmlTag, TagType } from '../SoapXmlTag';


class StartTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $Start = () => new StartTag('Start');