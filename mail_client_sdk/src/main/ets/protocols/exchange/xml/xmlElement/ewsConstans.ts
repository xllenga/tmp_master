
export class EwsUtilities {
  //#region constants in c# - typescript static
  static XSFalse: string = "false";
  static XSTrue: string = "true";
  static EwsTypesNamespacePrefix: string = "t";
  static EwsMessagesNamespacePrefix: string = "m";
  static EwsErrorsNamespacePrefix: string = "e";
  static EwsSoapNamespacePrefix: string = "soap";
  static EwsXmlSchemaInstanceNamespacePrefix: string = "xsi";
  static EwsXmlSchemaNamespacePrefix: string = "xsd";
  static PassportSoapFaultNamespacePrefix: string = "psf";
  static WSTrustFebruary2005NamespacePrefix: string = "wst";
  static WSAddressingNamespacePrefix: string = "wsa";
  static AutodiscoverSoapNamespacePrefix: string = "a";
  static WSSecurityUtilityNamespacePrefix: string = "wsu";
  static WSSecuritySecExtNamespacePrefix: string = "wsse";
  static EwsTypesNamespace: string = "http://schemas.microsoft.com/exchange/services/2006/types";
  static EwsMessagesNamespace: string = "http://schemas.microsoft.com/exchange/services/2006/messages";
  static EwsErrorsNamespace: string = "http://schemas.microsoft.com/exchange/services/2006/errors";
  static EwsSoapNamespace: string = "http://schemas.xmlsoap.org/soap/envelope/";
  static EwsSoap12Namespace: string = "http://www.w3.org/2003/05/soap-envelope";
  static EwsXmlSchemaInstanceNamespace: string = "http://www.w3.org/2001/XMLSchema-instance";
  static EwsXmlSchemaNamespace: string = "http://www.w3.org/2001/XMLSchema";
  static PassportSoapFaultNamespace: string = "http://schemas.microsoft.com/Passport/SoapServices/SOAPFault";
  static WSTrustFebruary2005Namespace: string = "http://schemas.xmlsoap.org/ws/2005/02/trust";
  static WSAddressingNamespace: string = "http://www.w3.org/2005/08/addressing";
  static AutodiscoverSoapNamespace: string = "http://schemas.microsoft.com/exchange/2010/Autodiscover";
  static WSSecurityUtilityNamespace: string = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
  static WSSecuritySecExtNamespace: string = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"

}
