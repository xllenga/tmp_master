import { FolderName } from '../../xml_attribute';
import { SoapXmlTag, TagType } from '../SoapXmlTag';


class DistinguishedFolderIdTag extends SoapXmlTag {
  // 标签类型
  readonly tagType = TagType.type

  setId(fn: FolderName) {
    this.setAttribute('Id', FolderName[fn].toLowerCase());
    return this;
  }
  setChangeKey(changeKey: string) {
    this.setAttribute('ChangeKey', changeKey)
  }
}

export const $DistinguishedFolderId = () => new DistinguishedFolderIdTag('DistinguishedFolderId');