import { booleanToString } from '../../../utils/transformToString';
import { SoapXmlTag, TagType } from '../SoapXmlTag';


export enum BodyType {
  HTML, Text
}

class BodyTag extends SoapXmlTag {

  // 标签类型
  readonly tagType = TagType.type

  setBodyType(bodyType: BodyType) {
    return this.setAttribute('BodyType', BodyType[bodyType]);
  }

  setIsTruncated(isTruncated: boolean) {
    return this.setAttribute('IsTruncated', booleanToString(isTruncated));
  }
}

export const $Body = () => new BodyTag('Body');