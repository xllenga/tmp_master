import { SoapXmlTag, TagType } from '../SoapXmlTag';


class EmailAddressTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $EmailAddress = () => new EmailAddressTag('EmailAddress');