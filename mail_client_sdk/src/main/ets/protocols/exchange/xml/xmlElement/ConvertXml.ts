import convertxml from '@ohos.convertxml';
import { createXmlTag, XmlTag } from './XmlTag';

export interface ElementAttr {
  _type: string;
  _name: string;
  _text?: string;
  _attributes?: object;
  _elements: ElementAttr[]
}

export interface XmlJsObj {
  _elements: ElementAttr[]
}

export class CustomConvertXml extends convertxml.ConvertXML {

  private _convertToXmlTag(xmlJsObj: ElementAttr, parentTag: null | XmlTag) {
    let newTag: XmlTag | null = null;
    if (xmlJsObj._elements && xmlJsObj._elements.length) {
      for (const el of xmlJsObj._elements) {
        if (el._type === 'element') {
          newTag = createXmlTag(el._name);
          if (el._attributes) {
            for (const attrKey of Object.keys(el._attributes)) {
              const attrValue: string = el._attributes[attrKey];
              newTag.setAttribute(attrKey, attrValue);
            }
          }
        } else if (el._type === 'text') {
          if (parentTag) {
            parentTag.setText(el._text || '');
          }
        }
        if (newTag) {
          if (parentTag) {
            parentTag.appendChild(newTag);
          }
          this._convertToXmlTag(el, newTag)
        }
      }
    }
    return newTag;
  }

  // 将xml js对象转换为 XmlTag 对象树, 方便查找取值
  public convertToXmlTag(value: string): XmlTag | null {
    const xmlJsObj = this.convertToJSObject(value) as ElementAttr;
    return this._convertToXmlTag(xmlJsObj, null);
  }
}