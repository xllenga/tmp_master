import { SoapXmlTag, TagType } from '../SoapXmlTag';


class DisplayNameTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $DisplayName = () => new DisplayNameTag('DisplayName');