import { SoapXmlTag, TagType } from '../SoapXmlTag';

export enum SendMeetingInvitationsEnum {
  SendToNone, SendOnlyToAll, SendToAllAndSaveCopy
}

class CreateItemTag extends SoapXmlTag {

  // 标签类型
  readonly tagType = TagType.message

  setSendMeetingInvitations(option: SendMeetingInvitationsEnum) {
    this.setAttribute('SendMeetingInvitations', SendMeetingInvitationsEnum[option]);
    return this
  }
}

export const $CreateItem = () => new CreateItemTag('CreateItem');