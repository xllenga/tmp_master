import { SoapXmlTag, TagType } from '../SoapXmlTag';


class ReminderMinutesBeforeStartTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $ReminderMinutesBeforeStart = () => new ReminderMinutesBeforeStartTag('ReminderMinutesBeforeStart');