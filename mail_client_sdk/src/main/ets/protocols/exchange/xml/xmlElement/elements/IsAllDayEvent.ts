import { SoapXmlTag, TagType } from '../SoapXmlTag';


class IsAllDayEventTag extends SoapXmlTag {
  // 标签类型
  readonly tagType = TagType.type
}

export const $IsAllDayEvent = () => new IsAllDayEventTag('IsAllDayEvent');
