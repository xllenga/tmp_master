import { SoapXmlTag, TagType } from '../SoapXmlTag';


class CalendarItemTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $CalendarItem = () => new CalendarItemTag('CalendarItem');