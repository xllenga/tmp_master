import { SoapXmlTag, TagType } from '../SoapXmlTag';


class FolderIdTag extends SoapXmlTag {
  readonly tagType = TagType.type;

  setId(id: string) {
    this.setAttribute('Id', id);
    return this;
  }

  setChangeKey(changeKey: string) {
    this.setAttribute('ChangeKey', changeKey);
    return this;
  }
}


export const $FolderId = () => new FolderIdTag('FolderId');
